select s.total_money ,o.id from order_entity o join storage_out_record s on o.order_id = s.order_id
and o.id = s.child_order_id


SELECT * FROM assembly WHERE code = 'BLD005';

SELECT sum(count) FROM storage_out_record WHERE assembly_id = 252 ;
-- # 35192
SELECT sum(count)
FROM storage_input_assembly
WHERE assembly_id = 252
-- # 205192

SELECT *
FROM stock
WHERE assembly_id in (270,326,328,329,332)

SELECT
  (sum(a.count) - (SELECT sum(b.count)
                   FROM storage_out_record b
                   WHERE a.assembly_id = b.assembly_id)) a
FROM storage_input_assembly a
where a.assembly_id = 252 group by a.assembly_id

-- # 查询库存表里面的数据是不是准确的
select a , assembly_id from (
SELECT ( (sum(a.count) - (SELECT case when (sum(b.count)) then sum(b.count) else 0  end
                          FROM storage_out_record b WHERE a.assembly_id = b.assembly_id))
-(select s.number from stock s where s.assembly_id = a.assembly_id))a,a.assembly_id FROM storage_input_assembly a
where a.assembly_id = a.assembly_id and a.assembly_type = 1 group by a.assembly_id
) a where a != 0

-- # 校正库存sql
update stock s set s.number =
(
  SELECT
    (sum(a.count) - (SELECT case when (sum(b.count)) then sum(b.count) else 0  end
                     FROM storage_out_record b
                     WHERE a.assembly_id = b.assembly_id)) a
  FROM storage_input_assembly a
  where a.assembly_id = s.assembly_id group by a.assembly_id
) where s.assembly_id = 252
-- 修补为null数据
update stock s set s.number = 0 where s.number is null

select * from storage_input_assembly a  where a.input_storage_number = 20190224054

select * from order_change_record a where a.order_id = 2019022612160764371
SELECT * FROM storage_out_record b where b.order_id = 20190224054
select * from order_entity o where o.order_id = 20190224054
select s.assembly_id , count(s.number )from stock s group by  s.assembly_id having count(s.number ) >1

-- 查看某单入库量和库存量
SELECT sum(a.count),(SELECT
                  case when (sum(b.count)) then sum(b.count)
                    else 0
                  end
                   FROM storage_out_record b
                   WHERE a.assembly_id = b.assembly_id) a
FROM storage_input_assembly a
where 1=1  group by a.assembly_id
