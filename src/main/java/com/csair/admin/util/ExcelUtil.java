package com.csair.admin.util;

import com.csair.admin.core.dto.InputFileDataDto;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

public class ExcelUtil {

    public static HSSFWorkbook getInputStorageDtoHSSFWorkbook(InputFileDataDto dto) {
        String row1Value = dto.getRow1Value();
        List<List<Object>> data = dto.getData();
        String end1 = dto.getEnd1();
        HSSFWorkbook wb = new HSSFWorkbook();
        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet("sheet1");
        sheet.setColumnWidth(2, 9000);
        String[] row3;
        if (dto.getIsProduct()) {
            row3 = "入库单,名称,规格型号,单位,数量,原始点数,出库点数,单价,备注".split(",");
        } else {
            row3 = "入库单,名称,规格型号,编码,单位,数量,备注".split(",");
        }
        int totalLength = row3.length;
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, totalLength - 1)); //
//        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, totalLength-2)); //
        if (dto.getIsProduct()) {
            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 6000);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 2000);
            sheet.setColumnWidth(4, 4000);
            sheet.setColumnWidth(5, 4000);
        }else{
            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 6000);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 6000);
            sheet.setColumnWidth(4, 2000);
            sheet.setColumnWidth(5, 4000);
        }

        //设置列宽
        for (int i = 6; i < totalLength; i++) {
            sheet.setColumnWidth(i, 4000);
        }

        int rowCount = 0;
        HSSFCellStyle baseRowStyle = ExcelUtil.getBaseRowStyle(wb);
        HSSFRow row = sheet.createRow(rowCount++);
        HSSFCell cell = row.createCell(0);
        row.setHeightInPoints((short) 30);
        cell.setCellValue(row1Value);
        cell.setCellStyle(getFirstRowStyle(wb));
//        row = sheet.createRow(rowCount++);
//        cell = row.createCell(0);
//        cell.setCellValue(row2Value);
//        cell.setCellStyle(getFirstRowStyle(wb));
        row = sheet.createRow(rowCount++);
        for (int i = 0; i < row3.length; i++) {
            HSSFCell cell1 = row.createCell(i);
            cell1.setCellValue(row3[i]);
            cell1.setCellStyle(baseRowStyle);
        }
        for (int i = 0; i < data.size(); i++) {
            row = sheet.createRow(rowCount++);
            for (int j = 0; j < data.get(i).size(); j++) {
                Object o = data.get(i).get(j);
                ExcelUtil.setCellValue(row, j, wb, o).setCellStyle(baseRowStyle);
            }
        }
//        sheet.createRow(rowCount).createCell(0).setCellValue(end1);
//        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount++, 0, totalLength));
//        sheet.createRow(rowCount).createCell(0).setCellValue(end2);
//        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount++, 0, totalLength));
//        sheet.createRow(rowCount).createCell(0).setCellValue(end3);
//        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount += 4, 0, totalLength));
        return wb;
    }

    /**
     * 导出Excel
     *
     * @param sheetName sheet名称
     * @param title     标题
     * @param values    内容
     * @param wb        HSSFWorkbook对象
     * @return
     */
    public static HSSFWorkbook getInputStorageDtoHSSFWorkbook(String sheetName, String[] title, String[][] values, HSSFWorkbook wb) {

        // 第一步，创建一个HSSFWorkbook，对应一个Excel文件
        if (wb == null) {
            wb = new HSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet(sheetName);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        HSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        //声明列对象
        HSSFCell cell = null;

        //创建标题
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);
        }
        //创建内容
        for (int i = 0; i < values.length; i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < values[i].length; j++) {
                //将内容按顺序赋给对应的列对象
                row.createCell(j).setCellValue(values[i][j]);
            }
        }
        return wb;
    }


    private static final String EXCEL_XLS = "xls";
    private static final String EXCEL_XLSX = "xlsx";

    /**
     * 判断Excel的版本,获取Workbook
     *
     * @param in
     * @return
     * @throws IOException
     */
    public static Workbook getWorkbok(InputStream in, File file) throws IOException {
        Workbook wb = null;
        if (file.getName().endsWith(EXCEL_XLS)) {  //Excel 2003
            wb = new HSSFWorkbook(in);
        } else if (file.getName().endsWith(EXCEL_XLSX)) {  // Excel 2007/2010
            wb = new XSSFWorkbook(in);
        }
        return wb;
    }

    /**
     * 判断文件是否是excel
     *
     * @throws Exception
     */
    public static void checkExcelVaild(File file) throws Exception {
        if (!file.exists()) {
            throw new Exception("文件不存在");
        }
        if (!(file.isFile() && (file.getName().endsWith(EXCEL_XLS) || file.getName().endsWith(EXCEL_XLSX)))) {
            throw new Exception("文件不是Excel");
        }
    }


    /**
     * 粗体显示
     */
    public static HSSFCellStyle getFirstRowStyle(HSSFWorkbook wb) {
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        HSSFFont font = wb.createFont();
        font.setFontName("宋体");
        font.setBold(true);//粗体显示
        font.setFontHeightInPoints((short) 18);//设置字体大小
        style.setFont(font);
        return style;
    }

    /**
     * 基础的样式
     */
    public static HSSFCellStyle getBaseRowStyle(HSSFWorkbook wb) {
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

    public static XSSFCell setCellValue(XSSFRow row, int count, XSSFWorkbook wb, Object o) {
        XSSFCell cellEntry = row.createCell(count);
        if (o instanceof String) {
            cellEntry.setCellValue((String) o);
        } else {
            try {
                cellEntry.setCellValue(Double.valueOf(o + ""));
            } catch (Exception e) {
                cellEntry.setCellValue(o + "");
            }
        }
//        cellEntry.setCellStyle(ExcelUtil.getBaseRowStyle(wb));
        return cellEntry;
    }

    public static HSSFCell setCellValue(HSSFRow row, int count, HSSFWorkbook wb, Object o) {
        HSSFCell cellEntry = row.createCell(count);
        if (o instanceof String) {
            cellEntry.setCellValue((String) o);
        } else {
            try {
                cellEntry.setCellValue(Double.valueOf(o + ""));
            } catch (Exception e) {
                cellEntry.setCellValue(o + "");
            }
        }
//        cellEntry.setCellStyle(ExcelUtil.getBaseRowStyle(wb));
        return cellEntry;
    }


    public static Object getValue(Cell cell) {
        Object obj = null;
        switch (cell.getCellTypeEnum()) {
            case BOOLEAN:
                obj = cell.getBooleanCellValue();
                break;
            case ERROR:
                obj = cell.getErrorCellValue();
                break;
            case NUMERIC:
                obj = cell.getNumericCellValue();
                break;
            case STRING:
                obj = cell.getStringCellValue();
                break;
            default:
                break;
        }
        return obj;
    }
}