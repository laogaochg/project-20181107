package com.csair.admin.util;

import com.csair.admin.core.po.core.User;
import org.apache.shiro.SecurityUtils;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/23 21:32
 */
public class UserUtil {
    public static User getUser() {
        return (User) SecurityUtils.getSubject().getSession().getAttribute(ParamConstants.USER_SESSION);
    }

    /**
     *
     * @return 仓库id
     */
    public static Integer getUserStorageId() {
        return ((User) SecurityUtils.getSubject().getSession().getAttribute(ParamConstants.USER_SESSION)).getType();
    }

    /**
     * 请到当前session里面的用户id
     */
    public static Long getUserId() {
        return ((User) SecurityUtils.getSubject().getSession().getAttribute(ParamConstants.USER_SESSION)).getId();
    }
}
