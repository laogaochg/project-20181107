package com.csair.admin.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import com.csair.admin.core.dao.IdGenerateDao;
import com.csair.admin.core.po.IdGenerate;
import org.springframework.util.StringUtils;

/**
 * laogaochg
 * 2017/7/6.
 */
public class StringUtil {
    /**
     * 检查字符串有没有值；
     * 有就返回这个字符串；
     * 没有就返回空字符串
     */
    public static String getString(String s) {
        return StringUtils.hasText(s) ? s : "";
    }


    /**
     * 给数组中间加指定的String
     */
    public static String join(Collection<?> coll, String delim) {
        return StringUtils.collectionToDelimitedString(coll, delim);
    }

    /**
     * 只留6位小数
     */
    public static String to6Point(String d) {
        if(!StringUtils.hasText(d)) d = "0";
        NumberFormat f = new DecimalFormat("0.000000");
        return f.format(Double.valueOf(d));
    }
    /**
     * 只留3位小数
     */
    public static String to3Point(String d) {
        if(!StringUtils.hasText(d)) d = "0";
        NumberFormat f = new DecimalFormat("0.000");
        return f.format(Double.valueOf(d));
    }
    /**
     * 只留2位小数
     */
    public static String to2Point(String d) {
        if(!StringUtils.hasText(d)) d = "0";
        NumberFormat f = new DecimalFormat("0.00");
        return f.format(Double.valueOf(d));
    }
    /**
     * 只留2位小数
     */
    public static String to0Point(String d) {
        if(!StringUtils.hasText(d)) d = "0";
        NumberFormat f = new DecimalFormat("0");
        return f.format(Double.valueOf(d));
    }


    /**
     * 给数组中间加指定的String
     */
    public static boolean isNumeric(CharSequence cs) {
        return org.apache.commons.lang3.StringUtils.isNumeric(cs);
    }

    private static Long count = 1L;

    public static synchronized String getOrderId() {
        //自动生成订单号
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        IdGenerateDao bean = SpringContextUtil.getBean("idGenerateDao", IdGenerateDao.class);
        IdGenerate a = new IdGenerate();
        a.setName("1");
        bean.insert(a);
        String s = format.format(new Date());
        DecimalFormat d = new DecimalFormat("000");
        return s + d.format(a.getId() % 1000);
    }

    public static synchronized String getChildOrderId() {
        try {
            Thread.sleep(1);//不管怎么调，毫秒数肯定不一样
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //自动生成订单号
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String s = format.format(new Date());
        for (int i = 0; i < 2; i++) {
            s = s + new Random().nextInt(9);
        }
        return s;
    }
}
