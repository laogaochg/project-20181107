package com.csair.admin.core.dao;

import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AssemblyDao {
    int countByExample(AssemblyQuery example);

    int deleteByExample(AssemblyQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(Assembly record);

    int insertSelective(Assembly record);

    List<Assembly> selectByExample(AssemblyQuery example);

    Assembly selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Assembly record, @Param("example") AssemblyQuery example);

    int updateByExample(@Param("record") Assembly record, @Param("example") AssemblyQuery example);

    int updateByPrimaryKeySelective(Assembly record);

    int updateByPrimaryKey(Assembly record);
}