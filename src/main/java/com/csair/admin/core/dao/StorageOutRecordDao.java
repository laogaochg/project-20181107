package com.csair.admin.core.dao;

import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StorageOutRecordDao {
    int countByExample(StorageOutRecordQuery example);

    int deleteByExample(StorageOutRecordQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(StorageOutRecord record);

    int insertSelective(StorageOutRecord record);

    List<StorageOutRecord> selectByExample(StorageOutRecordQuery example);

    StorageOutRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") StorageOutRecord record, @Param("example") StorageOutRecordQuery example);

    int updateByExample(@Param("record") StorageOutRecord record, @Param("example") StorageOutRecordQuery example);

    int updateByPrimaryKeySelective(StorageOutRecord record);

    int updateByPrimaryKey(StorageOutRecord record);
}