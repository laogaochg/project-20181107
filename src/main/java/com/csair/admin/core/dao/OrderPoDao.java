package com.csair.admin.core.dao;

import com.csair.admin.core.po.OrderPo;
import com.csair.admin.core.po.OrderPoQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderPoDao {
    int countByExample(OrderPoQuery example);

    int deleteByExample(OrderPoQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderPo record);

    int insertSelective(OrderPo record);

    List<OrderPo> selectByExample(OrderPoQuery example);

    OrderPo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderPo record, @Param("example") OrderPoQuery example);

    int updateByExample(@Param("record") OrderPo record, @Param("example") OrderPoQuery example);

    int updateByPrimaryKeySelective(OrderPo record);

    int updateByPrimaryKey(OrderPo record);
}