package com.csair.admin.core.dao;

import org.apache.ibatis.annotations.Param;

import com.csair.admin.core.po.Warehouse;

public interface WarehouseDao {

	Warehouse selectWareByUserId(Long warehouseManagId);

	int updateWare(Warehouse qo);

	int insertWare(Warehouse qo);

}
