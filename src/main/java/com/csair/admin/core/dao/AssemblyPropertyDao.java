package com.csair.admin.core.dao;

import com.csair.admin.core.po.AssemblyProperty;
import com.csair.admin.core.po.AssemblyPropertyQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AssemblyPropertyDao {
    int countByExample(AssemblyPropertyQuery example);

    int deleteByExample(AssemblyPropertyQuery example);

    int insert(AssemblyProperty record);

    int insertSelective(AssemblyProperty record);

    List<AssemblyProperty> selectByExample(AssemblyPropertyQuery example);

    int updateByExampleSelective(@Param("record") AssemblyProperty record, @Param("example") AssemblyPropertyQuery example);

    int updateByExample(@Param("record") AssemblyProperty record, @Param("example") AssemblyPropertyQuery example);
}