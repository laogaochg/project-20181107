package com.csair.admin.core.dao;

import com.csair.admin.core.po.Stock;
import com.csair.admin.core.po.StockQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StockDao {
    int countByExample(StockQuery example);

    int deleteByExample(StockQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(Stock record);

    int insertSelective(Stock record);

    List<Stock> selectByExample(StockQuery example);

    Stock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Stock record, @Param("example") StockQuery example);

    int updateByExample(@Param("record") Stock record, @Param("example") StockQuery example);

    int updateByPrimaryKeySelective(Stock record);

    int updateByPrimaryKey(Stock record);
}