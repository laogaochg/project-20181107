package com.csair.admin.core.dao;

import com.csair.admin.core.po.UserMessage;
import com.csair.admin.core.po.UserMessageQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMessageDao {
    int countByExample(UserMessageQuery example);

    int deleteByExample(UserMessageQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(UserMessage record);

    int insertSelective(UserMessage record);

    List<UserMessage> selectByExample(UserMessageQuery example);

    UserMessage selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserMessage record, @Param("example") UserMessageQuery example);

    int updateByExample(@Param("record") UserMessage record, @Param("example") UserMessageQuery example);

    int updateByPrimaryKeySelective(UserMessage record);

    int updateByPrimaryKey(UserMessage record);
}