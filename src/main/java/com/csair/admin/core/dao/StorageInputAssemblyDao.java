package com.csair.admin.core.dao;

import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StorageInputAssemblyDao {
    int countByExample(StorageInputAssemblyQuery example);

    int deleteByExample(StorageInputAssemblyQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(StorageInputAssembly record);

    int insertSelective(StorageInputAssembly record);

    List<StorageInputAssembly> selectByExample(StorageInputAssemblyQuery example);

    StorageInputAssembly selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") StorageInputAssembly record, @Param("example") StorageInputAssemblyQuery example);

    int updateByExample(@Param("record") StorageInputAssembly record, @Param("example") StorageInputAssemblyQuery example);

    int updateByPrimaryKeySelective(StorageInputAssembly record);

    int updateByPrimaryKey(StorageInputAssembly record);
}