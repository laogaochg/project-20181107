package com.csair.admin.core.dao;

import com.csair.admin.core.po.StorageInput;
import com.csair.admin.core.po.StorageInputQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StorageInputDao {
    int countByExample(StorageInputQuery example);

    int deleteByExample(StorageInputQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(StorageInput record);

    int insertSelective(StorageInput record);

    List<StorageInput> selectByExample(StorageInputQuery example);

    StorageInput selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") StorageInput record, @Param("example") StorageInputQuery example);

    int updateByExample(@Param("record") StorageInput record, @Param("example") StorageInputQuery example);

    int updateByPrimaryKeySelective(StorageInput record);

    int updateByPrimaryKey(StorageInput record);
}