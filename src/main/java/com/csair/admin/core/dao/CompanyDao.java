package com.csair.admin.core.dao;

import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.CompanyQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyDao {
    int countByExample(CompanyQuery example);

    int deleteByExample(CompanyQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(Company record);

    int insertSelective(Company record);

    List<Company> selectByExample(CompanyQuery example);

    Company selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Company record, @Param("example") CompanyQuery example);

    int updateByExample(@Param("record") Company record, @Param("example") CompanyQuery example);

    int updateByPrimaryKeySelective(Company record);

    int updateByPrimaryKey(Company record);
}