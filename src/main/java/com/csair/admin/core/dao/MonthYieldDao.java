package com.csair.admin.core.dao;

import com.csair.admin.core.po.MonthYield;
import com.csair.admin.core.po.MonthYieldQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface MonthYieldDao {
    int countByExample(MonthYieldQuery example);

    int deleteByExample(MonthYieldQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(MonthYield record);

    int insertSelective(MonthYield record);

    List<MonthYield> selectByExample(MonthYieldQuery example);

    MonthYield selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MonthYield record, @Param("example") MonthYieldQuery example);

    int updateByExample(@Param("record") MonthYield record, @Param("example") MonthYieldQuery example);

    int updateByPrimaryKeySelective(MonthYield record);

    int updateByPrimaryKey(MonthYield record);
}