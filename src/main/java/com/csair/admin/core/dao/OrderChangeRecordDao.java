package com.csair.admin.core.dao;

import com.csair.admin.core.po.OrderChangeRecord;
import com.csair.admin.core.po.OrderChangeRecordQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderChangeRecordDao {
    int countByExample(OrderChangeRecordQuery example);

    int deleteByExample(OrderChangeRecordQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderChangeRecord record);

    int insertSelective(OrderChangeRecord record);

    List<OrderChangeRecord> selectByExample(OrderChangeRecordQuery example);

    OrderChangeRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderChangeRecord record, @Param("example") OrderChangeRecordQuery example);

    int updateByExample(@Param("record") OrderChangeRecord record, @Param("example") OrderChangeRecordQuery example);

    int updateByPrimaryKeySelective(OrderChangeRecord record);

    int updateByPrimaryKey(OrderChangeRecord record);
}