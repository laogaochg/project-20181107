package com.csair.admin.core.dao;

import com.csair.admin.core.po.IdGenerate;
import com.csair.admin.core.po.IdGenerateQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IdGenerateDao {
    int countByExample(IdGenerateQuery example);

    int deleteByExample(IdGenerateQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(IdGenerate record);

    int insertSelective(IdGenerate record);

    List<IdGenerate> selectByExample(IdGenerateQuery example);

    IdGenerate selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") IdGenerate record, @Param("example") IdGenerateQuery example);

    int updateByExample(@Param("record") IdGenerate record, @Param("example") IdGenerateQuery example);

    int updateByPrimaryKeySelective(IdGenerate record);

    int updateByPrimaryKey(IdGenerate record);
}