package com.csair.admin.core.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.Warehouse;
import com.csair.admin.core.po.WarehouseQueryObject;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.WarehouseService;
import com.csair.admin.util.UserUtil;

/**
 * 信息管理，关联仓库，用来打印的
 * @author: Yin ShiHua
 */
@Controller
@RequestMapping("ware")
public class WarehouseController {
    private static Logger logger = LoggerFactory.getLogger(WarehouseController.class);
    
    @Autowired
    private WarehouseService warehouseService;
    @RequestMapping("list")
    public String list(Warehouse qo, Model model) {
    	Warehouse ware = warehouseService.selectWareByUserId();
    	if(ware!=null){
    		model.addAttribute("ware", ware);
    	}else{
    		model.addAttribute("ware", new Warehouse());
    	}
        return "/warehouse/warehouse";
    }
    
    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage update(@RequestBody Warehouse qo) {
        ResponseMessage result = new ResponseMessage();
        try {
            if (qo.getId() != null) {
            	int ret = warehouseService.updateWare(qo);
            	if(ret==1){
            		  result.setCode(200);
            	}
            } else {
            	int ret = warehouseService.insertWare(qo);
            	if(ret==1){
          		  result.setCode(200);
            	}
            }
        } catch (Exception e) {
            logger.error("{新增修改异常信息}", e);
            result.setCode(0);
            return result;
        }
        return result;
    }
}
