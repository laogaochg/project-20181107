package com.csair.admin.core.controller;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dto.MonthYieldDto;
import com.csair.admin.core.dto.StockQueryObject;
import com.csair.admin.core.po.MonthYield;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.po.core.User;
import com.csair.admin.core.service.MonthYieldService;
import com.csair.admin.core.vo.MonthYieldVo;
import com.csair.admin.util.HttpUtils;
import com.csair.admin.util.ParamConstants;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/6 16:23
 */
@Controller
@RequestMapping("monthYield")
public class MonthYieldController {
    private static Logger logger = LoggerFactory.getLogger(MonthYieldController.class);
    @Autowired
    private MonthYieldService monthYieldService;

    @RequestMapping("list")
    public String list(MonthYieldVo qo, Model model) {
        List<MonthYieldVo> vos = monthYieldService.pageQuery(qo);
        changeDate(vos);
        User user = (User) SecurityUtils.getSubject().getSession().getAttribute(ParamConstants.USER_SESSION);
        model.addAttribute("shopId", user.getShopId());
        model.addAttribute("pageResult",vos );
        model.addAttribute("qo", qo);
        return "/monthYield/monthYield";
    }

    private void changeDate(List<MonthYieldVo> vos) {
        for (MonthYieldVo vo : vos) {
            MonthYieldDto y = vo.getMonthYieldDto();
            if(y !=null){
                MonthYield m = y.getMonthYield();
                if(m !=null){
//                    m.setBeforeMonthNotOutCount();
                }
            }
        }

    }

    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage list(@RequestBody MonthYieldVo qo) {
        ResponseMessage result = new ResponseMessage();
        result.setCode(200);
        logger.info("update monthYield：参数" + JSON.toJSONString(qo));
        try {
            monthYieldService.update(qo);
        } catch (Exception e) {
            result.setCode(0);
            if (e instanceof PlatformException) {
                result.setMsg(((PlatformException) e).getReturnMsg());
            } else {
                logger.error("{}", e);
                result.setMsg("请输入正确参数!");
            }
            return result;
        }
        return result;
    }

    @RequestMapping(value = "/exportFile")
    public void export(String month, HttpServletResponse response) throws Exception {
        OutputStream os = response.getOutputStream();
        HSSFWorkbook wb;
        try {
            wb = monthYieldService.exportMonthYield(month);
        } catch (Exception e) {
            os.write("导出失败，请联系相关人员处理。".getBytes());
            os.flush();
            os.close();
            return;
        }
        //响应到客户端
        HttpUtils.setResponseHeader(response, "产量表.xls");
        wb.write(os);
        os.flush();
        os.close();
    }


}
