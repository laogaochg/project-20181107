package com.csair.admin.core.controller;

import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dto.OrderVo;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.OrderQueryObject;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.po.core.User;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.util.ExcelUtil;
import com.csair.admin.util.HttpUtils;
import com.csair.admin.util.ParamConstants;

import com.csair.admin.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/11 0:01
 * 订单
 */
@Controller
@RequestMapping("order")
public class OrderController {

    private static Logger logger = LoggerFactory.getLogger(AssemblyController.class);
    @Autowired
    private OrderService orderService;
    @Autowired
    private CompanyService companyService;

    @RequestMapping("list")
    public String list(OrderQueryObject qo, Model model) {
    	 Subject currentUser = SecurityUtils.getSubject();
         User user = (User) currentUser.getSession().getAttribute(ParamConstants.USER_SESSION);
         model.addAttribute("shopId", user.getShopId());
        PageResult<OrderVo> data = orderService.pageQuery(qo);
        changeData(data);
        model.addAttribute("pageResult", data);
        model.addAttribute("qo", qo);
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        model.addAttribute("qo", qo);
        model.addAttribute("companyList", companyService.pageQuery(qoo).getListData());
        return "/order/list";
    }

    private void changeData(PageResult<OrderVo> data) {
        for (OrderVo v : data.getListData()) {
            v.setMoney(StringUtil.to3Point(v.getMoney()));
            v.setInputCount(StringUtil.to0Point(v.getInputCount()));
            v.setOutCount(StringUtil.to0Point(v.getOutCount()));
            v.setResertCount(StringUtil.to0Point(v.getResertCount()));
        }
    }

    @RequestMapping("exportAccountStatement")
    public void exportAccountStatement(OrderQueryObject qo,  HttpServletResponse response) {
        HSSFWorkbook wb = orderService.exportAccountStatement(qo);
        //响应到客户端
        try {
            HttpUtils.setResponseHeader(response, "对账单.xls");
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @RequestMapping("getByCondition")
    @ResponseBody
    public List<AssemblyVo> getByCondition(AssemblyQueryObject qo) {
//        List<AssemblyVo> list = orderService.pageQuery(qo).getListData();
//        if (list.size() > 0) return list;
        List<AssemblyVo> result = new ArrayList<>();
        return result;
    }

}
