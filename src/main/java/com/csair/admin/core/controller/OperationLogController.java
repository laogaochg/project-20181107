package com.csair.admin.core.controller;

import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.OperationLog;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.resp.DatagridForLayUI;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.vo.OperationLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csair.admin.core.po.core.query.OperationLogQueryObject;
import com.csair.admin.core.service.OperationLogService;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * laogaochg
 * 2017/7/11.
 */
@Controller
@RequestMapping("log")
public class OperationLogController {
    @Resource
    private OperationLogService operationLogService;


    @RequestMapping("list")
    public String list(OperationLogQueryObject qo, Model model) {
        model.addAttribute("pageResult", operationLogService.pageQuery(qo));
        model.addAttribute("qo", qo);
        return "/log/list";
    }

    @RequestMapping("getByCondition")
    @ResponseBody
    public List<OperationLog> getByCondition(OperationLogQueryObject qo) {
        List<OperationLog> list = operationLogService.pageQuery(qo).getListData();
        if (list.size() > 0) return list;
        List<OperationLog> result = new ArrayList<>();
        return result;
    }
}



