package com.csair.admin.core.controller;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dto.InputFileDataDto;
import com.csair.admin.core.dto.StorageInputQueryObject;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.vo.StorageInputVo;
import com.csair.admin.util.ExcelUtil;
import com.csair.admin.util.HttpUtils;
import com.csair.admin.util.StringUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:31
 * 入库
 */
@Controller
@RequestMapping("StorageInput")
public class StorageInputController {
    private static Logger logger = LoggerFactory.getLogger(AssemblyController.class);
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private StorageInputService storageInputService;
    @Autowired
    private AssemblyDao assemblyDao;

    @RequestMapping("list")
    public String list(StorageInputQueryObject qo, Model model) {
        PageResult<StorageInputVo> data = storageInputService.pageQuery(qo);
        changeData(data);
        model.addAttribute("pageResult", data);
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        model.addAttribute("qo", qo);
        model.addAttribute("companyList", companyService.pageQuery(qoo).getListData());
        return "/StorageInput/list";
    }

    private void changeData(PageResult<StorageInputVo> data) {
        for (StorageInputVo v : data.getListData()) {
            v.setExpressId(StringUtil.to2Point(v.getExpressId()));
            v.setPoint(StringUtil.to2Point(v.getPoint()));
        }
    }

    @RequestMapping("queryStorageInput")
    @ResponseBody
    public Object queryStorageInput(StorageInputQueryObject qo) {
        qo.setPageSize(9999);
        return storageInputService.pageQuery(qo);
    }

    /**
     * 导出报表
     *
     * @return
     */
    @RequestMapping(value = "/export")
    public void export(StorageInputQueryObject qo, HttpServletResponse response) throws Exception {
        qo.setCurrentPage(1);
        qo.setPageSize(99999);
        if (qo.getAssemblyType() == null) {
            qo.setAssemblyType(3);
        }
        qo.setIsExportFile(true);
        //创建HSSFWorkbook
        InputFileDataDto dto = new InputFileDataDto();
        dto.setRow1Value("入库记录");
        dto.setEnd1("制表人账号：" + UserUtil.getUser().getEmail());
        dto.setEnd3("");
        qo.setPageSize(9999999);
        List<StorageInputVo> list = storageInputService.pageQuery(qo).getListData();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        List<List<Object>> data = new ArrayList<>();
        String companyName = "";
//        "入库单,名称,规格型号,编码,单位,数量,备注".split(",");
//        "入库单,名称,规格型号,单位,数量,原始点数,出库点数,单价,备注".split(",");
        for (StorageInputVo a : list) {
            List<Object> row = new ArrayList<>();
//            row.add(f.format(a.getInputStorageTime()));
            row.add(a.getDeliveryNumber());
            Assembly assembly = a.getAssembly();
            if (assembly != null) {
                row.add(assembly.getName());
                row.add(assembly.getModelName());
                if (Integer.valueOf(3).equals(qo.getAssemblyType())) {
                } else {
                    row.add(assembly.getCode());
                }
            } else {
                row.add("");
                row.add("");
                row.add("");
            }
            row.add("PCS");
            row.add(new BigDecimal(a.getCount()));
            if (Integer.valueOf(3).equals(qo.getAssemblyType())) {
                StorageInputAssembly in = a.getStorageInputAssembly();
                row.add(new BigDecimal(StringUtil.to2Point(in.getPoint())));
                row.add(new BigDecimal(StringUtil.to2Point(in.getExpressId())));
                row.add(new BigDecimal(StringUtil.to6Point(in.getPrice())));
            }
            row.add(a.getRemark());
            data.add(row);

        }
        dto.setIsProduct(Integer.valueOf(3).equals(qo.getAssemblyType()));
        dto.setEnd2("公司：" + companyName);
        dto.setData(data);
        HSSFWorkbook wb = ExcelUtil.getInputStorageDtoHSSFWorkbook(dto);

        //响应到客户端
        try {
            HttpUtils.setResponseHeader(response, "入库记录.xls");
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage update(@RequestBody HashMap<String, String> vo) {
        ResponseMessage result = new ResponseMessage();
        result.setCode(200);
        String error = null;
//        if (StringUtils.isBlank(vo.get("inputStorageTime"))) error = "入库时间不能为空";
        if (vo.get("companyId") == null) error = "货源公司不能为空";
//        if (vo.getAssemblyPrice() == null) error = "不能没有入库材料";
//        if (vo.getCount() == null) error = "不能没有入库材料";
//        if (vo.getAssemblyId() == null) error = "不能没有入库材料";
//        if (vo.getStandard() == null) error = "不能没有入库材料";
//        if (vo.getUnit() == null) error = "不能没有入库材料";
//        if (vo.getUnitPointCount() == null) error = "不能没有入库材料";
        if (error != null) {
            result.setCode(0);
            result.setMsg(error);
            return result;
        }
        try {
            //参数检验
            if (StringUtils.isBlank(vo.get("id"))) {
                storageInputService.insert(vo);
            } else {
                storageInputService.update(vo);
            }
        } catch (Exception e) {
            result.setCode(0);
            if (e instanceof PlatformException) {
                result.setMsg(((PlatformException) e).getReturnMsg());
            } else {
                logger.error("{}", e);
                result.setMsg("请输入正确参数!");
            }
            return result;
        }
        return result;
    }

    @RequestMapping("toEdit")
    public String toEdit(StorageInputQueryObject qo, Model model) {
        if (qo.getId() != null) {
            StorageInputAssemblyQuery example = new StorageInputAssemblyQuery();
            example.createCriteria().andIdEqualTo(qo.getId()).andStorageIdEqualTo(UserUtil.getUserStorageId());
            List<StorageInputAssembly> list = storageInputService.selectByExample(example);
            if (list.size() > 0) {
                StorageInputVo vo = new StorageInputVo(list.get(0));
                StorageInputQueryObject qoo = new StorageInputQueryObject();
                qoo.setPageSize(999);
                qoo.setInputStorageNumber(list.get(0).getInputStorageNumber());
                model.addAttribute("entity", vo);
                model.addAttribute("selectAssemblySJON", JSON.toJSONString(storageInputService.pageQuery(qoo).getListData()));
            }
        }
        AssemblyQuery condition = new AssemblyQuery();
        condition.setPageSize(999);
//        condition.createCriteria().andTypeIn(new ArrayList<>(Arrays.asList(0, 2)));
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        PageResult query = companyService.pageQuery(qoo);
        model.addAttribute("companyList", query.getListData());
        model.addAttribute("assemblyList", assemblyService.queryAllName(null));
        return "/StorageInput/edit";
    }

    @RequestMapping("toEditProduct")
    public String toEditProduct(StorageInputQueryObject qo, Model model) {
        if (qo.getId() != null) {
            StorageInputAssemblyQuery example = new StorageInputAssemblyQuery();
            example.createCriteria().andIdEqualTo(qo.getId()).andStorageIdEqualTo(UserUtil.getUserStorageId());
            List<StorageInputAssembly> list = storageInputService.selectByExample(example);
            if (list.size() > 0) {
                StorageInputVo vo = new StorageInputVo(list.get(0));
                StorageInputQueryObject qoo = new StorageInputQueryObject();
                qoo.setPageSize(999);
                qoo.setInputStorageNumber(list.get(0).getInputStorageNumber());
                model.addAttribute("entity", vo);
                model.addAttribute("selectAssemblySJON", JSON.toJSONString(storageInputService.pageQuery(qoo).getListData()));
            }
        }
        AssemblyQuery condition = new AssemblyQuery();
        condition.setPageSize(999);
//        condition.createCriteria().andTypeIn(new ArrayList<>(Arrays.asList(0, 2)));
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        PageResult query = companyService.pageQuery(qoo);
        model.addAttribute("companyList", query.getListData());
        model.addAttribute("assemblyList", assemblyService.queryAllName(null));
        return "/StorageInput/editProduct";
    }

    @RequestMapping("getByCondition")
    @ResponseBody
    public List<AssemblyVo> getByCondition(AssemblyQueryObject qo) {
        List<AssemblyVo> list = assemblyService.pageQuery(qo).getListData();
        if (list.size() > 0) return list;
        List<AssemblyVo> result = new ArrayList<>();
        return result;
    }


    @RequestMapping("delete")
    @ResponseBody
    public ResponseMessage delete(String ids) {
        ResponseMessage result = new ResponseMessage();
        try {
            List<Long> idList = new ArrayList<>();
            for (String s : ids.split(",")) {
                if (StringUtils.isNumeric(s)) {
                    idList.add(Long.valueOf(s));
                }
            }
            for (Long id : idList) {
                storageInputService.deleteByPrimaryKey(id);
            }
        } catch (Exception e) {
            result.setCode(0);
            e.printStackTrace();
        }
        return result;
    }
}
