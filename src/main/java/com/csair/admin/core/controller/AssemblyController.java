package com.csair.admin.core.controller;

import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.CompanyService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:24
 */
@Controller
@RequestMapping("assembly")
public class AssemblyController {
    private static Logger logger = LoggerFactory.getLogger(AssemblyController.class);
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private AssemblyDao assemblyDao;
    @Autowired
    private CompanyService companyService;

    @RequestMapping("list")
    public String list(AssemblyQueryObject qo, Model model) {
        model.addAttribute("pageResult", assemblyService.pageQuery(qo));
        model.addAttribute("qo", qo);
        AssemblyQuery condition = new AssemblyQuery();
        condition.createCriteria().andTypeIn(new ArrayList<>(Arrays.asList(1, 2)));
        List<Assembly> childProperties = assemblyDao.selectByExample(condition);
        model.addAttribute("childProperties", childProperties);
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        model.addAttribute("qo", qo);
        model.addAttribute("companyList", companyService.pageQuery(qoo).getListData());
        return "/assembly/assembly";
    }

    @RequestMapping("queryAssembly")
    @ResponseBody
    public List<Assembly> queryAssembly(AssemblyQueryObject qo) {
        AssemblyQuery e = new AssemblyQuery();
        AssemblyQuery.Criteria c = e.createCriteria();
        //根据名字查型号
        if (StringUtils.isNotBlank(qo.getName())) {
            c.andNameEqualTo(qo.getName()).andStatusEqualTo(1).andCompanyIdEqualTo(qo.getCompanyId()).andTypeEqualTo(3);
            return assemblyService.selectByExample(e);
        }
        if (qo.getCompanyId() != null) {
            c.andCompanyIdEqualTo(qo.getCompanyId()).andStatusEqualTo(1).andTypeEqualTo(3);
            List<Assembly> list = assemblyService.selectByExample(e);
            Set<String> set = new HashSet<>();
            Iterator<Assembly> iterator = list.iterator();
            while (iterator.hasNext()) {
                Assembly next = iterator.next();
                if (!set.add(next.getName())) {
                    iterator.remove();
                }
            }
            return assemblyService.selectByExample(e);
        }
        return new ArrayList<>();
    }

    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage update(@RequestBody HashMap<String, String> map) {
        ResponseMessage result = new ResponseMessage();
        result.setCode(200);
        try {
            if (StringUtils.isNotBlank(map.get("id"))) {
                /*AssemblyQueryObject qo = new AssemblyQueryObject();
                qo.setId(map.getId());
                AssemblyVo o = (AssemblyVo) assemblyService.pageQuery(qo).getListData().get(0);
                map.setCreateTime(o.getCreateTime());
                Assembly assemblyAssembly = map.getAssembly();
                assemblyAssembly.setCreateTime(o.getCreateTime());
                //编辑的时候不要把加个删除了
                if (map.getUpdateAction() == 0) {
                    map.setOutPoint(o.getOutPoint());
                    map.setOutPointPrice(o.getOutPointPrice());
                }*/
                assemblyService.updateByPrimaryKey(map);
            } else {
                assemblyService.insert(map);
            }
        } catch (Exception e) {
            if (e instanceof PlatformException) {
                result.setMsg(((PlatformException) e).getReturnMsg());
            } else {
                logger.error("{}", e);
            }
            result.setCode(0);
            return result;
        }
        return result;
    }

    @RequestMapping("getByCondition")
    @ResponseBody
    public List<AssemblyVo> getByCondition(AssemblyQueryObject qo) {
        List<AssemblyVo> list = assemblyService.pageQuery(qo).getListData();
        if (list.size() > 0) return list;
        List<AssemblyVo> result = new ArrayList<>();
        return result;
    }

    @RequestMapping("delete")
    @ResponseBody
    public ResponseMessage delete(String ids) {
        ResponseMessage result = new ResponseMessage();
        try {
            List<Long> idList = new ArrayList<>();
            for (String s : ids.split(",")) {
                if (StringUtils.isNumeric(s)) {
                    idList.add(Long.valueOf(s));
                }
            }
            for (Long id : idList) {
                assemblyService.deleteByPrimaryKey(id);
            }
        } catch (Exception e) {
            result.setCode(0);
            e.printStackTrace();
        }
        return result;
    }

}
