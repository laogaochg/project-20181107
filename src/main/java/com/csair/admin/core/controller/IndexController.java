package com.csair.admin.core.controller;

import javax.annotation.Resource;

import com.csair.admin.util.HttpUtils;
import com.csair.admin.util.UserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csair.admin.core.service.OperationLogService;
import com.csair.admin.core.service.UserService;

/**
 * laogaochg
 * 2017/7/10.
 * 首页
 */
@Controller
public class IndexController {

    @Resource
    private UserService userService;
    @Resource
    private OperationLogService operationLogService;

    /**
     * 首页
     */
    @RequestMapping("main")
    public String index(Model model) {
        model.addAttribute("userName", UserUtil.getUser().getEmail());
        return "main";
    }
}
