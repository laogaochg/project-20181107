package com.csair.admin.core.controller;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dto.OutRecordPrintDto;
import com.csair.admin.core.dto.OutRecordPrintDto.OutRecordPrintItemDto;
import com.csair.admin.core.dto.StorageOutQueryObject;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.Warehouse;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.service.WarehouseService;
import com.csair.admin.core.vo.StorageOutRecordVo;
import com.csair.admin.util.StringUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:31
 * 出库
 */
@Controller
@RequestMapping("StorageOut")
public class StorageOutRecordController {
    private static Logger logger = LoggerFactory.getLogger(AssemblyController.class);
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private StorageOutRecordService storageOutRecordService;
    @Autowired
    private AssemblyDao assemblyDao;
    @Autowired
    private WarehouseService warehouseService;

    @RequestMapping("list")
    public String list(StorageOutQueryObject qo, Model model) {
        PageResult<StorageOutRecordVo> data = storageOutRecordService.pageQuery(qo);
        changeListData(data);
        model.addAttribute("pageResult", data);
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        model.addAttribute("qo", qo);
        model.addAttribute("companyList", companyService.pageQuery(qoo).getListData());
        return "/StorageOut/list";
    }

    private void changeListData(PageResult<StorageOutRecordVo> data) {
        for (StorageOutRecordVo s : data.getListData()) {
            StorageOutRecord o = s.getStorageOutRecord();
            if (o != null) {
                o.setOutPrice(StringUtil.to6Point(o.getOutPrice()));
                o.setTotalMoney(StringUtil.to3Point(o.getTotalMoney()));
            }
        }
    }
//    	······宏屴送货单
//            NO00001
//    客户			加工方
//    地址			地址
//    联系电话			联系电话		时间
//
//    编号	名称	规格	单位	数量	剩余	备注
//            合计
//    收货人及经手人				送货人及经手人

    @RequestMapping("print")
    public String print(String outNumber, Model model) {
        if (StringUtils.isNotBlank(outNumber)) {
            OutRecordPrintDto dto = storageOutRecordService.printOutRecord(outNumber);
            changeData(dto);
            model.addAttribute("entity", dto);
            long totalCount = 0;
            float totalResidue = (float) 0.000;
            List<OutRecordPrintItemDto> items = dto.getItems();
            Set<String> set = new HashSet<>();
            for (OutRecordPrintItemDto obj : items) {
                if (obj.getCount() != null && !"".equals(obj.getCount())) {
                    totalCount += Long.valueOf(obj.getCount());
                }

                if(obj.getStorageOutRecord()!=null){
                    String s = obj.getStorageOutRecord().getOutNumber() + "|" + obj.getStorageOutRecord().getAssemblyId();
                    if (set.add(s) && obj.getResetCount() != null && !"".equals(obj.getResetCount())) {
                        totalResidue += Float.valueOf(obj.getResetCount());
                    }
                }

            }
            model.addAttribute("totalCount", totalCount);
            model.addAttribute("totalResidue", totalResidue);
            Warehouse warehouse = warehouseService.selectWareByUserId();
            model.addAttribute("warehouse", warehouse);

            if (dto.getAssemblyType() != 3) {
                return "/StorageOut/printMaterial";
            }

        } else {
            model.addAttribute("entity", new OutRecordPrintDto());
        }
        return "/StorageOut/print";
    }

    private void changeData(OutRecordPrintDto dto) {
        if (dto.getItems() != null) {
            for (OutRecordPrintItemDto d : dto.getItems()) {
                StorageInputAssembly in = d.getStorageInputAssembly();
                if (in != null) {
                    in.setExpressId(StringUtil.to2Point(in.getExpressId()));
                }
            }
        }

    }

    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage update(@RequestBody HashMap<String, String> vo) {
        ResponseMessage result = new ResponseMessage();
        result.setCode(200);
        String error = null;
        logger.info("修改入库：参数" + JSON.toJSONString(vo));
        //{"type":"1","remark_6":"","count_7":"","remark_5":"","count_6":"","remark_4":"","count_5":"","remark_3":"","count_4":"","remark_2":"","count_3":"","remark_1":"","count_2":"","count_1":"","id":"","count_10":"","remark_9":"","remark_8":"","count_9":"","remark_7":"","count_8":"","inputStorageTime":"","companyId":"0","assemblyModelName_9":"","assemblyModelName_6":"","assemblyModelName_5":"","assemblyModelName_8":"","assemblyModelName_7":"","assemblyModelName_2":"","assemblyModelName_1":"5050-48D","assemblyModelName_4":"","assemblyModelName_3":"","waste_9":"","waste_7":"","waste_8":"","waste_5":"","waste_6":"","assemblyName_7":"","assemblyName_6":"","assemblyName_10":"","assemblyName_9":"","assemblyName_8":"","waste_10":"","assemblyModelName_10":"","waste_3":"","waste_4":"","assemblyName_1":"流星管","waste_1":"","waste_2":"","assemblyName_3":"","assemblyName_2":"","assemblyName_5":"","assemblyName_4":"","remark_10":""}
//        if (vo.getCompanyId() == null) error = "不能没有出库材料";
//        if (vo.getAssemblyPrice() == null) error = "不能没有出库材料";
//        if (vo.getCount() == null) error = "不能没有出库材料";
//        if (vo.getAssemblyId() == null) error = "不能没有出库材料";
//        if (vo.getStandard() == null) error = "不能没有出库材料";
//        if (vo.getUnit() == null) error = "不能没有出库材料";
//        if (vo.getUnitPointCount() == null) error = "不能没有出库材料";
        if (error != null) {
            result.setCode(0);
            result.setMsg(error);
            return result;
        }
        try {
//            参数检验
            if (!StringUtils.isNotBlank(vo.get("id"))) {
                storageOutRecordService.insert(vo);
            } else {
                storageOutRecordService.update(vo);
            }
        } catch (Exception e) {
            result.setCode(0);
            if (e instanceof PlatformException) {
                result.setMsg(((PlatformException) e).getReturnMsg());
            } else {
                logger.error("{}", e);
                result.setMsg("请输入正确参数!");
            }
            return result;
        }
        return result;
    }

    @RequestMapping("toEdit")
    public String toEdit(StorageOutQueryObject qo, Model model) {
        if (qo.getId() != null) {
            StorageOutRecordQuery example = new StorageOutRecordQuery();
            example.createCriteria().andIdEqualTo(qo.getId()).andStorageIdEqualTo(UserUtil.getUserStorageId());
            List<StorageOutRecord> list = storageOutRecordService.selectByExample(example);
            model.addAttribute("entity", list != null && list.size() > 0 ? list.get(0) : new StorageOutRecord());
            qo = new StorageOutQueryObject();
            qo.setPageSize(9999);
            qo.setOutNumber(list.get(0).getOutNumber());
            PageResult<StorageOutRecordVo> query = storageOutRecordService.pageQuery(qo);
            model.addAttribute("selectAssemblySJON", JSON.toJSONString(query.getListData()));
        }
        if (qo.getOrderId() != null) {
            qo.setPageSize(9999);
            PageResult<StorageOutRecordVo> query = storageOutRecordService.pageQuery(qo);
            model.addAttribute("selectAssemblySJON", JSON.toJSONString(query.getListData()));
        }
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        PageResult query = companyService.pageQuery(qoo);
        model.addAttribute("companyList", query.getListData());
        model.addAttribute("assemblyList", assemblyService.queryAllName(null));
        return "/StorageOut/edit";
    }

    @RequestMapping("toEditProduct")
    public String toEditProduct(StorageOutQueryObject qo, Model model) {
        if (qo.getId() != null) {
            StorageOutRecordQuery example = new StorageOutRecordQuery();
            example.createCriteria().andIdEqualTo(qo.getId()).andStorageIdEqualTo(UserUtil.getUserStorageId());
            List<StorageOutRecord> list = storageOutRecordService.selectByExample(example);
            model.addAttribute("entity", list != null && list.size() > 0 ? list.get(0) : new StorageOutRecord());
            qo = new StorageOutQueryObject();
            qo.setPageSize(9999);
            qo.setOutNumber(list.get(0).getOutNumber());
            PageResult<StorageOutRecordVo> query = storageOutRecordService.pageQuery(qo);
            model.addAttribute("selectAssemblySJON", JSON.toJSONString(query.getListData()));
        }
        if (qo.getOrderId() != null) {
            qo.setPageSize(9999);
            PageResult<StorageOutRecordVo> query = storageOutRecordService.pageQuery(qo);
            model.addAttribute("selectAssemblySJON", JSON.toJSONString(query.getListData()));
        }
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        PageResult query = companyService.pageQuery(qoo);
        model.addAttribute("companyList", query.getListData());
        model.addAttribute("assemblyList", assemblyService.queryAllName(null));
        return "/StorageOut/editProduct";
    }

    @RequestMapping("getByCondition")
    @ResponseBody
    public List<AssemblyVo> getByCondition(AssemblyQueryObject qo) {
        List<AssemblyVo> list = assemblyService.pageQuery(qo).getListData();
        if (list.size() > 0) return list;
        List<AssemblyVo> result = new ArrayList<>();
        return result;
    }


    @RequestMapping("delete")
    @ResponseBody
    public ResponseMessage delete(String ids) {
        ResponseMessage result = new ResponseMessage();
        try {
            List<Long> idList = new ArrayList<>();
            for (String s : ids.split(",")) {
                if (StringUtils.isNumeric(s)) {
                    idList.add(Long.valueOf(s));
                }
            }
            for (Long id : idList) {
                storageOutRecordService.deleteByPrimaryKey(id);
            }
        } catch (Exception e) {
            result.setCode(0);
            e.printStackTrace();
        }
        return result;
    }
}
