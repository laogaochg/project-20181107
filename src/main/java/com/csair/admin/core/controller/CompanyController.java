package com.csair.admin.core.controller;

import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.service.CompanyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:24
 */
@Controller
@RequestMapping("company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @RequestMapping("list")
    public String list(CompanyQueryObject qo, Model model) {
        model.addAttribute("pageResult", companyService.pageQuery(qo));
        model.addAttribute("qo", qo);
        return "/company/company";
    }

    @RequestMapping("update")
    @ResponseBody
    public ResponseMessage update(Company company) {
        ResponseMessage result = new ResponseMessage();
        result.setCode(200);
        try {
            if (company.getId() != null) {
                CompanyQueryObject qo = new CompanyQueryObject();
                qo.setId(company.getId());
                Company o = (Company) companyService.pageQuery(qo).getListData().get(0);
                company.setCreateDate(o.getCreateDate());
                companyService.updateByPrimaryKey(company);
            } else {
                companyService.insert(company);
            }
        } catch (Exception e) {
            result.setCode(0);
            return result;
        }
        return result;
    }

    @RequestMapping("getByCondition")
    @ResponseBody
    public List<Company> getByCondition(CompanyQueryObject qo) {
        List<Company> list = companyService.pageQuery(qo).getListData();
        if (list.size() > 0) return list;
        List<Company> result = new ArrayList<>();
        return result;
    }

    @RequestMapping("delete")
    @ResponseBody
    public ResponseMessage delete(String ids) {
        ResponseMessage result = new ResponseMessage();
        try {
            List<Long> idList = new ArrayList<>();
            for (String s : ids.split(",")) {
                if (StringUtils.isNumeric(s)) {
                    idList.add(Long.valueOf(s));
                }
            }
            for (Long id : idList) {
                companyService.deleteByPrimaryKey(id);
            }
        } catch (Exception e) {
            result.setCode(0);
            e.printStackTrace();
        }
        return result;
    }

}
