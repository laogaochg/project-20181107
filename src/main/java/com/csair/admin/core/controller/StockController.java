package com.csair.admin.core.controller;

import com.csair.admin.core.dto.StockQueryObject;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.service.StockService;
import com.csair.admin.util.HttpUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/22 0:22
 */
@Controller
@RequestMapping("stock")
public class StockController {
    @Autowired
    private StockService stockService;
    @Autowired
    private CompanyService companyService;

    @RequestMapping("list")
    public String list(StockQueryObject qo, Model model) {

        model.addAttribute("pageResult", stockService.pageQuery(qo));
        CompanyQueryObject qoo = new CompanyQueryObject();
        qoo.setPageSize(9999);
        model.addAttribute("qo", qo);
        model.addAttribute("companyList", companyService.pageQuery(qoo).getListData());
        return "/stock/list";
    }

    @RequestMapping(value = "/exportFile")
    public void export(StockQueryObject qo, HttpServletResponse response) throws Exception {
        XSSFWorkbook wb = stockService.exportFile(qo);
        //响应到客户端
        try {
            HttpUtils.setResponseHeader(response, "盘存纪录.xls");
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
