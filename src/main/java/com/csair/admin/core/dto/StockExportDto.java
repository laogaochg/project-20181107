package com.csair.admin.core.dto;

import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/22 13:44
 */
@Data
public class StockExportDto {
    /**
     * 出库或入库id
     */
    private String orderId;
    private Date date;
    private Map<String,String> assemblyMap = new HashMap<>();

    public StockExportDto() {
    }

}
