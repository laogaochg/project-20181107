package com.csair.admin.core.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/5 0:47
 */
@Data
public class InputFileDataDto {
    private String row1Value;
    private String row2Value;
    private List<List<Object>> data;
    private String end1;
    private String end2;
    private String end3;
    private Boolean isProduct;
}
