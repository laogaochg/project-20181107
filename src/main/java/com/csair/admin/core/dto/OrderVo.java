package com.csair.admin.core.dto;

import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.OrderPo;
import com.csair.admin.core.po.StorageOutRecord;
import lombok.Data;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/11 0:20
 */
@Data
public class OrderVo extends OrderPo {

    private String assemblyName;
    private String assemblyCode;
    private String companyName;
    /**
     * 已经出库的金额
     */
    private String money;
    /**
     * 出库记录
     */
    private List<StorageOutRecord> outList;

    private Assembly assembly;

    public OrderVo() {
    }

    public OrderVo(OrderPo b) {
        OrderVo a = this;
        a.setAssemblyId(b.getAssemblyId());
        a.setCompanyId(b.getCompanyId());
        a.setCreateTime(b.getCreateTime());
        a.setId(b.getId());
        a.setOrderId(b.getOrderId());
        a.setInputCount(b.getInputCount());
        a.setOutCount(b.getOutCount());
        a.setPaidCount(b.getPaidCount());
        a.setRemark(b.getRemark());
        a.setResertCount(b.getResertCount());
        a.setStorageId(b.getStorageId());
        a.setTotalMoney(b.getTotalMoney());
        a.setType(b.getType());
        a.setUpdateTime(b.getUpdateTime());
        a.setWasteCount(b.getWasteCount());
        a.setShouldGetCount(b.getShouldGetCount());
    }
}
