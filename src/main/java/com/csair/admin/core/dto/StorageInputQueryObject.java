package com.csair.admin.core.dto;

import com.csair.admin.core.po.BaseQueryObject;
import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 14:04
 */
@Data
public class StorageInputQueryObject extends BaseQueryObject {
    private Long id;
    private Integer type;

    private String orderId;

    private String beginDate;
    private String endDate;
    private Long companyId;
    private String expressId;
    private String inputStorageNumber;
    /**
     * 名字
     */
    private String assemblyName;
    /**
     * 客户单号
     */
    private String customOrderId;
    private Integer assemblyType;
    private Boolean isExportFile;


}
