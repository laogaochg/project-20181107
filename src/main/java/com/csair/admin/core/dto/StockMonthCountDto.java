package com.csair.admin.core.dto;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/21 9:20
 */
@Data
public class StockMonthCountDto {
    private BigDecimal outCount = BigDecimal.ZERO;
    private BigDecimal inputCount = BigDecimal.ZERO;
}
