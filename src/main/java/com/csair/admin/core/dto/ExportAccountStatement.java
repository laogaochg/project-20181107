package com.csair.admin.core.dto;

import com.csair.admin.core.po.Assembly;
import lombok.Data;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/16 20:23
 */
@Data
public class ExportAccountStatement {
    private String orderName;
    private String orderId;
    private String companyName;
    private String companyAddress;
    private String companyPhone;
    private String processingPartyName;
    private String processingPartyAddress;
    private String processingPartyPhone;
    private String date;
    private String totalCount;
    private List<OutRecordPrintDto.OutRecordPrintItemDto> items;

    @Data
    public static class OutRecordPrintItemDto {
        private String code;
        private String name;
        private String standard;
        private String unit;
        private String count;
        private String resetCount;
        private String remark;

        public OutRecordPrintItemDto(Assembly a, String count, String resetCount, String remark) {
            this.code = a.getCode();
            this.name = a.getName();
            this.standard = a.getModelName();
            this.unit = a.getUnit();
            this.remark = remark;
            this.resetCount = resetCount;
            this.count = count;
        }

        public OutRecordPrintItemDto() {
        }

    }

}
