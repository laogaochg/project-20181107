package com.csair.admin.core.dto;

import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/8 11:37
 */
@Data
public class InsertStorageInputDto {
    private String name;
    private String modelName;
    private String count;
    private String point;//点数
    private String expressId;
    /**
     * 客户单号
     */
    private String customOrderId;
    private String price;

    public InsertStorageInputDto(String name, String modelName, String count,
                                 String point,String customOrderId
            ,String expressId
            ,String price
    ) {
        this.name = name;
        this.modelName = modelName;
        this.count = count;
        this.point = point;
        this.customOrderId = customOrderId;
        this.expressId = expressId;
        this.price = price;
    }
}
