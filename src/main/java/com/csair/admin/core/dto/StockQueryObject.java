package com.csair.admin.core.dto;

import com.csair.admin.core.po.BaseQueryObject;
import com.csair.admin.core.po.core.query.QueryObject;
import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/21 0:36
 */
@Data
public class StockQueryObject extends BaseQueryObject {
    private String beginDate;
    private String endDate;
    private String name;
}
