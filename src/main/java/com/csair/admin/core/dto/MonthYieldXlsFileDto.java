package com.csair.admin.core.dto;

import com.csair.admin.core.vo.MonthYieldVo;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/7 22:32
 */
@Data
public class MonthYieldXlsFileDto {
    //主板名称	出货数量	单价	减上月未出货数量	加本月未出货数量	实际数量
    private String code;//主板名称
    private Double count;//出货数量
    private Double price;//单价
    private Double beforeCount;//减上月未出货数量
    private Double currentCount;//加本月未出货数量
    private Double actualCount;//实际数量
    // 原始点数	出货点数	原始点数合计	出货点数合计
    private Double avgInputPoint;//原始点数
    private Double avgOutPoint;//出货点数
    private Double totalInputPoint;//原始点数合计
    private Double totalOutPoint;//出货点数合计
    private Double money;//金额
    private String remark;

    //         备注
    public MonthYieldXlsFileDto(MonthYieldVo vo) {
        code = vo.getAssembly().getName();
        count = Double.valueOf(vo.getMonthYieldDto().getTotalCount());
        price = Double.valueOf(vo.getMonthYieldDto().getPrice());
        actualCount = Double.valueOf(vo.getMonthYieldDto().getTotalActualCount());
        avgInputPoint = Double.valueOf(vo.getMonthYieldDto().getAvgInputPoint());
        avgOutPoint = Double.valueOf(vo.getMonthYieldDto().getAvgOutPoint());
        totalInputPoint = Double.valueOf(vo.getMonthYieldDto().getTotalInputPoint());
        totalOutPoint = Double.valueOf(vo.getMonthYieldDto().getTotalOutPoint());
        beforeCount = vo.getMonthYieldDto().getMonthYield() != null ? Double.valueOf(vo.getMonthYieldDto().getMonthYield().getBeforeMonthNotOutCount()) : 0;
        currentCount = vo.getMonthYieldDto().getMonthYield() != null ? Double.valueOf(vo.getMonthYieldDto().getMonthYield().getCurrentMonthNotOutCount()) : 0;
        money = Double.valueOf(vo.getMonthYieldDto().getTotalMoney());
        if (vo.getMonthYieldDto().getMonthYield() == null ) {
            remark = "未设置上月未出货数量和本月未出货数量，暂时取0";
        }
    }

    public List<Object> getRowData() {
        List<Object> result = new ArrayList<>();
        result.add(code);
        result.add("PCS");
        result.add(count);
        result.add(beforeCount);
        result.add(currentCount);
        result.add(actualCount);
        result.add(price);
        result.add(avgInputPoint);
        result.add(avgOutPoint);
        result.add(totalInputPoint);
        result.add(totalOutPoint);
        result.add(money);
        if (remark != null) result.add(remark);
        return result;
    }

    public static List<String> getTitle() {
        String[] strings = {
                "主板名称"
                , "单位"
                , "出货数量"
                , "上月未出货数量"
                , "本月未出货数量"
                , "实际数量"
                , "单价"
                , "原始点数"
                , "出货点数"
                , "原始点数合计"
                , "出货点数合计"
                , "金额"
        };
        List<String> result = new ArrayList<>(Arrays.asList(strings));
        return result;
    }

    public MonthYieldXlsFileDto() {
    }
}
