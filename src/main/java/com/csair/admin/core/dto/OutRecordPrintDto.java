package com.csair.admin.core.dto;

import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageOutRecord;
import lombok.Data;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/16 16:46
 */
@Data
public class OutRecordPrintDto {
    private String orderName;
    private String orderId;
    private String companyName;
    private String companyAddress;
    private String companyPhone;
    private String processingPartyName;
    private String processingPartyAddress;
    private String processingPartyPhone;
    private String date;
    private String totalCount;
    private Integer assemblyType;
    /**
     * 客户公司
     */
    private Company company;
    private List<OutRecordPrintItemDto> items;

    @Data
    public static class OutRecordPrintItemDto {
        private String code;
        private String name;
        private String standard;
        private String unit;
        private String count;
        private String resetCount;
        private String remark;
        private Assembly assembly;
        private StorageOutRecord storageOutRecord;
        private StorageInputAssembly storageInputAssembly;


        public OutRecordPrintItemDto(Assembly a, StorageOutRecord o, String resetCount) {
            this.code = a.getCode();
            this.name = a.getName();
            this.standard = a.getModelName();
            this.unit = a.getUnit();
            this.remark = o.getRemark();
            this.resetCount = resetCount;
            this.count = o.getCount();
            this.assembly = a;
            this.storageOutRecord = o;
        }

        public OutRecordPrintItemDto() {
        }

    }


}
