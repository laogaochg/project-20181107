package com.csair.admin.core.dto;

import com.csair.admin.core.po.StorageInput;
import com.csair.admin.core.po.StorageInputAssembly;

import java.util.Date;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:37
 */
public class StorageInputDto {

    private Long id;

    private Date createDate;

    /**
     * 入库单号
     */
    private String expressId;

    /**
     * 进货总价
     */
    private String totalPrice;

    /**
     * 总点数
     */
    private String totalPoint;

    /**
     * 总数量
     */
    private String totalCount;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 状态:0有效,1已经删除,2无效
     */
    private Integer state;

    /**
     * 对应订单号(父)
     */
    private String parentOrderId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 操作人id
     */
    private Long inputerId;

    private List<StorageInputAssembly> storageInputAssemblyList;

    private static final long serialVersionUID = 1L;

    public StorageInputDto() {
    }

    public StorageInputDto(StorageInput a) {
        this.companyId = a.getCompanyId();
        this.createDate = a.getCreateDate();
        this.expressId = a.getExpressId();
        this.id = a.getId();
        this.inputerId = a.getInputerId();
        this.parentOrderId = a.getParentOrderId();
        this.state = a.getState();
        this.storageId = a.getStorageId();
        this.totalCount = a.getTotalCount();
        this.totalPoint = a.getTotalPoint();
        this.totalPrice = a.getTotalPrice();
    }

    public StorageInput getStorageInput() {
        StorageInput result = new StorageInput();
        result.setCompanyId(this.getCompanyId());
        result.setCreateDate(this.getCreateDate());
        result.setExpressId(this.getExpressId());
        result.setId(this.getId());
        result.setInputerId(this.getInputerId());
        result.setParentOrderId(this.getParentOrderId());
        result.setState(this.getState());
        result.setStorageId(this.getStorageId());
        result.setTotalCount(this.getTotalCount());
        result.setTotalPoint(this.getTotalPoint());
        result.setTotalPrice(this.getTotalPrice());
        return result;
    }

    public List<StorageInputAssembly> getStorageInputAssemblyList() {
        return storageInputAssemblyList;
    }

    public void setStorageInputAssemblyList(List<StorageInputAssembly> storageInputAssemblyList) {
        this.storageInputAssemblyList = storageInputAssemblyList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getExpressId() {
        return expressId;
    }

    public void setExpressId(String expressId) {
        this.expressId = expressId == null ? null : expressId.trim();
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice == null ? null : totalPrice.trim();
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint == null ? null : totalPoint.trim();
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount == null ? null : totalCount.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getParentOrderId() {
        return parentOrderId;
    }

    public void setParentOrderId(String parentOrderId) {
        this.parentOrderId = parentOrderId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getInputerId() {
        return inputerId;
    }

    public void setInputerId(Long inputerId) {
        this.inputerId = inputerId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createDate=").append(createDate);
        sb.append(", expressId=").append(expressId);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", totalPoint=").append(totalPoint);
        sb.append(", totalCount=").append(totalCount);
        sb.append(", storageId=").append(storageId);
        sb.append(", state=").append(state);
        sb.append(", parentOrderId=").append(parentOrderId);
        sb.append(", companyId=").append(companyId);
        sb.append(", inputerId=").append(inputerId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

}
