package com.csair.admin.core.dto;

import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.MonthYield;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/6 15:03
 */
@Data
public class MonthYieldDto {
    private MonthYield monthYield;
    private List<StorageOutRecord> outList = new ArrayList<>();
    private List<StorageInputAssembly> inputList = new ArrayList<>();


    private BigDecimal totalInputPoint() {
        BigDecimal t = BigDecimal.ZERO;
        for (int i = 0; i < inputList.size(); i++) {
            StorageInputAssembly input = inputList.get(i);
            StorageOutRecord out = outList.get(i);
            t = t.add(new BigDecimal(out.getCount()).multiply(new BigDecimal(input.getPoint())));
        }
        return t;
    }

    private BigDecimal totalOutPoint() {
        BigDecimal t = BigDecimal.ZERO;
        for (int i = 0; i < outList.size(); i++) {
            StorageOutRecord out = outList.get(i);
            t = t.add(new BigDecimal(out.getCount()).multiply(new BigDecimal(out.getOutPoint())));
        }
        return t;
    }

    private BigDecimal totalMoney() {
        BigDecimal t = BigDecimal.ZERO;
        for (int i = 0; i < outList.size(); i++) {
            StorageOutRecord out = outList.get(i);
            t = t.add(new BigDecimal(out.getCount())
                    .multiply(new BigDecimal(out.getOutPoint()))
                    .multiply(new BigDecimal(out.getOutPrice()))
            );
        }
        return t;
    }

    private BigDecimal totalCount() {
        BigDecimal totalCount = BigDecimal.ZERO;
        for (StorageOutRecord o : outList) {
            totalCount = totalCount.add(new BigDecimal(o.getCount()));
        }
        return totalCount;
    }

    private BigDecimal totalActualCount() {
        BigDecimal totalCount = totalCount();
        if (monthYield != null && monthYield.getCurrentMonthNotOutCount() != null && monthYield.getBeforeMonthNotOutCount() != null) {
            totalCount = totalCount
                    .subtract(new BigDecimal(monthYield.getBeforeMonthNotOutCount()))
                    .add(new BigDecimal(monthYield.getCurrentMonthNotOutCount()));
        }
        return totalCount;
    }
    public String getTotalActualCount() {
        return totalActualCount().setScale(2, BigDecimal.ROUND_UP).toString();
    }
    public String getPrice() {
        return totalMoney().divide(totalActualCount(), 6, BigDecimal.ROUND_UP).setScale(6, BigDecimal.ROUND_UP).toString();
    }

    public String getTotalMoney() {
        return totalMoney().setScale(3, BigDecimal.ROUND_UP).toString();
    }

    public String getTotalCount() {
        return totalCount().setScale(2, BigDecimal.ROUND_UP).toString();
    }

    public String getTotalInputPoint() {
        return totalInputPoint().setScale(2, BigDecimal.ROUND_UP).toString();
    }

    public String getAvgInputPoint() {
        return totalInputPoint().divide(totalActualCount(), 6, BigDecimal.ROUND_UP).setScale(2, BigDecimal.ROUND_UP).toString();
    }

    public String getTotalOutPoint() {
        return totalOutPoint().setScale(2, BigDecimal.ROUND_UP).toString();
    }

    public String getAvgOutPoint() {
        return totalOutPoint().divide(totalActualCount(), 6, BigDecimal.ROUND_UP).setScale(2, BigDecimal.ROUND_UP).toString();
    }
}
