package com.csair.admin.core.dto;

import com.csair.admin.core.po.BaseQueryObject;
import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 14:04
 */
@Data
public class StorageOutQueryObject extends BaseQueryObject {
    private Long id;

    private Long orderId;

    private String beginDate;
    private String endDate;
    private String outNumber;
    private Long companyId;
    private String expressId;
    private String inputStorageNumber;
    private Integer assemblyType;
    /**
     * 名字
     */
    private String assemblyName;
    /**
     * 客户单号
     */
    private String customOrderId;
}
