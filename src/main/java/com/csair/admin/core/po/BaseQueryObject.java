package com.csair.admin.core.po;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:31
 */
public abstract class BaseQueryObject {
    private Integer currentPage =1;

    private Integer pageSize = 20;
    private Integer limit;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
