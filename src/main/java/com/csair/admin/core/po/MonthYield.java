package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class MonthYield implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 归属月份
     */
    private Date month;

    /**
     * 1:有效，0:无效
     */
    private Integer status;

    /**
     * 上月未出货数量
     */
    private String beforeMonthNotOutCount;

    /**
     * 本月未出货数量
     */
    private String currentMonthNotOutCount;

    /**
     * 归属仓库
     */
    private Integer storageId;

    /**
     * 成品id
     */
    private Long assemblyId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBeforeMonthNotOutCount() {
        return beforeMonthNotOutCount;
    }

    public void setBeforeMonthNotOutCount(String beforeMonthNotOutCount) {
        this.beforeMonthNotOutCount = beforeMonthNotOutCount == null ? null : beforeMonthNotOutCount.trim();
    }

    public String getCurrentMonthNotOutCount() {
        return currentMonthNotOutCount;
    }

    public void setCurrentMonthNotOutCount(String currentMonthNotOutCount) {
        this.currentMonthNotOutCount = currentMonthNotOutCount == null ? null : currentMonthNotOutCount.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", month=").append(month);
        sb.append(", status=").append(status);
        sb.append(", beforeMonthNotOutCount=").append(beforeMonthNotOutCount);
        sb.append(", currentMonthNotOutCount=").append(currentMonthNotOutCount);
        sb.append(", storageId=").append(storageId);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}