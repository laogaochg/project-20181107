package com.csair.admin.core.po;

import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/11 0:18
 */
@Data
public class OrderQueryObject extends BaseQueryObject{
    private Long companyId;
    private Long orderId;
    private Long assemblyId;
    private String beginDate;
    private String endDate;
    /**
     * 名字
     */
    private String assemblyName;
    /**
     * 客户单号
     */
    private String customOrderId;
    /**
     * 1:正常，2:外协
     */
    private String outType;
}
