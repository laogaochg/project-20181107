package com.csair.admin.core.po;

import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/12 21:09
 */
@Data
public class WarehouseQueryObject extends BaseQueryObject {
    private Long id;

    private Integer type = -1;

    private String name;
    /**
     * 是否根据名字分类，特别接口会用
     */
    private Boolean groupByName;
}
