package com.csair.admin.core.po.core;

import java.util.ArrayList;
import java.util.List;


//分页查询结果对象
public class PageResult<T> {
    private List<T> listData;// 当前页的结果集数据:查询
    private Integer totalCount;// 总数据条数:查询

    private Integer currentPage = 1;
    private Integer pageSize = 5;

    private Integer prevPage;// 上一页
    private Integer nextPage;// 下一页
    private Integer totalPage;// 总页数

    // 如果总数据条数为0,返回一个空集
    public static PageResult empty(Integer pageSize) {
        return new PageResult(new ArrayList<Object>(), 0, 1, pageSize);
    }

    public int getTotalPage() {
        return totalPage == 0 ? 1 : totalPage;
    }

    public PageResult() {
    }

    public PageResult(List<T> listData, Integer totalCount, Integer currentPage,
                      Integer pageSize) {
        if (pageSize == 0) pageSize = 9999;
        this.listData = listData;
        this.totalCount = totalCount;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        // ----------------------------------------
        this.totalPage = this.totalCount % this.pageSize == 0 ? this.totalCount
                / this.pageSize : this.totalCount / this.pageSize + 1;

        this.prevPage = this.currentPage - 1 >= 1 ? this.currentPage - 1 : 1;
        this.nextPage = this.currentPage + 1 <= this.totalPage ? this.currentPage + 1
                : this.totalPage;
    }

    public List<T> getListData() {
        return listData;
    }

    public void setListData(List<T> listData) {
        this.listData = listData;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(Integer prevPage) {
        this.prevPage = prevPage;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
