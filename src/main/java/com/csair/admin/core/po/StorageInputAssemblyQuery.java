package com.csair.admin.core.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StorageInputAssemblyQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public StorageInputAssemblyQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdIsNull() {
            addCriterion("storage_input_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdIsNotNull() {
            addCriterion("storage_input_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdEqualTo(Long value) {
            addCriterion("storage_input_id =", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdNotEqualTo(Long value) {
            addCriterion("storage_input_id <>", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdGreaterThan(Long value) {
            addCriterion("storage_input_id >", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdGreaterThanOrEqualTo(Long value) {
            addCriterion("storage_input_id >=", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdLessThan(Long value) {
            addCriterion("storage_input_id <", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdLessThanOrEqualTo(Long value) {
            addCriterion("storage_input_id <=", value, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdIn(List<Long> values) {
            addCriterion("storage_input_id in", values, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdNotIn(List<Long> values) {
            addCriterion("storage_input_id not in", values, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdBetween(Long value1, Long value2) {
            addCriterion("storage_input_id between", value1, value2, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andStorageInputIdNotBetween(Long value1, Long value2) {
            addCriterion("storage_input_id not between", value1, value2, "storageInputId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNull() {
            addCriterion("assembly_id is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNotNull() {
            addCriterion("assembly_id is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdEqualTo(Long value) {
            addCriterion("assembly_id =", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotEqualTo(Long value) {
            addCriterion("assembly_id <>", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThan(Long value) {
            addCriterion("assembly_id >", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("assembly_id >=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThan(Long value) {
            addCriterion("assembly_id <", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThanOrEqualTo(Long value) {
            addCriterion("assembly_id <=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIn(List<Long> values) {
            addCriterion("assembly_id in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotIn(List<Long> values) {
            addCriterion("assembly_id not in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdBetween(Long value1, Long value2) {
            addCriterion("assembly_id between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotBetween(Long value1, Long value2) {
            addCriterion("assembly_id not between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(String value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(String value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(String value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(String value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(String value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(String value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLike(String value) {
            addCriterion("unit like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotLike(String value) {
            addCriterion("unit not like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<String> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<String> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(String value1, String value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(String value1, String value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andCountIsNull() {
            addCriterion("count is null");
            return (Criteria) this;
        }

        public Criteria andCountIsNotNull() {
            addCriterion("count is not null");
            return (Criteria) this;
        }

        public Criteria andCountEqualTo(String value) {
            addCriterion("count =", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotEqualTo(String value) {
            addCriterion("count <>", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThan(String value) {
            addCriterion("count >", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThanOrEqualTo(String value) {
            addCriterion("count >=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThan(String value) {
            addCriterion("count <", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThanOrEqualTo(String value) {
            addCriterion("count <=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLike(String value) {
            addCriterion("count like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotLike(String value) {
            addCriterion("count not like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountIn(List<String> values) {
            addCriterion("count in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotIn(List<String> values) {
            addCriterion("count not in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountBetween(String value1, String value2) {
            addCriterion("count between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotBetween(String value1, String value2) {
            addCriterion("count not between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andExpressIdIsNull() {
            addCriterion("express_id is null");
            return (Criteria) this;
        }

        public Criteria andExpressIdIsNotNull() {
            addCriterion("express_id is not null");
            return (Criteria) this;
        }

        public Criteria andExpressIdEqualTo(String value) {
            addCriterion("express_id =", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdNotEqualTo(String value) {
            addCriterion("express_id <>", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdGreaterThan(String value) {
            addCriterion("express_id >", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdGreaterThanOrEqualTo(String value) {
            addCriterion("express_id >=", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdLessThan(String value) {
            addCriterion("express_id <", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdLessThanOrEqualTo(String value) {
            addCriterion("express_id <=", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdLike(String value) {
            addCriterion("express_id like", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdNotLike(String value) {
            addCriterion("express_id not like", value, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdIn(List<String> values) {
            addCriterion("express_id in", values, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdNotIn(List<String> values) {
            addCriterion("express_id not in", values, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdBetween(String value1, String value2) {
            addCriterion("express_id between", value1, value2, "expressId");
            return (Criteria) this;
        }

        public Criteria andExpressIdNotBetween(String value1, String value2) {
            addCriterion("express_id not between", value1, value2, "expressId");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberIsNull() {
            addCriterion("input_storage_number is null");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberIsNotNull() {
            addCriterion("input_storage_number is not null");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberEqualTo(String value) {
            addCriterion("input_storage_number =", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberNotEqualTo(String value) {
            addCriterion("input_storage_number <>", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberGreaterThan(String value) {
            addCriterion("input_storage_number >", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberGreaterThanOrEqualTo(String value) {
            addCriterion("input_storage_number >=", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberLessThan(String value) {
            addCriterion("input_storage_number <", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberLessThanOrEqualTo(String value) {
            addCriterion("input_storage_number <=", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberLike(String value) {
            addCriterion("input_storage_number like", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberNotLike(String value) {
            addCriterion("input_storage_number not like", value, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberIn(List<String> values) {
            addCriterion("input_storage_number in", values, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberNotIn(List<String> values) {
            addCriterion("input_storage_number not in", values, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberBetween(String value1, String value2) {
            addCriterion("input_storage_number between", value1, value2, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andInputStorageNumberNotBetween(String value1, String value2) {
            addCriterion("input_storage_number not between", value1, value2, "inputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberIsNull() {
            addCriterion("child_input_storage_number is null");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberIsNotNull() {
            addCriterion("child_input_storage_number is not null");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberEqualTo(String value) {
            addCriterion("child_input_storage_number =", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberNotEqualTo(String value) {
            addCriterion("child_input_storage_number <>", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberGreaterThan(String value) {
            addCriterion("child_input_storage_number >", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberGreaterThanOrEqualTo(String value) {
            addCriterion("child_input_storage_number >=", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberLessThan(String value) {
            addCriterion("child_input_storage_number <", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberLessThanOrEqualTo(String value) {
            addCriterion("child_input_storage_number <=", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberLike(String value) {
            addCriterion("child_input_storage_number like", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberNotLike(String value) {
            addCriterion("child_input_storage_number not like", value, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberIn(List<String> values) {
            addCriterion("child_input_storage_number in", values, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberNotIn(List<String> values) {
            addCriterion("child_input_storage_number not in", values, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberBetween(String value1, String value2) {
            addCriterion("child_input_storage_number between", value1, value2, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andChildInputStorageNumberNotBetween(String value1, String value2) {
            addCriterion("child_input_storage_number not between", value1, value2, "childInputStorageNumber");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNull() {
            addCriterion("storage_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNotNull() {
            addCriterion("storage_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageIdEqualTo(Integer value) {
            addCriterion("storage_id =", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotEqualTo(Integer value) {
            addCriterion("storage_id <>", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThan(Integer value) {
            addCriterion("storage_id >", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("storage_id >=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThan(Integer value) {
            addCriterion("storage_id <", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThanOrEqualTo(Integer value) {
            addCriterion("storage_id <=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIn(List<Integer> values) {
            addCriterion("storage_id in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotIn(List<Integer> values) {
            addCriterion("storage_id not in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdBetween(Integer value1, Integer value2) {
            addCriterion("storage_id between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("storage_id not between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNull() {
            addCriterion("company_id is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNotNull() {
            addCriterion("company_id is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdEqualTo(Long value) {
            addCriterion("company_id =", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotEqualTo(Long value) {
            addCriterion("company_id <>", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThan(Long value) {
            addCriterion("company_id >", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("company_id >=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThan(Long value) {
            addCriterion("company_id <", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThanOrEqualTo(Long value) {
            addCriterion("company_id <=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIn(List<Long> values) {
            addCriterion("company_id in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotIn(List<Long> values) {
            addCriterion("company_id not in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdBetween(Long value1, Long value2) {
            addCriterion("company_id between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotBetween(Long value1, Long value2) {
            addCriterion("company_id not between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andInputerIdIsNull() {
            addCriterion("inputer_id is null");
            return (Criteria) this;
        }

        public Criteria andInputerIdIsNotNull() {
            addCriterion("inputer_id is not null");
            return (Criteria) this;
        }

        public Criteria andInputerIdEqualTo(Long value) {
            addCriterion("inputer_id =", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotEqualTo(Long value) {
            addCriterion("inputer_id <>", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdGreaterThan(Long value) {
            addCriterion("inputer_id >", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdGreaterThanOrEqualTo(Long value) {
            addCriterion("inputer_id >=", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdLessThan(Long value) {
            addCriterion("inputer_id <", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdLessThanOrEqualTo(Long value) {
            addCriterion("inputer_id <=", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdIn(List<Long> values) {
            addCriterion("inputer_id in", values, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotIn(List<Long> values) {
            addCriterion("inputer_id not in", values, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdBetween(Long value1, Long value2) {
            addCriterion("inputer_id between", value1, value2, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotBetween(Long value1, Long value2) {
            addCriterion("inputer_id not between", value1, value2, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeIsNull() {
            addCriterion("input_storage_time is null");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeIsNotNull() {
            addCriterion("input_storage_time is not null");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeEqualTo(Date value) {
            addCriterionForJDBCDate("input_storage_time =", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("input_storage_time <>", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("input_storage_time >", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("input_storage_time >=", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeLessThan(Date value) {
            addCriterionForJDBCDate("input_storage_time <", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("input_storage_time <=", value, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeIn(List<Date> values) {
            addCriterionForJDBCDate("input_storage_time in", values, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("input_storage_time not in", values, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("input_storage_time between", value1, value2, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andInputStorageTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("input_storage_time not between", value1, value2, "inputStorageTime");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIsNull() {
            addCriterion("custom_order_id is null");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIsNotNull() {
            addCriterion("custom_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdEqualTo(String value) {
            addCriterion("custom_order_id =", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotEqualTo(String value) {
            addCriterion("custom_order_id <>", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdGreaterThan(String value) {
            addCriterion("custom_order_id >", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("custom_order_id >=", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLessThan(String value) {
            addCriterion("custom_order_id <", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLessThanOrEqualTo(String value) {
            addCriterion("custom_order_id <=", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLike(String value) {
            addCriterion("custom_order_id like", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotLike(String value) {
            addCriterion("custom_order_id not like", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIn(List<String> values) {
            addCriterion("custom_order_id in", values, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotIn(List<String> values) {
            addCriterion("custom_order_id not in", values, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdBetween(String value1, String value2) {
            addCriterion("custom_order_id between", value1, value2, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotBetween(String value1, String value2) {
            addCriterion("custom_order_id not between", value1, value2, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andPointIsNull() {
            addCriterion("point is null");
            return (Criteria) this;
        }

        public Criteria andPointIsNotNull() {
            addCriterion("point is not null");
            return (Criteria) this;
        }

        public Criteria andPointEqualTo(String value) {
            addCriterion("point =", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotEqualTo(String value) {
            addCriterion("point <>", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThan(String value) {
            addCriterion("point >", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThanOrEqualTo(String value) {
            addCriterion("point >=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThan(String value) {
            addCriterion("point <", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThanOrEqualTo(String value) {
            addCriterion("point <=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLike(String value) {
            addCriterion("point like", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotLike(String value) {
            addCriterion("point not like", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointIn(List<String> values) {
            addCriterion("point in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotIn(List<String> values) {
            addCriterion("point not in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointBetween(String value1, String value2) {
            addCriterion("point between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotBetween(String value1, String value2) {
            addCriterion("point not between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andCustomIsNull() {
            addCriterion("custom is null");
            return (Criteria) this;
        }

        public Criteria andCustomIsNotNull() {
            addCriterion("custom is not null");
            return (Criteria) this;
        }

        public Criteria andCustomEqualTo(String value) {
            addCriterion("custom =", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotEqualTo(String value) {
            addCriterion("custom <>", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomGreaterThan(String value) {
            addCriterion("custom >", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomGreaterThanOrEqualTo(String value) {
            addCriterion("custom >=", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLessThan(String value) {
            addCriterion("custom <", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLessThanOrEqualTo(String value) {
            addCriterion("custom <=", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLike(String value) {
            addCriterion("custom like", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotLike(String value) {
            addCriterion("custom not like", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomIn(List<String> values) {
            addCriterion("custom in", values, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotIn(List<String> values) {
            addCriterion("custom not in", values, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomBetween(String value1, String value2) {
            addCriterion("custom between", value1, value2, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotBetween(String value1, String value2) {
            addCriterion("custom not between", value1, value2, "custom");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIsNull() {
            addCriterion("assembly_type is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIsNotNull() {
            addCriterion("assembly_type is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeEqualTo(Integer value) {
            addCriterion("assembly_type =", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotEqualTo(Integer value) {
            addCriterion("assembly_type <>", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeGreaterThan(Integer value) {
            addCriterion("assembly_type >", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("assembly_type >=", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeLessThan(Integer value) {
            addCriterion("assembly_type <", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeLessThanOrEqualTo(Integer value) {
            addCriterion("assembly_type <=", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIn(List<Integer> values) {
            addCriterion("assembly_type in", values, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotIn(List<Integer> values) {
            addCriterion("assembly_type not in", values, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeBetween(Integer value1, Integer value2) {
            addCriterion("assembly_type between", value1, value2, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("assembly_type not between", value1, value2, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(String value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(String value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(String value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(String value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(String value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(String value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLike(String value) {
            addCriterion("price like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotLike(String value) {
            addCriterion("price not like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<String> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<String> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(String value1, String value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(String value1, String value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}