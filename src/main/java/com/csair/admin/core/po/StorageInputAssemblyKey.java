package com.csair.admin.core.po;

import java.io.Serializable;

public class StorageInputAssemblyKey implements Serializable {
    /**
     * 入库记录id
     */
    private Long storageInputId;

    /**
     * 组件id
     */
    private Long assemblyId;

    private static final long serialVersionUID = 1L;

    public Long getStorageInputId() {
        return storageInputId;
    }

    public void setStorageInputId(Long storageInputId) {
        this.storageInputId = storageInputId;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", storageInputId=").append(storageInputId);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}