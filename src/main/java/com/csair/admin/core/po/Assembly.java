package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class Assembly implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 物料点
     */
    private String outPoint;

    /**
     * 出货单价(点乘这个得一个组件出库件单价)
     */
    private String outPointPrice;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 1:有效，0:无效
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0:组件,1:SMT,3:成品
     */
    private Integer type;

    /**
     * 加工单价
     */
    private String processingUnitPrice;

    private String name;

    /**
     * 型号
     */
    private String modelName;

    /**
     * 编码
     */
    private String code;

    /**
     * 单位
     */
    private String unit;

    /**
     * 归属仓库
     */
    private Integer storageId;

    /**
     * 公司id
     */
    private Long companyId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOutPoint() {
        return outPoint;
    }

    public void setOutPoint(String outPoint) {
        this.outPoint = outPoint == null ? null : outPoint.trim();
    }

    public String getOutPointPrice() {
        return outPointPrice;
    }

    public void setOutPointPrice(String outPointPrice) {
        this.outPointPrice = outPointPrice == null ? null : outPointPrice.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getProcessingUnitPrice() {
        return processingUnitPrice;
    }

    public void setProcessingUnitPrice(String processingUnitPrice) {
        this.processingUnitPrice = processingUnitPrice == null ? null : processingUnitPrice.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName == null ? null : modelName.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", outPoint=").append(outPoint);
        sb.append(", outPointPrice=").append(outPointPrice);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", remark=").append(remark);
        sb.append(", type=").append(type);
        sb.append(", processingUnitPrice=").append(processingUnitPrice);
        sb.append(", name=").append(name);
        sb.append(", modelName=").append(modelName);
        sb.append(", code=").append(code);
        sb.append(", unit=").append(unit);
        sb.append(", storageId=").append(storageId);
        sb.append(", companyId=").append(companyId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}