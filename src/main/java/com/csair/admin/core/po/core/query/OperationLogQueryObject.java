package com.csair.admin.core.po.core.query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.csair.admin.core.po.BaseQueryObject;
import com.csair.admin.util.DateUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * laogaochg
 * 2017/7/11.
 */
@Data
public class OperationLogQueryObject extends BaseQueryObject {
    public static final String baseSelect = "select l.id as id , l.author as author ," +//
            " l.action as action , l.content as content , l.op_time as opTime , " +//
            "l.op_ip as opIp , u.email as authorName from csair_log_operation l join e_admin_user u on l.author = u.id ";

    private String keyword;
    private Date beginTime;
    private Date endTime;
    private String authorId;
    private Long id;

    /**
     * 操作人员id
     */
    private String author;

    private String account;

    /**
     * 动作
     */
    private String action;

    /**
     * 操作时间
     */
    private Date opTime;

    /**
     * 操作的ip地址
     */
    private String opIp;

    /**
     * 内容
     */
    private String content;

    public void setBeginTime(String beginTime) {
        try {
            if (StringUtils.isNotBlank(beginTime)) {
                this.beginTime = new SimpleDateFormat("yyyy-MM-dd").parse(beginTime);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setEndTime(String endTime) {
        try {
            if (StringUtils.isNotBlank(endTime)) {
                this.endTime = DateUtil.getTodayEndTime(new SimpleDateFormat("yyyy-MM-dd").parse(endTime));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
