package com.csair.admin.core.po.core;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2017/10/17 10:58
 * 类里面对应的url，对应的菜单，对应的controller方法
 */
public class MenuPermissionUrl {
    List<String> requestUrl = new ArrayList<>();
    List<String> controllerUrl = new ArrayList<>();
    List<String> permissionNames = new ArrayList<>();
}
