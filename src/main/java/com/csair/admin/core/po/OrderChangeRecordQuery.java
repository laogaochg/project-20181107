package com.csair.admin.core.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderChangeRecordQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public OrderChangeRecordQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNull() {
            addCriterion("storage_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNotNull() {
            addCriterion("storage_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageIdEqualTo(Integer value) {
            addCriterion("storage_id =", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotEqualTo(Integer value) {
            addCriterion("storage_id <>", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThan(Integer value) {
            addCriterion("storage_id >", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("storage_id >=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThan(Integer value) {
            addCriterion("storage_id <", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThanOrEqualTo(Integer value) {
            addCriterion("storage_id <=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIn(List<Integer> values) {
            addCriterion("storage_id in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotIn(List<Integer> values) {
            addCriterion("storage_id not in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdBetween(Integer value1, Integer value2) {
            addCriterion("storage_id between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("storage_id not between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeIsNull() {
            addCriterion("input_count_before is null");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeIsNotNull() {
            addCriterion("input_count_before is not null");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeEqualTo(String value) {
            addCriterion("input_count_before =", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeNotEqualTo(String value) {
            addCriterion("input_count_before <>", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeGreaterThan(String value) {
            addCriterion("input_count_before >", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeGreaterThanOrEqualTo(String value) {
            addCriterion("input_count_before >=", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeLessThan(String value) {
            addCriterion("input_count_before <", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeLessThanOrEqualTo(String value) {
            addCriterion("input_count_before <=", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeLike(String value) {
            addCriterion("input_count_before like", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeNotLike(String value) {
            addCriterion("input_count_before not like", value, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeIn(List<String> values) {
            addCriterion("input_count_before in", values, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeNotIn(List<String> values) {
            addCriterion("input_count_before not in", values, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeBetween(String value1, String value2) {
            addCriterion("input_count_before between", value1, value2, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountBeforeNotBetween(String value1, String value2) {
            addCriterion("input_count_before not between", value1, value2, "inputCountBefore");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterIsNull() {
            addCriterion("input_count_after is null");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterIsNotNull() {
            addCriterion("input_count_after is not null");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterEqualTo(String value) {
            addCriterion("input_count_after =", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterNotEqualTo(String value) {
            addCriterion("input_count_after <>", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterGreaterThan(String value) {
            addCriterion("input_count_after >", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterGreaterThanOrEqualTo(String value) {
            addCriterion("input_count_after >=", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterLessThan(String value) {
            addCriterion("input_count_after <", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterLessThanOrEqualTo(String value) {
            addCriterion("input_count_after <=", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterLike(String value) {
            addCriterion("input_count_after like", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterNotLike(String value) {
            addCriterion("input_count_after not like", value, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterIn(List<String> values) {
            addCriterion("input_count_after in", values, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterNotIn(List<String> values) {
            addCriterion("input_count_after not in", values, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterBetween(String value1, String value2) {
            addCriterion("input_count_after between", value1, value2, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andInputCountAfterNotBetween(String value1, String value2) {
            addCriterion("input_count_after not between", value1, value2, "inputCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeIsNull() {
            addCriterion("out_count_before is null");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeIsNotNull() {
            addCriterion("out_count_before is not null");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeEqualTo(String value) {
            addCriterion("out_count_before =", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeNotEqualTo(String value) {
            addCriterion("out_count_before <>", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeGreaterThan(String value) {
            addCriterion("out_count_before >", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeGreaterThanOrEqualTo(String value) {
            addCriterion("out_count_before >=", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeLessThan(String value) {
            addCriterion("out_count_before <", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeLessThanOrEqualTo(String value) {
            addCriterion("out_count_before <=", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeLike(String value) {
            addCriterion("out_count_before like", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeNotLike(String value) {
            addCriterion("out_count_before not like", value, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeIn(List<String> values) {
            addCriterion("out_count_before in", values, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeNotIn(List<String> values) {
            addCriterion("out_count_before not in", values, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeBetween(String value1, String value2) {
            addCriterion("out_count_before between", value1, value2, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountBeforeNotBetween(String value1, String value2) {
            addCriterion("out_count_before not between", value1, value2, "outCountBefore");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterIsNull() {
            addCriterion("out_count_after is null");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterIsNotNull() {
            addCriterion("out_count_after is not null");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterEqualTo(String value) {
            addCriterion("out_count_after =", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterNotEqualTo(String value) {
            addCriterion("out_count_after <>", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterGreaterThan(String value) {
            addCriterion("out_count_after >", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterGreaterThanOrEqualTo(String value) {
            addCriterion("out_count_after >=", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterLessThan(String value) {
            addCriterion("out_count_after <", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterLessThanOrEqualTo(String value) {
            addCriterion("out_count_after <=", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterLike(String value) {
            addCriterion("out_count_after like", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterNotLike(String value) {
            addCriterion("out_count_after not like", value, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterIn(List<String> values) {
            addCriterion("out_count_after in", values, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterNotIn(List<String> values) {
            addCriterion("out_count_after not in", values, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterBetween(String value1, String value2) {
            addCriterion("out_count_after between", value1, value2, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andOutCountAfterNotBetween(String value1, String value2) {
            addCriterion("out_count_after not between", value1, value2, "outCountAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeIsNull() {
            addCriterion("total_money_before is null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeIsNotNull() {
            addCriterion("total_money_before is not null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeEqualTo(String value) {
            addCriterion("total_money_before =", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeNotEqualTo(String value) {
            addCriterion("total_money_before <>", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeGreaterThan(String value) {
            addCriterion("total_money_before >", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeGreaterThanOrEqualTo(String value) {
            addCriterion("total_money_before >=", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeLessThan(String value) {
            addCriterion("total_money_before <", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeLessThanOrEqualTo(String value) {
            addCriterion("total_money_before <=", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeLike(String value) {
            addCriterion("total_money_before like", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeNotLike(String value) {
            addCriterion("total_money_before not like", value, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeIn(List<String> values) {
            addCriterion("total_money_before in", values, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeNotIn(List<String> values) {
            addCriterion("total_money_before not in", values, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeBetween(String value1, String value2) {
            addCriterion("total_money_before between", value1, value2, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBeforeNotBetween(String value1, String value2) {
            addCriterion("total_money_before not between", value1, value2, "totalMoneyBefore");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterIsNull() {
            addCriterion("total_money_after is null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterIsNotNull() {
            addCriterion("total_money_after is not null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterEqualTo(String value) {
            addCriterion("total_money_after =", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterNotEqualTo(String value) {
            addCriterion("total_money_after <>", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterGreaterThan(String value) {
            addCriterion("total_money_after >", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterGreaterThanOrEqualTo(String value) {
            addCriterion("total_money_after >=", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterLessThan(String value) {
            addCriterion("total_money_after <", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterLessThanOrEqualTo(String value) {
            addCriterion("total_money_after <=", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterLike(String value) {
            addCriterion("total_money_after like", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterNotLike(String value) {
            addCriterion("total_money_after not like", value, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterIn(List<String> values) {
            addCriterion("total_money_after in", values, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterNotIn(List<String> values) {
            addCriterion("total_money_after not in", values, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterBetween(String value1, String value2) {
            addCriterion("total_money_after between", value1, value2, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyAfterNotBetween(String value1, String value2) {
            addCriterion("total_money_after not between", value1, value2, "totalMoneyAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeIsNull() {
            addCriterion("paid_count_before is null");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeIsNotNull() {
            addCriterion("paid_count_before is not null");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeEqualTo(String value) {
            addCriterion("paid_count_before =", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeNotEqualTo(String value) {
            addCriterion("paid_count_before <>", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeGreaterThan(String value) {
            addCriterion("paid_count_before >", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeGreaterThanOrEqualTo(String value) {
            addCriterion("paid_count_before >=", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeLessThan(String value) {
            addCriterion("paid_count_before <", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeLessThanOrEqualTo(String value) {
            addCriterion("paid_count_before <=", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeLike(String value) {
            addCriterion("paid_count_before like", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeNotLike(String value) {
            addCriterion("paid_count_before not like", value, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeIn(List<String> values) {
            addCriterion("paid_count_before in", values, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeNotIn(List<String> values) {
            addCriterion("paid_count_before not in", values, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeBetween(String value1, String value2) {
            addCriterion("paid_count_before between", value1, value2, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountBeforeNotBetween(String value1, String value2) {
            addCriterion("paid_count_before not between", value1, value2, "paidCountBefore");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterIsNull() {
            addCriterion("paid_count_after is null");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterIsNotNull() {
            addCriterion("paid_count_after is not null");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterEqualTo(String value) {
            addCriterion("paid_count_after =", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterNotEqualTo(String value) {
            addCriterion("paid_count_after <>", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterGreaterThan(String value) {
            addCriterion("paid_count_after >", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterGreaterThanOrEqualTo(String value) {
            addCriterion("paid_count_after >=", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterLessThan(String value) {
            addCriterion("paid_count_after <", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterLessThanOrEqualTo(String value) {
            addCriterion("paid_count_after <=", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterLike(String value) {
            addCriterion("paid_count_after like", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterNotLike(String value) {
            addCriterion("paid_count_after not like", value, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterIn(List<String> values) {
            addCriterion("paid_count_after in", values, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterNotIn(List<String> values) {
            addCriterion("paid_count_after not in", values, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterBetween(String value1, String value2) {
            addCriterion("paid_count_after between", value1, value2, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andPaidCountAfterNotBetween(String value1, String value2) {
            addCriterion("paid_count_after not between", value1, value2, "paidCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeIsNull() {
            addCriterion("resert_count_before is null");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeIsNotNull() {
            addCriterion("resert_count_before is not null");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeEqualTo(String value) {
            addCriterion("resert_count_before =", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeNotEqualTo(String value) {
            addCriterion("resert_count_before <>", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeGreaterThan(String value) {
            addCriterion("resert_count_before >", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeGreaterThanOrEqualTo(String value) {
            addCriterion("resert_count_before >=", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeLessThan(String value) {
            addCriterion("resert_count_before <", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeLessThanOrEqualTo(String value) {
            addCriterion("resert_count_before <=", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeLike(String value) {
            addCriterion("resert_count_before like", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeNotLike(String value) {
            addCriterion("resert_count_before not like", value, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeIn(List<String> values) {
            addCriterion("resert_count_before in", values, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeNotIn(List<String> values) {
            addCriterion("resert_count_before not in", values, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeBetween(String value1, String value2) {
            addCriterion("resert_count_before between", value1, value2, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountBeforeNotBetween(String value1, String value2) {
            addCriterion("resert_count_before not between", value1, value2, "resertCountBefore");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterIsNull() {
            addCriterion("resert_count_after is null");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterIsNotNull() {
            addCriterion("resert_count_after is not null");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterEqualTo(String value) {
            addCriterion("resert_count_after =", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterNotEqualTo(String value) {
            addCriterion("resert_count_after <>", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterGreaterThan(String value) {
            addCriterion("resert_count_after >", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterGreaterThanOrEqualTo(String value) {
            addCriterion("resert_count_after >=", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterLessThan(String value) {
            addCriterion("resert_count_after <", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterLessThanOrEqualTo(String value) {
            addCriterion("resert_count_after <=", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterLike(String value) {
            addCriterion("resert_count_after like", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterNotLike(String value) {
            addCriterion("resert_count_after not like", value, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterIn(List<String> values) {
            addCriterion("resert_count_after in", values, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterNotIn(List<String> values) {
            addCriterion("resert_count_after not in", values, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterBetween(String value1, String value2) {
            addCriterion("resert_count_after between", value1, value2, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andResertCountAfterNotBetween(String value1, String value2) {
            addCriterion("resert_count_after not between", value1, value2, "resertCountAfter");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}