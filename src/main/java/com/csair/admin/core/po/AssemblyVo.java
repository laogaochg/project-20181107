package com.csair.admin.core.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class AssemblyVo implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 组件单价(出货运算单价)
     */
    private String outPrice;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 1:有效，0:无效
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0:组件,1:SMT
     */
    private Integer type;

    private String name;
    /**
     * 部件信息
     */
    private List<AssemblyVo> properties;

    private String propertyIds;
    private String counts;
    private String modelName;
    private String code;
    /**
     * 出货点
     */
    private String outPoint;

    /**
     * 出货单价(点乘这个得一个组件出库件单价)
     */
    private String outPointPrice;
    /**
     * 修改的动作类型，1:修改价格，0:编辑
     */
    private Integer updateAction;

    private String unit;
    private Long companyId;

    public AssemblyVo() {
    }

    public AssemblyVo(Assembly a) {
        this.id = a.getId();
        this.createTime = a.getCreateTime();
        this.status = a.getStatus();
        this.remark = a.getRemark();
        this.type = a.getType();
        this.name = a.getName();
        this.code = a.getCode();
        this.modelName = a.getModelName();
        this.outPoint = a.getOutPoint();
        this.outPointPrice = a.getOutPointPrice();
        this.companyId = a.getCompanyId();
        this.setUnit(a.getUnit());
    }

    public Assembly getAssembly() {
        Assembly result = new Assembly();
        result.setId(this.getId());
        result.setCreateTime(this.getCreateTime());
        result.setStatus(this.getStatus());
        result.setRemark(this.getRemark());
        result.setType(this.getType());
        result.setName(this.getName());
        result.setCode(this.getCode());
        result.setModelName(this.getModelName());
        result.setOutPoint(this.getOutPoint());
        result.setOutPointPrice(this.getOutPointPrice());
        result.setUnit(this.getUnit());
        result.setCompanyId(this.getCompanyId());
        return result;
    }

    public String getPropertyIds() {
        return propertyIds;
    }

    public String getDesc() {
        if (properties == null) return "";
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        for (AssemblyVo p : properties) {
            sb.append(p.getName() + " " + p.counts + " 个");
            if (i++ != (properties.size() - 1)) {
                sb.append("，");
            }
        }
        return sb.toString();
    }

}