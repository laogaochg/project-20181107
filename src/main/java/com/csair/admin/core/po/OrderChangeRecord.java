package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class OrderChangeRecord implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 操作人ID
     */
    private Long userId;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 入库数量变化前
     */
    private String inputCountBefore;

    /**
     * 入库数量变化后
     */
    private String inputCountAfter;

    /**
     * 已出库数量变化前
     */
    private String outCountBefore;

    /**
     * 已出库数量变化后
     */
    private String outCountAfter;

    /**
     * 总金额变化前
     */
    private String totalMoneyBefore;

    /**
     * 总金额变化后
     */
    private String totalMoneyAfter;

    /**
     * 已结金额变化前
     */
    private String paidCountBefore;

    /**
     * 已结金额变化后
     */
    private String paidCountAfter;

    /**
     * 剩余数量变化前
     */
    private String resertCountBefore;

    /**
     * 剩余数量变化后
     */
    private String resertCountAfter;

    /**
     * 状态
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public String getInputCountBefore() {
        return inputCountBefore;
    }

    public void setInputCountBefore(String inputCountBefore) {
        this.inputCountBefore = inputCountBefore == null ? null : inputCountBefore.trim();
    }

    public String getInputCountAfter() {
        return inputCountAfter;
    }

    public void setInputCountAfter(String inputCountAfter) {
        this.inputCountAfter = inputCountAfter == null ? null : inputCountAfter.trim();
    }

    public String getOutCountBefore() {
        return outCountBefore;
    }

    public void setOutCountBefore(String outCountBefore) {
        this.outCountBefore = outCountBefore == null ? null : outCountBefore.trim();
    }

    public String getOutCountAfter() {
        return outCountAfter;
    }

    public void setOutCountAfter(String outCountAfter) {
        this.outCountAfter = outCountAfter == null ? null : outCountAfter.trim();
    }

    public String getTotalMoneyBefore() {
        return totalMoneyBefore;
    }

    public void setTotalMoneyBefore(String totalMoneyBefore) {
        this.totalMoneyBefore = totalMoneyBefore == null ? null : totalMoneyBefore.trim();
    }

    public String getTotalMoneyAfter() {
        return totalMoneyAfter;
    }

    public void setTotalMoneyAfter(String totalMoneyAfter) {
        this.totalMoneyAfter = totalMoneyAfter == null ? null : totalMoneyAfter.trim();
    }

    public String getPaidCountBefore() {
        return paidCountBefore;
    }

    public void setPaidCountBefore(String paidCountBefore) {
        this.paidCountBefore = paidCountBefore == null ? null : paidCountBefore.trim();
    }

    public String getPaidCountAfter() {
        return paidCountAfter;
    }

    public void setPaidCountAfter(String paidCountAfter) {
        this.paidCountAfter = paidCountAfter == null ? null : paidCountAfter.trim();
    }

    public String getResertCountBefore() {
        return resertCountBefore;
    }

    public void setResertCountBefore(String resertCountBefore) {
        this.resertCountBefore = resertCountBefore == null ? null : resertCountBefore.trim();
    }

    public String getResertCountAfter() {
        return resertCountAfter;
    }

    public void setResertCountAfter(String resertCountAfter) {
        this.resertCountAfter = resertCountAfter == null ? null : resertCountAfter.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", userId=").append(userId);
        sb.append(", storageId=").append(storageId);
        sb.append(", inputCountBefore=").append(inputCountBefore);
        sb.append(", inputCountAfter=").append(inputCountAfter);
        sb.append(", outCountBefore=").append(outCountBefore);
        sb.append(", outCountAfter=").append(outCountAfter);
        sb.append(", totalMoneyBefore=").append(totalMoneyBefore);
        sb.append(", totalMoneyAfter=").append(totalMoneyAfter);
        sb.append(", paidCountBefore=").append(paidCountBefore);
        sb.append(", paidCountAfter=").append(paidCountAfter);
        sb.append(", resertCountBefore=").append(resertCountBefore);
        sb.append(", resertCountAfter=").append(resertCountAfter);
        sb.append(", type=").append(type);
        sb.append(", remark=").append(remark);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}