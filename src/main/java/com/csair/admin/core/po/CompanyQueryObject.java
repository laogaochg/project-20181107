package com.csair.admin.core.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class CompanyQueryObject extends BaseQueryObject implements Serializable {
    private Long id;

    private String name;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 公司电话
     */
    private String companyPhone;

    /**
     * 公司邮箱
     */
    private String companyEmail;

    /**
     * 公司传真
     */
    private String fax;

    private String address;

    private Date createDate;

    /**
     * 1:有效,2无效
     */
    private Integer state;
    private String contacts;

    private static final long serialVersionUID = 1L;

}