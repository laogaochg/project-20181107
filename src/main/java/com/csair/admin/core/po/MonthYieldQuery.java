package com.csair.admin.core.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonthYieldQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public MonthYieldQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andMonthIsNull() {
            addCriterion("month is null");
            return (Criteria) this;
        }

        public Criteria andMonthIsNotNull() {
            addCriterion("month is not null");
            return (Criteria) this;
        }

        public Criteria andMonthEqualTo(Date value) {
            addCriterion("month =", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotEqualTo(Date value) {
            addCriterion("month <>", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthGreaterThan(Date value) {
            addCriterion("month >", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthGreaterThanOrEqualTo(Date value) {
            addCriterion("month >=", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthLessThan(Date value) {
            addCriterion("month <", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthLessThanOrEqualTo(Date value) {
            addCriterion("month <=", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthIn(List<Date> values) {
            addCriterion("month in", values, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotIn(List<Date> values) {
            addCriterion("month not in", values, "month");
            return (Criteria) this;
        }

        public Criteria andMonthBetween(Date value1, Date value2) {
            addCriterion("month between", value1, value2, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotBetween(Date value1, Date value2) {
            addCriterion("month not between", value1, value2, "month");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountIsNull() {
            addCriterion("before_month_not_out_count is null");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountIsNotNull() {
            addCriterion("before_month_not_out_count is not null");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountEqualTo(String value) {
            addCriterion("before_month_not_out_count =", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountNotEqualTo(String value) {
            addCriterion("before_month_not_out_count <>", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountGreaterThan(String value) {
            addCriterion("before_month_not_out_count >", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountGreaterThanOrEqualTo(String value) {
            addCriterion("before_month_not_out_count >=", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountLessThan(String value) {
            addCriterion("before_month_not_out_count <", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountLessThanOrEqualTo(String value) {
            addCriterion("before_month_not_out_count <=", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountLike(String value) {
            addCriterion("before_month_not_out_count like", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountNotLike(String value) {
            addCriterion("before_month_not_out_count not like", value, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountIn(List<String> values) {
            addCriterion("before_month_not_out_count in", values, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountNotIn(List<String> values) {
            addCriterion("before_month_not_out_count not in", values, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountBetween(String value1, String value2) {
            addCriterion("before_month_not_out_count between", value1, value2, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andBeforeMonthNotOutCountNotBetween(String value1, String value2) {
            addCriterion("before_month_not_out_count not between", value1, value2, "beforeMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountIsNull() {
            addCriterion("current_month_not_out_count is null");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountIsNotNull() {
            addCriterion("current_month_not_out_count is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountEqualTo(String value) {
            addCriterion("current_month_not_out_count =", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountNotEqualTo(String value) {
            addCriterion("current_month_not_out_count <>", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountGreaterThan(String value) {
            addCriterion("current_month_not_out_count >", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountGreaterThanOrEqualTo(String value) {
            addCriterion("current_month_not_out_count >=", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountLessThan(String value) {
            addCriterion("current_month_not_out_count <", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountLessThanOrEqualTo(String value) {
            addCriterion("current_month_not_out_count <=", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountLike(String value) {
            addCriterion("current_month_not_out_count like", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountNotLike(String value) {
            addCriterion("current_month_not_out_count not like", value, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountIn(List<String> values) {
            addCriterion("current_month_not_out_count in", values, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountNotIn(List<String> values) {
            addCriterion("current_month_not_out_count not in", values, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountBetween(String value1, String value2) {
            addCriterion("current_month_not_out_count between", value1, value2, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andCurrentMonthNotOutCountNotBetween(String value1, String value2) {
            addCriterion("current_month_not_out_count not between", value1, value2, "currentMonthNotOutCount");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNull() {
            addCriterion("storage_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNotNull() {
            addCriterion("storage_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageIdEqualTo(Integer value) {
            addCriterion("storage_id =", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotEqualTo(Integer value) {
            addCriterion("storage_id <>", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThan(Integer value) {
            addCriterion("storage_id >", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("storage_id >=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThan(Integer value) {
            addCriterion("storage_id <", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThanOrEqualTo(Integer value) {
            addCriterion("storage_id <=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIn(List<Integer> values) {
            addCriterion("storage_id in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotIn(List<Integer> values) {
            addCriterion("storage_id not in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdBetween(Integer value1, Integer value2) {
            addCriterion("storage_id between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("storage_id not between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNull() {
            addCriterion("assembly_id is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNotNull() {
            addCriterion("assembly_id is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdEqualTo(Long value) {
            addCriterion("assembly_id =", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotEqualTo(Long value) {
            addCriterion("assembly_id <>", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThan(Long value) {
            addCriterion("assembly_id >", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("assembly_id >=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThan(Long value) {
            addCriterion("assembly_id <", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThanOrEqualTo(Long value) {
            addCriterion("assembly_id <=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIn(List<Long> values) {
            addCriterion("assembly_id in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotIn(List<Long> values) {
            addCriterion("assembly_id not in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdBetween(Long value1, Long value2) {
            addCriterion("assembly_id between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotBetween(Long value1, Long value2) {
            addCriterion("assembly_id not between", value1, value2, "assemblyId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}