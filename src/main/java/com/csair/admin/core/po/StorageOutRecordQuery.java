package com.csair.admin.core.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StorageOutRecordQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public StorageOutRecordQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNull() {
            addCriterion("assembly_id is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNotNull() {
            addCriterion("assembly_id is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdEqualTo(Long value) {
            addCriterion("assembly_id =", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotEqualTo(Long value) {
            addCriterion("assembly_id <>", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThan(Long value) {
            addCriterion("assembly_id >", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("assembly_id >=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThan(Long value) {
            addCriterion("assembly_id <", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThanOrEqualTo(Long value) {
            addCriterion("assembly_id <=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIn(List<Long> values) {
            addCriterion("assembly_id in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotIn(List<Long> values) {
            addCriterion("assembly_id not in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdBetween(Long value1, Long value2) {
            addCriterion("assembly_id between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotBetween(Long value1, Long value2) {
            addCriterion("assembly_id not between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andCountIsNull() {
            addCriterion("count is null");
            return (Criteria) this;
        }

        public Criteria andCountIsNotNull() {
            addCriterion("count is not null");
            return (Criteria) this;
        }

        public Criteria andCountEqualTo(String value) {
            addCriterion("count =", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotEqualTo(String value) {
            addCriterion("count <>", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThan(String value) {
            addCriterion("count >", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThanOrEqualTo(String value) {
            addCriterion("count >=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThan(String value) {
            addCriterion("count <", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThanOrEqualTo(String value) {
            addCriterion("count <=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLike(String value) {
            addCriterion("count like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotLike(String value) {
            addCriterion("count not like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountIn(List<String> values) {
            addCriterion("count in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotIn(List<String> values) {
            addCriterion("count not in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountBetween(String value1, String value2) {
            addCriterion("count between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotBetween(String value1, String value2) {
            addCriterion("count not between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andOutNumberIsNull() {
            addCriterion("out_number is null");
            return (Criteria) this;
        }

        public Criteria andOutNumberIsNotNull() {
            addCriterion("out_number is not null");
            return (Criteria) this;
        }

        public Criteria andOutNumberEqualTo(String value) {
            addCriterion("out_number =", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberNotEqualTo(String value) {
            addCriterion("out_number <>", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberGreaterThan(String value) {
            addCriterion("out_number >", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberGreaterThanOrEqualTo(String value) {
            addCriterion("out_number >=", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberLessThan(String value) {
            addCriterion("out_number <", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberLessThanOrEqualTo(String value) {
            addCriterion("out_number <=", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberLike(String value) {
            addCriterion("out_number like", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberNotLike(String value) {
            addCriterion("out_number not like", value, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberIn(List<String> values) {
            addCriterion("out_number in", values, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberNotIn(List<String> values) {
            addCriterion("out_number not in", values, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberBetween(String value1, String value2) {
            addCriterion("out_number between", value1, value2, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutNumberNotBetween(String value1, String value2) {
            addCriterion("out_number not between", value1, value2, "outNumber");
            return (Criteria) this;
        }

        public Criteria andOutPointIsNull() {
            addCriterion("out_point is null");
            return (Criteria) this;
        }

        public Criteria andOutPointIsNotNull() {
            addCriterion("out_point is not null");
            return (Criteria) this;
        }

        public Criteria andOutPointEqualTo(String value) {
            addCriterion("out_point =", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointNotEqualTo(String value) {
            addCriterion("out_point <>", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointGreaterThan(String value) {
            addCriterion("out_point >", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointGreaterThanOrEqualTo(String value) {
            addCriterion("out_point >=", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointLessThan(String value) {
            addCriterion("out_point <", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointLessThanOrEqualTo(String value) {
            addCriterion("out_point <=", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointLike(String value) {
            addCriterion("out_point like", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointNotLike(String value) {
            addCriterion("out_point not like", value, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointIn(List<String> values) {
            addCriterion("out_point in", values, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointNotIn(List<String> values) {
            addCriterion("out_point not in", values, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointBetween(String value1, String value2) {
            addCriterion("out_point between", value1, value2, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPointNotBetween(String value1, String value2) {
            addCriterion("out_point not between", value1, value2, "outPoint");
            return (Criteria) this;
        }

        public Criteria andOutPriceIsNull() {
            addCriterion("out_price is null");
            return (Criteria) this;
        }

        public Criteria andOutPriceIsNotNull() {
            addCriterion("out_price is not null");
            return (Criteria) this;
        }

        public Criteria andOutPriceEqualTo(String value) {
            addCriterion("out_price =", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceNotEqualTo(String value) {
            addCriterion("out_price <>", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceGreaterThan(String value) {
            addCriterion("out_price >", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceGreaterThanOrEqualTo(String value) {
            addCriterion("out_price >=", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceLessThan(String value) {
            addCriterion("out_price <", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceLessThanOrEqualTo(String value) {
            addCriterion("out_price <=", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceLike(String value) {
            addCriterion("out_price like", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceNotLike(String value) {
            addCriterion("out_price not like", value, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceIn(List<String> values) {
            addCriterion("out_price in", values, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceNotIn(List<String> values) {
            addCriterion("out_price not in", values, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceBetween(String value1, String value2) {
            addCriterion("out_price between", value1, value2, "outPrice");
            return (Criteria) this;
        }

        public Criteria andOutPriceNotBetween(String value1, String value2) {
            addCriterion("out_price not between", value1, value2, "outPrice");
            return (Criteria) this;
        }

        public Criteria andWasteIsNull() {
            addCriterion("waste is null");
            return (Criteria) this;
        }

        public Criteria andWasteIsNotNull() {
            addCriterion("waste is not null");
            return (Criteria) this;
        }

        public Criteria andWasteEqualTo(String value) {
            addCriterion("waste =", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteNotEqualTo(String value) {
            addCriterion("waste <>", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteGreaterThan(String value) {
            addCriterion("waste >", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteGreaterThanOrEqualTo(String value) {
            addCriterion("waste >=", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteLessThan(String value) {
            addCriterion("waste <", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteLessThanOrEqualTo(String value) {
            addCriterion("waste <=", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteLike(String value) {
            addCriterion("waste like", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteNotLike(String value) {
            addCriterion("waste not like", value, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteIn(List<String> values) {
            addCriterion("waste in", values, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteNotIn(List<String> values) {
            addCriterion("waste not in", values, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteBetween(String value1, String value2) {
            addCriterion("waste between", value1, value2, "waste");
            return (Criteria) this;
        }

        public Criteria andWasteNotBetween(String value1, String value2) {
            addCriterion("waste not between", value1, value2, "waste");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberIsNull() {
            addCriterion("child_out_number is null");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberIsNotNull() {
            addCriterion("child_out_number is not null");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberEqualTo(String value) {
            addCriterion("child_out_number =", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberNotEqualTo(String value) {
            addCriterion("child_out_number <>", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberGreaterThan(String value) {
            addCriterion("child_out_number >", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberGreaterThanOrEqualTo(String value) {
            addCriterion("child_out_number >=", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberLessThan(String value) {
            addCriterion("child_out_number <", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberLessThanOrEqualTo(String value) {
            addCriterion("child_out_number <=", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberLike(String value) {
            addCriterion("child_out_number like", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberNotLike(String value) {
            addCriterion("child_out_number not like", value, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberIn(List<String> values) {
            addCriterion("child_out_number in", values, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberNotIn(List<String> values) {
            addCriterion("child_out_number not in", values, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberBetween(String value1, String value2) {
            addCriterion("child_out_number between", value1, value2, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andChildOutNumberNotBetween(String value1, String value2) {
            addCriterion("child_out_number not between", value1, value2, "childOutNumber");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNull() {
            addCriterion("storage_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNotNull() {
            addCriterion("storage_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageIdEqualTo(Integer value) {
            addCriterion("storage_id =", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotEqualTo(Integer value) {
            addCriterion("storage_id <>", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThan(Integer value) {
            addCriterion("storage_id >", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("storage_id >=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThan(Integer value) {
            addCriterion("storage_id <", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThanOrEqualTo(Integer value) {
            addCriterion("storage_id <=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIn(List<Integer> values) {
            addCriterion("storage_id in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotIn(List<Integer> values) {
            addCriterion("storage_id not in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdBetween(Integer value1, Integer value2) {
            addCriterion("storage_id between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("storage_id not between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andOutTypeIsNull() {
            addCriterion("out_type is null");
            return (Criteria) this;
        }

        public Criteria andOutTypeIsNotNull() {
            addCriterion("out_type is not null");
            return (Criteria) this;
        }

        public Criteria andOutTypeEqualTo(Integer value) {
            addCriterion("out_type =", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeNotEqualTo(Integer value) {
            addCriterion("out_type <>", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeGreaterThan(Integer value) {
            addCriterion("out_type >", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("out_type >=", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeLessThan(Integer value) {
            addCriterion("out_type <", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeLessThanOrEqualTo(Integer value) {
            addCriterion("out_type <=", value, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeIn(List<Integer> values) {
            addCriterion("out_type in", values, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeNotIn(List<Integer> values) {
            addCriterion("out_type not in", values, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeBetween(Integer value1, Integer value2) {
            addCriterion("out_type between", value1, value2, "outType");
            return (Criteria) this;
        }

        public Criteria andOutTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("out_type not between", value1, value2, "outType");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNull() {
            addCriterion("company_id is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNotNull() {
            addCriterion("company_id is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdEqualTo(Long value) {
            addCriterion("company_id =", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotEqualTo(Long value) {
            addCriterion("company_id <>", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThan(Long value) {
            addCriterion("company_id >", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("company_id >=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThan(Long value) {
            addCriterion("company_id <", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThanOrEqualTo(Long value) {
            addCriterion("company_id <=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIn(List<Long> values) {
            addCriterion("company_id in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotIn(List<Long> values) {
            addCriterion("company_id not in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdBetween(Long value1, Long value2) {
            addCriterion("company_id between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotBetween(Long value1, Long value2) {
            addCriterion("company_id not between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdIsNull() {
            addCriterion("child_order_id is null");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdIsNotNull() {
            addCriterion("child_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdEqualTo(Long value) {
            addCriterion("child_order_id =", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdNotEqualTo(Long value) {
            addCriterion("child_order_id <>", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdGreaterThan(Long value) {
            addCriterion("child_order_id >", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("child_order_id >=", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdLessThan(Long value) {
            addCriterion("child_order_id <", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("child_order_id <=", value, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdIn(List<Long> values) {
            addCriterion("child_order_id in", values, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdNotIn(List<Long> values) {
            addCriterion("child_order_id not in", values, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdBetween(Long value1, Long value2) {
            addCriterion("child_order_id between", value1, value2, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andChildOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("child_order_id not between", value1, value2, "childOrderId");
            return (Criteria) this;
        }

        public Criteria andInputerIdIsNull() {
            addCriterion("inputer_id is null");
            return (Criteria) this;
        }

        public Criteria andInputerIdIsNotNull() {
            addCriterion("inputer_id is not null");
            return (Criteria) this;
        }

        public Criteria andInputerIdEqualTo(Long value) {
            addCriterion("inputer_id =", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotEqualTo(Long value) {
            addCriterion("inputer_id <>", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdGreaterThan(Long value) {
            addCriterion("inputer_id >", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdGreaterThanOrEqualTo(Long value) {
            addCriterion("inputer_id >=", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdLessThan(Long value) {
            addCriterion("inputer_id <", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdLessThanOrEqualTo(Long value) {
            addCriterion("inputer_id <=", value, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdIn(List<Long> values) {
            addCriterion("inputer_id in", values, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotIn(List<Long> values) {
            addCriterion("inputer_id not in", values, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdBetween(Long value1, Long value2) {
            addCriterion("inputer_id between", value1, value2, "inputerId");
            return (Criteria) this;
        }

        public Criteria andInputerIdNotBetween(Long value1, Long value2) {
            addCriterion("inputer_id not between", value1, value2, "inputerId");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeIsNull() {
            addCriterion("out_storage_time is null");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeIsNotNull() {
            addCriterion("out_storage_time is not null");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeEqualTo(Date value) {
            addCriterionForJDBCDate("out_storage_time =", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("out_storage_time <>", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("out_storage_time >", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("out_storage_time >=", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeLessThan(Date value) {
            addCriterionForJDBCDate("out_storage_time <", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("out_storage_time <=", value, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeIn(List<Date> values) {
            addCriterionForJDBCDate("out_storage_time in", values, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("out_storage_time not in", values, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("out_storage_time between", value1, value2, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andOutStorageTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("out_storage_time not between", value1, value2, "outStorageTime");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIsNull() {
            addCriterion("total_money is null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIsNotNull() {
            addCriterion("total_money is not null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyEqualTo(String value) {
            addCriterion("total_money =", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotEqualTo(String value) {
            addCriterion("total_money <>", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyGreaterThan(String value) {
            addCriterion("total_money >", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyGreaterThanOrEqualTo(String value) {
            addCriterion("total_money >=", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLessThan(String value) {
            addCriterion("total_money <", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLessThanOrEqualTo(String value) {
            addCriterion("total_money <=", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLike(String value) {
            addCriterion("total_money like", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotLike(String value) {
            addCriterion("total_money not like", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIn(List<String> values) {
            addCriterion("total_money in", values, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotIn(List<String> values) {
            addCriterion("total_money not in", values, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBetween(String value1, String value2) {
            addCriterion("total_money between", value1, value2, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotBetween(String value1, String value2) {
            addCriterion("total_money not between", value1, value2, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIsNull() {
            addCriterion("assembly_type is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIsNotNull() {
            addCriterion("assembly_type is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeEqualTo(Integer value) {
            addCriterion("assembly_type =", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotEqualTo(Integer value) {
            addCriterion("assembly_type <>", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeGreaterThan(Integer value) {
            addCriterion("assembly_type >", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("assembly_type >=", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeLessThan(Integer value) {
            addCriterion("assembly_type <", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeLessThanOrEqualTo(Integer value) {
            addCriterion("assembly_type <=", value, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeIn(List<Integer> values) {
            addCriterion("assembly_type in", values, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotIn(List<Integer> values) {
            addCriterion("assembly_type not in", values, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeBetween(Integer value1, Integer value2) {
            addCriterion("assembly_type between", value1, value2, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andAssemblyTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("assembly_type not between", value1, value2, "assemblyType");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIsNull() {
            addCriterion("custom_order_id is null");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIsNotNull() {
            addCriterion("custom_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdEqualTo(String value) {
            addCriterion("custom_order_id =", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotEqualTo(String value) {
            addCriterion("custom_order_id <>", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdGreaterThan(String value) {
            addCriterion("custom_order_id >", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("custom_order_id >=", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLessThan(String value) {
            addCriterion("custom_order_id <", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLessThanOrEqualTo(String value) {
            addCriterion("custom_order_id <=", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdLike(String value) {
            addCriterion("custom_order_id like", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotLike(String value) {
            addCriterion("custom_order_id not like", value, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdIn(List<String> values) {
            addCriterion("custom_order_id in", values, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotIn(List<String> values) {
            addCriterion("custom_order_id not in", values, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdBetween(String value1, String value2) {
            addCriterion("custom_order_id between", value1, value2, "customOrderId");
            return (Criteria) this;
        }

        public Criteria andCustomOrderIdNotBetween(String value1, String value2) {
            addCriterion("custom_order_id not between", value1, value2, "customOrderId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}