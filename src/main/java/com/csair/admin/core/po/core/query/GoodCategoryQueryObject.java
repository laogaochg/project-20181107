package com.csair.admin.core.po.core.query;

/**
 * laogaochg
 * 2017/7/19.
 */
public class GoodCategoryQueryObject extends QueryObject {
    private Long certificateId;

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }
}
