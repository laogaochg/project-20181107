package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class Stock implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 组件id
     */
    private Long assemblyId;

    /**
     * 数量
     */
    private String number;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 归属仓库
     */
    private Integer storageId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", number=").append(number);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", storageId=").append(storageId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}