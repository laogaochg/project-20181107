package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class StorageInputAssembly implements Serializable {
    private Long id;

    /**
     * 入库记录id(已经废弃)
     */
    private Long storageInputId;

    /**
     * 组件id
     */
    private Long assemblyId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 组件数量
     */
    private String count;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态:0有效,1已经删除,2无效
     */
    private Integer state;

    /**
     * 添加时间
     */
    private Date createDate;

    /**
     * 仓库1,临时处理,这个字段保存为出库点数
     */
    private String expressId;

    /**
     * 入库单号
     */
    private String inputStorageNumber;

    /**
     * 子入库单号
     */
    private String childInputStorageNumber;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 类型:1:正常入库,2:外协入库
     */
    private Integer type;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 操作人id
     */
    private Long inputerId;

    /**
     * 入库时间
     */
    private Date inputStorageTime;

    /**
     * 客户单号
     */
    private String customOrderId;

    /**
     * 点数
     */
    private String point;

    /**
     * 客户
     */
    private String custom;

    /**
     * 物料类型,(成品，组件，物料)(冗余字段必须同步更新)
     */
    private Integer assemblyType;

    /**
     * 单价
     */
    private String price;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStorageInputId() {
        return storageInputId;
    }

    public void setStorageInputId(Long storageInputId) {
        this.storageInputId = storageInputId;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count == null ? null : count.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getExpressId() {
        return expressId;
    }

    public void setExpressId(String expressId) {
        this.expressId = expressId == null ? null : expressId.trim();
    }

    public String getInputStorageNumber() {
        return inputStorageNumber;
    }

    public void setInputStorageNumber(String inputStorageNumber) {
        this.inputStorageNumber = inputStorageNumber == null ? null : inputStorageNumber.trim();
    }

    public String getChildInputStorageNumber() {
        return childInputStorageNumber;
    }

    public void setChildInputStorageNumber(String childInputStorageNumber) {
        this.childInputStorageNumber = childInputStorageNumber == null ? null : childInputStorageNumber.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getInputerId() {
        return inputerId;
    }

    public void setInputerId(Long inputerId) {
        this.inputerId = inputerId;
    }

    public Date getInputStorageTime() {
        return inputStorageTime;
    }

    public void setInputStorageTime(Date inputStorageTime) {
        this.inputStorageTime = inputStorageTime;
    }

    public String getCustomOrderId() {
        return customOrderId;
    }

    public void setCustomOrderId(String customOrderId) {
        this.customOrderId = customOrderId == null ? null : customOrderId.trim();
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point == null ? null : point.trim();
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom == null ? null : custom.trim();
    }

    public Integer getAssemblyType() {
        return assemblyType;
    }

    public void setAssemblyType(Integer assemblyType) {
        this.assemblyType = assemblyType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", storageInputId=").append(storageInputId);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", unit=").append(unit);
        sb.append(", count=").append(count);
        sb.append(", remark=").append(remark);
        sb.append(", state=").append(state);
        sb.append(", createDate=").append(createDate);
        sb.append(", expressId=").append(expressId);
        sb.append(", inputStorageNumber=").append(inputStorageNumber);
        sb.append(", childInputStorageNumber=").append(childInputStorageNumber);
        sb.append(", storageId=").append(storageId);
        sb.append(", type=").append(type);
        sb.append(", companyId=").append(companyId);
        sb.append(", inputerId=").append(inputerId);
        sb.append(", inputStorageTime=").append(inputStorageTime);
        sb.append(", customOrderId=").append(customOrderId);
        sb.append(", point=").append(point);
        sb.append(", custom=").append(custom);
        sb.append(", assemblyType=").append(assemblyType);
        sb.append(", price=").append(price);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}