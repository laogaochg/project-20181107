package com.csair.admin.core.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderPoQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public OrderPoQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNull() {
            addCriterion("assembly_id is null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIsNotNull() {
            addCriterion("assembly_id is not null");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdEqualTo(Long value) {
            addCriterion("assembly_id =", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotEqualTo(Long value) {
            addCriterion("assembly_id <>", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThan(Long value) {
            addCriterion("assembly_id >", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("assembly_id >=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThan(Long value) {
            addCriterion("assembly_id <", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdLessThanOrEqualTo(Long value) {
            addCriterion("assembly_id <=", value, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdIn(List<Long> values) {
            addCriterion("assembly_id in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotIn(List<Long> values) {
            addCriterion("assembly_id not in", values, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdBetween(Long value1, Long value2) {
            addCriterion("assembly_id between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andAssemblyIdNotBetween(Long value1, Long value2) {
            addCriterion("assembly_id not between", value1, value2, "assemblyId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNull() {
            addCriterion("company_id is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNotNull() {
            addCriterion("company_id is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdEqualTo(Long value) {
            addCriterion("company_id =", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotEqualTo(Long value) {
            addCriterion("company_id <>", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThan(Long value) {
            addCriterion("company_id >", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("company_id >=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThan(Long value) {
            addCriterion("company_id <", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThanOrEqualTo(Long value) {
            addCriterion("company_id <=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIn(List<Long> values) {
            addCriterion("company_id in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotIn(List<Long> values) {
            addCriterion("company_id not in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdBetween(Long value1, Long value2) {
            addCriterion("company_id between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotBetween(Long value1, Long value2) {
            addCriterion("company_id not between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNull() {
            addCriterion("storage_id is null");
            return (Criteria) this;
        }

        public Criteria andStorageIdIsNotNull() {
            addCriterion("storage_id is not null");
            return (Criteria) this;
        }

        public Criteria andStorageIdEqualTo(Integer value) {
            addCriterion("storage_id =", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotEqualTo(Integer value) {
            addCriterion("storage_id <>", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThan(Integer value) {
            addCriterion("storage_id >", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("storage_id >=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThan(Integer value) {
            addCriterion("storage_id <", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdLessThanOrEqualTo(Integer value) {
            addCriterion("storage_id <=", value, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdIn(List<Integer> values) {
            addCriterion("storage_id in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotIn(List<Integer> values) {
            addCriterion("storage_id not in", values, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdBetween(Integer value1, Integer value2) {
            addCriterion("storage_id between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andStorageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("storage_id not between", value1, value2, "storageId");
            return (Criteria) this;
        }

        public Criteria andInputCountIsNull() {
            addCriterion("input_count is null");
            return (Criteria) this;
        }

        public Criteria andInputCountIsNotNull() {
            addCriterion("input_count is not null");
            return (Criteria) this;
        }

        public Criteria andInputCountEqualTo(String value) {
            addCriterion("input_count =", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountNotEqualTo(String value) {
            addCriterion("input_count <>", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountGreaterThan(String value) {
            addCriterion("input_count >", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountGreaterThanOrEqualTo(String value) {
            addCriterion("input_count >=", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountLessThan(String value) {
            addCriterion("input_count <", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountLessThanOrEqualTo(String value) {
            addCriterion("input_count <=", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountLike(String value) {
            addCriterion("input_count like", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountNotLike(String value) {
            addCriterion("input_count not like", value, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountIn(List<String> values) {
            addCriterion("input_count in", values, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountNotIn(List<String> values) {
            addCriterion("input_count not in", values, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountBetween(String value1, String value2) {
            addCriterion("input_count between", value1, value2, "inputCount");
            return (Criteria) this;
        }

        public Criteria andInputCountNotBetween(String value1, String value2) {
            addCriterion("input_count not between", value1, value2, "inputCount");
            return (Criteria) this;
        }

        public Criteria andOutCountIsNull() {
            addCriterion("out_count is null");
            return (Criteria) this;
        }

        public Criteria andOutCountIsNotNull() {
            addCriterion("out_count is not null");
            return (Criteria) this;
        }

        public Criteria andOutCountEqualTo(String value) {
            addCriterion("out_count =", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountNotEqualTo(String value) {
            addCriterion("out_count <>", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountGreaterThan(String value) {
            addCriterion("out_count >", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountGreaterThanOrEqualTo(String value) {
            addCriterion("out_count >=", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountLessThan(String value) {
            addCriterion("out_count <", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountLessThanOrEqualTo(String value) {
            addCriterion("out_count <=", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountLike(String value) {
            addCriterion("out_count like", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountNotLike(String value) {
            addCriterion("out_count not like", value, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountIn(List<String> values) {
            addCriterion("out_count in", values, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountNotIn(List<String> values) {
            addCriterion("out_count not in", values, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountBetween(String value1, String value2) {
            addCriterion("out_count between", value1, value2, "outCount");
            return (Criteria) this;
        }

        public Criteria andOutCountNotBetween(String value1, String value2) {
            addCriterion("out_count not between", value1, value2, "outCount");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIsNull() {
            addCriterion("total_money is null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIsNotNull() {
            addCriterion("total_money is not null");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyEqualTo(String value) {
            addCriterion("total_money =", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotEqualTo(String value) {
            addCriterion("total_money <>", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyGreaterThan(String value) {
            addCriterion("total_money >", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyGreaterThanOrEqualTo(String value) {
            addCriterion("total_money >=", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLessThan(String value) {
            addCriterion("total_money <", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLessThanOrEqualTo(String value) {
            addCriterion("total_money <=", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyLike(String value) {
            addCriterion("total_money like", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotLike(String value) {
            addCriterion("total_money not like", value, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyIn(List<String> values) {
            addCriterion("total_money in", values, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotIn(List<String> values) {
            addCriterion("total_money not in", values, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyBetween(String value1, String value2) {
            addCriterion("total_money between", value1, value2, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andTotalMoneyNotBetween(String value1, String value2) {
            addCriterion("total_money not between", value1, value2, "totalMoney");
            return (Criteria) this;
        }

        public Criteria andPaidCountIsNull() {
            addCriterion("paid_count is null");
            return (Criteria) this;
        }

        public Criteria andPaidCountIsNotNull() {
            addCriterion("paid_count is not null");
            return (Criteria) this;
        }

        public Criteria andPaidCountEqualTo(String value) {
            addCriterion("paid_count =", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountNotEqualTo(String value) {
            addCriterion("paid_count <>", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountGreaterThan(String value) {
            addCriterion("paid_count >", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountGreaterThanOrEqualTo(String value) {
            addCriterion("paid_count >=", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountLessThan(String value) {
            addCriterion("paid_count <", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountLessThanOrEqualTo(String value) {
            addCriterion("paid_count <=", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountLike(String value) {
            addCriterion("paid_count like", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountNotLike(String value) {
            addCriterion("paid_count not like", value, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountIn(List<String> values) {
            addCriterion("paid_count in", values, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountNotIn(List<String> values) {
            addCriterion("paid_count not in", values, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountBetween(String value1, String value2) {
            addCriterion("paid_count between", value1, value2, "paidCount");
            return (Criteria) this;
        }

        public Criteria andPaidCountNotBetween(String value1, String value2) {
            addCriterion("paid_count not between", value1, value2, "paidCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountIsNull() {
            addCriterion("should_get_count is null");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountIsNotNull() {
            addCriterion("should_get_count is not null");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountEqualTo(String value) {
            addCriterion("should_get_count =", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountNotEqualTo(String value) {
            addCriterion("should_get_count <>", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountGreaterThan(String value) {
            addCriterion("should_get_count >", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountGreaterThanOrEqualTo(String value) {
            addCriterion("should_get_count >=", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountLessThan(String value) {
            addCriterion("should_get_count <", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountLessThanOrEqualTo(String value) {
            addCriterion("should_get_count <=", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountLike(String value) {
            addCriterion("should_get_count like", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountNotLike(String value) {
            addCriterion("should_get_count not like", value, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountIn(List<String> values) {
            addCriterion("should_get_count in", values, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountNotIn(List<String> values) {
            addCriterion("should_get_count not in", values, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountBetween(String value1, String value2) {
            addCriterion("should_get_count between", value1, value2, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andShouldGetCountNotBetween(String value1, String value2) {
            addCriterion("should_get_count not between", value1, value2, "shouldGetCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountIsNull() {
            addCriterion("waste_count is null");
            return (Criteria) this;
        }

        public Criteria andWasteCountIsNotNull() {
            addCriterion("waste_count is not null");
            return (Criteria) this;
        }

        public Criteria andWasteCountEqualTo(String value) {
            addCriterion("waste_count =", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountNotEqualTo(String value) {
            addCriterion("waste_count <>", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountGreaterThan(String value) {
            addCriterion("waste_count >", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountGreaterThanOrEqualTo(String value) {
            addCriterion("waste_count >=", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountLessThan(String value) {
            addCriterion("waste_count <", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountLessThanOrEqualTo(String value) {
            addCriterion("waste_count <=", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountLike(String value) {
            addCriterion("waste_count like", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountNotLike(String value) {
            addCriterion("waste_count not like", value, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountIn(List<String> values) {
            addCriterion("waste_count in", values, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountNotIn(List<String> values) {
            addCriterion("waste_count not in", values, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountBetween(String value1, String value2) {
            addCriterion("waste_count between", value1, value2, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andWasteCountNotBetween(String value1, String value2) {
            addCriterion("waste_count not between", value1, value2, "wasteCount");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andResertCountIsNull() {
            addCriterion("resert_count is null");
            return (Criteria) this;
        }

        public Criteria andResertCountIsNotNull() {
            addCriterion("resert_count is not null");
            return (Criteria) this;
        }

        public Criteria andResertCountEqualTo(String value) {
            addCriterion("resert_count =", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountNotEqualTo(String value) {
            addCriterion("resert_count <>", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountGreaterThan(String value) {
            addCriterion("resert_count >", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountGreaterThanOrEqualTo(String value) {
            addCriterion("resert_count >=", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountLessThan(String value) {
            addCriterion("resert_count <", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountLessThanOrEqualTo(String value) {
            addCriterion("resert_count <=", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountLike(String value) {
            addCriterion("resert_count like", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountNotLike(String value) {
            addCriterion("resert_count not like", value, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountIn(List<String> values) {
            addCriterion("resert_count in", values, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountNotIn(List<String> values) {
            addCriterion("resert_count not in", values, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountBetween(String value1, String value2) {
            addCriterion("resert_count between", value1, value2, "resertCount");
            return (Criteria) this;
        }

        public Criteria andResertCountNotBetween(String value1, String value2) {
            addCriterion("resert_count not between", value1, value2, "resertCount");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}