package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class StorageInput implements Serializable {
    private Long id;

    /**
     * 添加时间
     */
    private Date createDate;

    /**
     * 快递单号
     */
    private String expressId;

    /**
     * 入库单号
     */
    private String inputStorageNumber;

    /**
     * 进货总价
     */
    private String totalPrice;

    /**
     * 总点数
     */
    private String totalPoint;

    /**
     * 总数量
     */
    private String totalCount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 状态:0有效,1已经删除,2无效
     */
    private Integer state;

    /**
     * 对应订单号(父)
     */
    private String parentOrderId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 操作人id
     */
    private Long inputerId;

    /**
     * 入库时间
     */
    private Date inputStorageTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getExpressId() {
        return expressId;
    }

    public void setExpressId(String expressId) {
        this.expressId = expressId == null ? null : expressId.trim();
    }

    public String getInputStorageNumber() {
        return inputStorageNumber;
    }

    public void setInputStorageNumber(String inputStorageNumber) {
        this.inputStorageNumber = inputStorageNumber == null ? null : inputStorageNumber.trim();
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice == null ? null : totalPrice.trim();
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint == null ? null : totalPoint.trim();
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount == null ? null : totalCount.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getParentOrderId() {
        return parentOrderId;
    }

    public void setParentOrderId(String parentOrderId) {
        this.parentOrderId = parentOrderId == null ? null : parentOrderId.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getInputerId() {
        return inputerId;
    }

    public void setInputerId(Long inputerId) {
        this.inputerId = inputerId;
    }

    public Date getInputStorageTime() {
        return inputStorageTime;
    }

    public void setInputStorageTime(Date inputStorageTime) {
        this.inputStorageTime = inputStorageTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createDate=").append(createDate);
        sb.append(", expressId=").append(expressId);
        sb.append(", inputStorageNumber=").append(inputStorageNumber);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", totalPoint=").append(totalPoint);
        sb.append(", totalCount=").append(totalCount);
        sb.append(", remark=").append(remark);
        sb.append(", storageId=").append(storageId);
        sb.append(", state=").append(state);
        sb.append(", parentOrderId=").append(parentOrderId);
        sb.append(", companyId=").append(companyId);
        sb.append(", inputerId=").append(inputerId);
        sb.append(", inputStorageTime=").append(inputStorageTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}