package com.csair.admin.core.po.core;

/**
 * 添加文章上传图片pojo
 */
public class ImagesData {
    /**
     * 图片路径
     */
    String src;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

}
