package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;

public class OrderPo implements Serializable {
    /**
     * 子入库订单号
     */
    private Long id;

    /**
     * 组件ID
     */
    private Long assemblyId;

    /**
     * 父订单号
     */
    private Long orderId;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 入库数量
     */
    private String inputCount;

    /**
     * 已出库数量
     */
    private String outCount;

    /**
     * 总金额
     */
    private String totalMoney;

    /**
     * 已结金额
     */
    private String paidCount;

    /**
     * 应收金额
     */
    private String shouldGetCount;

    /**
     * 损耗数量
     */
    private String wasteCount;

    /**
     * 状态0正常1没有计算总金额
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 剩余数量
     */
    private String resertCount;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public String getInputCount() {
        return inputCount;
    }

    public void setInputCount(String inputCount) {
        this.inputCount = inputCount == null ? null : inputCount.trim();
    }

    public String getOutCount() {
        return outCount;
    }

    public void setOutCount(String outCount) {
        this.outCount = outCount == null ? null : outCount.trim();
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney == null ? null : totalMoney.trim();
    }

    public String getPaidCount() {
        return paidCount;
    }

    public void setPaidCount(String paidCount) {
        this.paidCount = paidCount == null ? null : paidCount.trim();
    }

    public String getShouldGetCount() {
        return shouldGetCount;
    }

    public void setShouldGetCount(String shouldGetCount) {
        this.shouldGetCount = shouldGetCount == null ? null : shouldGetCount.trim();
    }

    public String getWasteCount() {
        return wasteCount;
    }

    public void setWasteCount(String wasteCount) {
        this.wasteCount = wasteCount == null ? null : wasteCount.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getResertCount() {
        return resertCount;
    }

    public void setResertCount(String resertCount) {
        this.resertCount = resertCount == null ? null : resertCount.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", orderId=").append(orderId);
        sb.append(", companyId=").append(companyId);
        sb.append(", storageId=").append(storageId);
        sb.append(", inputCount=").append(inputCount);
        sb.append(", outCount=").append(outCount);
        sb.append(", totalMoney=").append(totalMoney);
        sb.append(", paidCount=").append(paidCount);
        sb.append(", shouldGetCount=").append(shouldGetCount);
        sb.append(", wasteCount=").append(wasteCount);
        sb.append(", type=").append(type);
        sb.append(", remark=").append(remark);
        sb.append(", resertCount=").append(resertCount);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}