package com.csair.admin.core.po;

import java.io.Serializable;

public class AssemblyProperty implements Serializable {
    /**
     * 块id
     */
    private Long blockId;

    /**
     * 球泡或灯条id
     */
    private Long propertyId;

    /**
     * 数量
     */
    private Long count;

    private static final long serialVersionUID = 1L;

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", blockId=").append(blockId);
        sb.append(", propertyId=").append(propertyId);
        sb.append(", count=").append(count);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}