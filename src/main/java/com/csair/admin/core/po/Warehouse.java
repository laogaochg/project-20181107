package com.csair.admin.core.po;

import java.util.Date;

/**
 * 仓库关联信息
 * @author: Yin ShiHua
 * 
 */
public class Warehouse {
	
	 private Long id;//id
	 private String warehouseName;//仓库所属公司
	 private String warehousePhone;//仓库所属公司电话
	 private String warehouseAddress;//仓库所属公司地址
	 private String warehouseFax;//仓库所属公司传真
	 private String warehouseEmail;//仓库所属公司邮箱
	 private String warehouseManager;//仓库管理员
	 private Long warehouseManagId;//仓库管理员ID
	 private String state;//状态
	 private Date createDate;
	 
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	public String getWarehousePhone() {
		return warehousePhone;
	}
	public void setWarehousePhone(String warehousePhone) {
		this.warehousePhone = warehousePhone;
	}
	public String getWarehouseAddress() {
		return warehouseAddress;
	}
	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}
	
	public String getWarehouseFax() {
		return warehouseFax;
	}
	public void setWarehouseFax(String warehouseFax) {
		this.warehouseFax = warehouseFax;
	}
	public String getWarehouseEmail() {
		return warehouseEmail;
	}
	public void setWarehouseEmail(String warehouseEmail) {
		this.warehouseEmail = warehouseEmail;
	}

	public String getWarehouseManager() {
		return warehouseManager;
	}
	public void setWarehouseManager(String warehouseManager) {
		this.warehouseManager = warehouseManager;
	}

	public Long getWarehouseManagId() {
		return warehouseManagId;
	}
	public void setWarehouseManagId(Long warehouseManagId) {
		this.warehouseManagId = warehouseManagId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "Warehouse [id=" + id + ", warehouseName=" + warehouseName + ", warehousePhone=" + warehousePhone
				+ ", warehouseAddress=" + warehouseAddress + ", warehouseFax=" + warehouseFax + ", warehouseEmail="
				+ warehouseEmail + ", warehouseManager=" + warehouseManager + ", warehouseManagId=" + warehouseManagId
				+ ", state=" + state + ", createDate=" + createDate + "]";
	}
	 
	 
	 
}
