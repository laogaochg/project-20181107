package com.csair.admin.core.po;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.csair.admin.config.PlatformException;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.util.SpringContextUtil;
import com.csair.admin.util.StringUtil;

public class StorageOutRecord implements Serializable {
    private Long id;

    /**
     * 组件id
     */
    private Long assemblyId;

    /**
     * 组件数量
     */
    private String count;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态:0有效,1已经删除,2无效
     */
    private Integer state;

    /**
     * 添加时间
     */
    private Date createDate;

    /**
     * 出货单号
     */
    private String outNumber;

    /**
     * 出货点数
     */
    private String outPoint;

    /**
     * 出库单价
     * 这个字段已经被对应入库的单价覆盖
     */
    private String outPrice;

    /**
     * 损耗数量
     */
    private String waste;

    /**
     * 子出货单号
     */
    private String childOutNumber;

    /**
     * 仓库号:有1,2,3
     */
    private Integer storageId;

    /**
     * 出库类型:1,正常出库，2：外协出库
     */
    private Integer outType;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 父入库订单号
     */
    private Long orderId;

    /**
     * 入库子订单号
     */
    private Long childOrderId;

    /**
     * 操作人id
     */
    private Long inputerId;

    /**
     * 出库时间
     */
    private Date outStorageTime;

    /**
     * 出货应收金额
     */
    private String totalMoney;

    /**
     * 物料类型,(成品，组件，物料)(冗余字段必须同步更新)
     */
    private Integer assemblyType;

    private String customOrderId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count == null ? null : count.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOutNumber() {
        return outNumber;
    }

    public void setOutNumber(String outNumber) {
        this.outNumber = outNumber == null ? null : outNumber.trim();
    }

    public String getOutPoint() {
//        if (orderId == null || getChildOrderId() == null || assemblyId == null) {
//            return outPoint;
//        } else {
            StorageInputAssemblyQuery e = new StorageInputAssemblyQuery();
            e.createCriteria().andInputStorageNumberEqualTo(orderId + "").andAssemblyIdEqualTo(assemblyId);
            List<StorageInputAssembly> select = SpringContextUtil.getBean("storageInputService", StorageInputService.class).selectByExample(e);
            if (select.size() == 0) throw new PlatformException(0, "没找到子订单号");
            StorageInputAssembly storageInputAssembly = select.get(0);
            return StringUtil.to2Point(storageInputAssembly.getExpressId());
//        }
    }

    public void setOutPoint(String outPoint) {
        this.outPoint = outPoint == null ? null : outPoint.trim();
    }

    public String getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(String outPrice) {
        this.outPrice = outPrice == null ? null : outPrice.trim();
    }

    public String getWaste() {
        return waste;
    }

    public void setWaste(String waste) {
        this.waste = waste == null ? null : waste.trim();
    }

    public String getChildOutNumber() {
        return childOutNumber;
    }

    public void setChildOutNumber(String childOutNumber) {
        this.childOutNumber = childOutNumber == null ? null : childOutNumber.trim();
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getOutType() {
        return outType;
    }

    public void setOutType(Integer outType) {
        this.outType = outType;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getChildOrderId() {
        return childOrderId;
    }

    public void setChildOrderId(Long childOrderId) {
        this.childOrderId = childOrderId;
    }

    public Long getInputerId() {
        return inputerId;
    }

    public void setInputerId(Long inputerId) {
        this.inputerId = inputerId;
    }

    public Date getOutStorageTime() {
        return outStorageTime;
    }

    public void setOutStorageTime(Date outStorageTime) {
        this.outStorageTime = outStorageTime;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney == null ? null : totalMoney.trim();
    }

    public Integer getAssemblyType() {
        return assemblyType;
    }

    public void setAssemblyType(Integer assemblyType) {
        this.assemblyType = assemblyType;
    }

    public String getCustomOrderId() {
        return customOrderId;
    }

    public void setCustomOrderId(String customOrderId) {
        this.customOrderId = customOrderId == null ? null : customOrderId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", assemblyId=").append(assemblyId);
        sb.append(", count=").append(count);
        sb.append(", remark=").append(remark);
        sb.append(", state=").append(state);
        sb.append(", createDate=").append(createDate);
        sb.append(", outNumber=").append(outNumber);
        sb.append(", outPoint=").append(outPoint);
        sb.append(", outPrice=").append(outPrice);
        sb.append(", waste=").append(waste);
        sb.append(", childOutNumber=").append(childOutNumber);
        sb.append(", storageId=").append(storageId);
        sb.append(", outType=").append(outType);
        sb.append(", companyId=").append(companyId);
        sb.append(", orderId=").append(orderId);
        sb.append(", childOrderId=").append(childOrderId);
        sb.append(", inputerId=").append(inputerId);
        sb.append(", outStorageTime=").append(outStorageTime);
        sb.append(", totalMoney=").append(totalMoney);
        sb.append(", assemblyType=").append(assemblyType);
        sb.append(", customOrderId=").append(customOrderId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}