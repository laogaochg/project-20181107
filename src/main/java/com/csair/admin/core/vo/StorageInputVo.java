package com.csair.admin.core.vo;

import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.StorageInputAssembly;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:37
 */
@Data
public class StorageInputVo extends StorageInputAssembly {
    /**
     * 类型:1:正常入库,2:外协入库
     */
    private Integer type;
    private Long id;
    /**
     *
     */
    private String expressId;
    private String inputStorageTimeString;
    private Long companyId;
    private String deliveryNumber;
    private String remark;
    private String assemblyPrice;
    private String count;
    private String standard;
    private String unit;
    private String unitPointCount;
    private Long parentOrderId;
    //入库单号
    private String inputStorageNumber;
    private String totalPrice;
    private String companyName;
    private String assemblyName;
    private String point;
    private String assemblyCode;//组件编码
    private String customOrderId;//
    private String assemblyTypeName;

    private Assembly assembly;
    private StorageInputAssembly storageInputAssembly;



    public StorageInputVo(StorageInputAssembly b) {
        StorageInputVo a = this;
        a.setCount(b.getCount());
        a.setExpressId(b.getExpressId());
        a.setId(b.getId());
        a.setInputStorageNumber(b.getInputStorageNumber());
        a.inputStorageTimeString = new SimpleDateFormat("yyyy-MM-dd").format(b.getInputStorageTime());
        a.setRemark(b.getRemark());
        a.setType(b.getType());
        a.setUnit(b.getUnit());
        a.setPoint(b.getPoint());
        a.setCustomOrderId(b.getCustomOrderId());
        a.setCustom(b.getCustom());
        this.companyId = b.getCompanyId();
        assemblyTypeName = b.getAssemblyType() == 3 ? "成品" : "物料";
        a.storageInputAssembly = b;
    }

    public StorageInputVo() {
    }

}
