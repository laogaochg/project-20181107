package com.csair.admin.core.vo;

import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/3/30 14:24
 */
@Data
public class GoodsSkuVo {
    private String attr;
    private String price;
}
