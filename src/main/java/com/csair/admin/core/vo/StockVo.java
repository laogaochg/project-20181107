package com.csair.admin.core.vo;

import com.csair.admin.core.dto.StockMonthCountDto;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.Stock;
import lombok.Data;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/21 0:35
 */
@Data
public class StockVo extends Stock {
    private Assembly assembly;
    private Stock stock;
    private StockMonthCountDto stockMonthCountDto;

}
