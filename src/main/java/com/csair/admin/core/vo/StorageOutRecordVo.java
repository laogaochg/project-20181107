package com.csair.admin.core.vo;

import com.csair.admin.config.PlatformException;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.StorageOutRecord;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/29 22:27
 */
@Data
public class StorageOutRecordVo extends StorageOutRecord {
    private String companyName;
    private String assemblyName;
    private String assemblyModelName;
    private Assembly assembly;
    private StorageOutRecord storageOutRecord;
    /**
     * 出库点数
     */
    private String outPoint;
    public StorageOutRecordVo() {
    }

    public void setOutStorageTime(String date) {
        try {
            setOutStorageTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            throw new PlatformException(0, "时间格式不对");
        }
    }

    public StorageOutRecordVo(StorageOutRecord b) {
        this.storageOutRecord =b;
        StorageOutRecordVo a = this;
        a.setAssemblyId(b.getAssemblyId());
        a.setChildOutNumber(b.getChildOutNumber());
        a.setCompanyId(b.getCompanyId());
        a.setCount(b.getCount());
        a.setCreateDate(b.getCreateDate());
        a.setId(b.getId());
        a.setInputerId(b.getInputerId());
        a.setOutNumber(b.getOutNumber());
        a.setOutStorageTime(b.getOutStorageTime());
        a.setOutType(b.getOutType());
        a.setRemark(b.getRemark());
        a.setState(b.getState());
        a.setStorageId(b.getStorageId());
        a.setWaste(b.getWaste());
        a.setChildOrderId(b.getChildOrderId());
        a.setOrderId(b.getOrderId());
        a.setOutPrice(b.getOutPrice());
        a.setOutPoint(b.getOutPoint());

    }


    public StorageOutRecord convertClass() {
        StorageOutRecord result = new StorageOutRecord();
        result.setAssemblyId(this.getAssemblyId());
//        result.setAssemblyPrice(this.getAssemblyPrice());
//        result.setColour(this.getColour());
//        result.setCompanyId(this.getCompanyId());
//        result.setCount(this.getCount());
//        result.setCreateDate(this.getCreateDate());
//        result.setId(this.getId());
//        result.setInputerId(this.getInputerId());
//        result.setOrderId(this.getOrderId());
//        result.setOutNumber(this.getOutNumber());
//        result.setOutStorageTime(this.getOutStorageTime());
//        result.setRemark(this.getRemark());
//        result.setStandard(this.getStandard());
//        result.setState(this.getState());
//        result.setStorageId(this.getStorageId());
//        result.setSubtotalPrice(this.getSubtotalPrice());
//        result.setUnit(this.getUnit());
        return result;
    }
}
