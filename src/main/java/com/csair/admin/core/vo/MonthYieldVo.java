package com.csair.admin.core.vo;

import com.csair.admin.core.dto.MonthYieldDto;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.MonthYield;
import lombok.Data;

import java.util.Date;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/6 14:30
 */
@Data
public class MonthYieldVo {
    private MonthYield monthYield;
    /**
     * 归属月份(yyyy-MM)
     */
    private String month;
    private Assembly assembly;
    private MonthYieldDto monthYieldDto;
    private Long assemblyId;
    private String beforeMonthNotOutCount;
    private String currentMonthNotOutCount;
    /**
     * 归属时间
     */
    private Date date;
}
