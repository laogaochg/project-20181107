package com.csair.admin.core.service;

import com.csair.admin.core.dto.StorageInputDto;
import com.csair.admin.core.dto.StorageInputQueryObject;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.vo.StorageInputVo;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:33
 */
public interface StorageInputService {


    PageResult<StorageInputVo> pageQuery(StorageInputQueryObject qo);

    List<StorageInputAssembly> selectByExample(StorageInputAssemblyQuery example);

    int deleteByPrimaryKey(Long id);


    int updateByPrimaryKey(Company record);

    int insert(HashMap<String, String> vo);

    int update(HashMap<String, String> vo);
}
