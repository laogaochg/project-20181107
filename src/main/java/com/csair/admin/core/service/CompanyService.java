package com.csair.admin.core.service;

import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.core.PageResult;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:33
 */
public interface CompanyService {
    PageResult<Company> pageQuery(CompanyQueryObject qo);

    int deleteByPrimaryKey(Long id);

    int insert(Company record);

    int updateByPrimaryKey(Company record);
    Company selectByPrimaryKey(Long id);
}
