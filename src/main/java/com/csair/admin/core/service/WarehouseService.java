package com.csair.admin.core.service;

import com.csair.admin.core.po.Warehouse;
import com.csair.admin.core.po.WarehouseQueryObject;
import com.csair.admin.core.po.core.PageResult;

/**
 * 关联企业打印信息查询
 * @author: Yin ShiHua
 */
public interface WarehouseService {

	 
	 Warehouse selectWareByUserId();//单个查询

	 int updateWare(Warehouse qo);//修改

	 int insertWare(Warehouse qo);//新增
}
