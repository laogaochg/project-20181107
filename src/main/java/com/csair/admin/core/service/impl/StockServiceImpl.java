package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.StockDao;
import com.csair.admin.core.dto.StockExportDto;
import com.csair.admin.core.dto.StockMonthCountDto;
import com.csair.admin.core.dto.StockQueryObject;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.Stock;
import com.csair.admin.core.po.StockQuery;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.StockService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.vo.StockVo;
import com.csair.admin.util.DateUtil;
import com.csair.admin.util.ExcelUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/21 0:27
 */
@Service
public class StockServiceImpl implements StockService {
    @Autowired
    private StockDao stockDao;
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private StorageInputService storageInputService;
    @Autowired
    private StorageOutRecordService storageOutRecordService;
    private static Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);

    @Override
    public List<Stock> selectByExample(StockQuery example) {
        return stockDao.selectByExample(example);
    }

    @Override
    public PageResult<StockVo> pageQuery(StockQueryObject qo) {
        StockQuery e = new StockQuery();
        e.setPageNo(qo.getCurrentPage());
        e.setPageSize(qo.getPageSize());
        StockQuery.Criteria c = e.createCriteria();
//        if (qo.getOrderId() != null && qo.getOrderId() > 0) {
//            c.andOrderIdEqualTo(qo.getOrderId());
//        }
//        if (qo.getCompanyId() != null && qo.getCompanyId() > 0) {
//            c.andCompanyIdEqualTo(qo.getCompanyId());
//        }
        if (StringUtils.isNotBlank(qo.getName())) {
            AssemblyQuery ex = new AssemblyQuery();
            ex.createCriteria().andNameEqualTo(qo.getName());
            List<Assembly> assemblyList = assemblyService.selectByExample(ex);
            List<Long> ids = new ArrayList<>();
            for (Assembly a : assemblyList) {
                ids.add(a.getId());
            }
            if(ids.size()==0) ids.add(-1L);
            c.andAssemblyIdIn(ids);
        }


        int i = stockDao.countByExample(e);
        List<Stock> stocks = stockDao.selectByExample(e);

        List<StockVo> vos = new ArrayList<>();
        List<Long> assemblyIds = new ArrayList<>();
        for (Stock s : stocks) {
            StockVo vo = new StockVo();
            vo.setAssembly(assemblyService.selectByPrimaryKey(s.getAssemblyId()));
            vo.setStock(s);
            vos.add(vo);
            assemblyIds.add(s.getAssemblyId());
        }
        Map<Long, StockMonthCountDto> totalData = getTotalData(assemblyIds, qo);
        for (StockVo s : vos) {
            s.setStockMonthCountDto(totalData.get(s.getStock().getAssemblyId()));
        }
        return new PageResult<>(vos, i, qo.getCurrentPage(), qo.getPageSize());
    }

    private Map<Long, StockMonthCountDto> getTotalData(List<Long> assemblyIds, StockQueryObject qo) {
        Date endDate = new Date();
        try {
            if (StringUtils.isNotBlank(qo.getBeginDate())) {
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                endDate = f.parse(qo.getBeginDate());
            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Map<Long, StockMonthCountDto> data = new HashMap<>();
        if (assemblyIds.size() > 0) {
            Date monthBegin = DateUtil.getMonthBegin(endDate);
            Date end = DateUtils.addMonths(monthBegin, 1);
            {
                StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
                StorageInputAssemblyQuery.Criteria c = ex.createCriteria();
                c
                        .andAssemblyIdIn(assemblyIds)
                        .andInputStorageTimeLessThan(end)
                        .andInputStorageTimeGreaterThanOrEqualTo(monthBegin);
                List<StorageInputAssembly> inputAssemblyList = storageInputService.selectByExample(ex);
                for (StorageInputAssembly in : inputAssemblyList) {
                    if (data.get(in.getAssemblyId()) == null) data.put(in.getAssemblyId(), new StockMonthCountDto());
                    StockMonthCountDto dto = data.get(in.getAssemblyId());
                    dto.setInputCount(dto.getInputCount().add(new BigDecimal(in.getCount())).setScale(0));
                }
            }
            {
                StorageOutRecordQuery ex = new StorageOutRecordQuery();
                StorageOutRecordQuery.Criteria c = ex.createCriteria();
                c
                        .andAssemblyIdIn(assemblyIds)
                        .andOutStorageTimeLessThan(end)
                        .andOutStorageTimeGreaterThanOrEqualTo(monthBegin);
                List<StorageOutRecord> inputAssemblyList = storageOutRecordService.selectByExample(ex);
                for (StorageOutRecord in : inputAssemblyList) {
                    if (data.get(in.getAssemblyId()) == null) data.put(in.getAssemblyId(), new StockMonthCountDto());
                    StockMonthCountDto dto = data.get(in.getAssemblyId());
                    dto.setOutCount(dto.getOutCount().add(new BigDecimal(in.getCount())).setScale(0));
                }
            }
        }
        return data;
    }


    @Override
    public XSSFWorkbook exportFile(StockQueryObject qo) {
        Date endDate;
        Date beginDate;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            endDate = format.parse(qo.getEndDate());
            beginDate = format.parse(qo.getBeginDate());
        } catch (ParseException e) {
            e.printStackTrace();
            throw new PlatformException(0, "时间参数不对");
        }
        Map<String, StockExportDto> rowData = getDateListMap(endDate, beginDate);
        AssemblyQuery assE = new AssemblyQuery();
        assE.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andTypeNotEqualTo(3);//不导成品
        List<Assembly> assemblyList = assemblyService.selectByExample(assE);
        StockQuery ex = new StockQuery();
        ex.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<Stock> stocks = stockDao.selectByExample(ex);
        XSSFWorkbook hw = getHSSFSheet(assemblyList, rowData, stocks);
        return hw;
    }

    private XSSFWorkbook getHSSFSheet(List<Assembly> assemblyList, Map<String, StockExportDto> rowData, List<Stock> stocks) {
        XSSFWorkbook wb = new XSSFWorkbook ();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = wb.createSheet("sheet1");
        int totalLength = assemblyList.size() + 2;
        //设置列宽
        for (int i = 0; i < totalLength; i++) {
            sheet.setColumnWidth(i, 4000);
        }
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 7000);
        int rowCount = 0;
        XSSFCellStyle baseRowStyle = wb.createCellStyle();
        baseRowStyle.setAlignment(HorizontalAlignment.CENTER);
        baseRowStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        {
            XSSFRow row = sheet.createRow(rowCount++);
            ExcelUtil.setCellValue(row, 0, wb, "日期").setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 1, wb, "入库/出库单号").setCellStyle(baseRowStyle);
            for (int i = 0; i < assemblyList.size(); i++) {
                Assembly assembly = assemblyList.get(i);
                ExcelUtil.setCellValue(row, i + 2, wb, assembly.getCode()).setCellStyle(baseRowStyle);
            }
        }
        //根据时间排序好
        Map<Date, List<StockExportDto>> data = new TreeMap<>();
        for (Map.Entry<String, StockExportDto> entry : rowData.entrySet()) {
            List<StockExportDto> list = data.get(entry.getValue().getDate());
            if (list == null) list = new ArrayList<>();
            list.add(entry.getValue());
            data.put(entry.getValue().getDate(), list);
        }
        {
            for (Map.Entry<Date, List<StockExportDto>> entry : data.entrySet()) {
                for (StockExportDto dto : entry.getValue()) {
                    XSSFRow row = sheet.createRow(rowCount++);
                    ExcelUtil.setCellValue(row, 0, wb, format.format(dto.getDate())).setCellStyle(baseRowStyle);
                    ExcelUtil.setCellValue(row, 1, wb, dto.getOrderId()).setCellStyle(baseRowStyle);
                    for (int i = 0; i < assemblyList.size(); i++) {
                        Assembly a = assemblyList.get(i);
                        if (dto.getAssemblyMap().containsKey(a.getCode())) {
                            ExcelUtil.setCellValue(row, i + 2, wb, new BigDecimal(dto.getAssemblyMap().get(a.getCode())))
                                    .setCellStyle(baseRowStyle);
                        }
                    }
                }
            }
        }
        {
            XSSFRow row = sheet.createRow(rowCount++);
            ExcelUtil.setCellValue(row, 1, wb, "库存").setCellStyle(baseRowStyle);
            for (int i = 0; i < assemblyList.size(); i++) {
                Assembly a = assemblyList.get(i);
                BigDecimal stock = new BigDecimal("0");
                for (Stock s : stocks) {
                    if (a.getId().equals(s.getAssemblyId())) {
                        stock = new BigDecimal(s.getNumber());
                    }
                }
                ExcelUtil.setCellValue(row, i + 2, wb, stock).setCellStyle(baseRowStyle);
            }
        }
        return wb;
    }


    private Map<String, StockExportDto> getDateListMap(Date endDate, Date beginDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        StorageInputAssemblyQuery inputE = new StorageInputAssemblyQuery();
        inputE.createCriteria()
                .andInputStorageTimeGreaterThanOrEqualTo(beginDate)
                .andInputStorageTimeLessThanOrEqualTo(endDate)
                .andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<StorageInputAssembly> inputList = storageInputService.selectByExample(inputE);
        StorageOutRecordQuery outE = new StorageOutRecordQuery();
        outE.createCriteria()
                .andOutStorageTimeGreaterThanOrEqualTo(beginDate)
                .andOutStorageTimeLessThanOrEqualTo(endDate)
                .andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<StorageOutRecord> outList = storageOutRecordService.selectByExample(outE);
        Map<String, List<StorageOutRecord>> outMap = new HashMap<>();
        for (StorageOutRecord s : outList) {
            List<StorageOutRecord> list = outMap.get(s.getOrderId());
            if (list == null) list = new ArrayList<>(5);
            list.add(s);
            outMap.put(s.getOrderId() + "", list);
        }
        //行以时间+（出入库单决定）
        Map<String, StockExportDto> rowData = new TreeMap<>();
        for (StorageInputAssembly s : inputList) {
            String key = s.getInputStorageNumber();
            StockExportDto dto = rowData.get(key);
            if (dto == null) dto = new StockExportDto();
            dto.setDate(s.getInputStorageTime());
            dto.setOrderId(key);
            dto.getAssemblyMap().put(assemblyService.selectByPrimaryKey(s.getAssemblyId()).getCode(), s.getCount());
            rowData.put(key, dto);
        }
        for (StorageOutRecord s : outList) {
            String key = s.getOutNumber();
            StockExportDto dto = rowData.get(key);
            if (dto == null) dto = new StockExportDto();
            dto.setDate(s.getOutStorageTime());
            dto.setOrderId(key);
            dto.getAssemblyMap().put(assemblyService.selectByPrimaryKey(s.getAssemblyId()).getCode(), "-" + s.getCount());
            rowData.put(key, dto);
        }

        return rowData;
    }

    @Override
    public void updateStock(String changeCount, Assembly assembly) {
        if (assembly.getType().equals(3)) return;//成品暂时不计算存量
        StockQuery e = new StockQuery();
        e.createCriteria().andAssemblyIdEqualTo(assembly.getId()).andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<Stock> list = stockDao.selectByExample(e);
        if (list.size() == 0) {
            Stock s = new Stock();
            s.setAssemblyId(assembly.getId());
            s.setUpdateTime(new Date());
            s.setNumber(changeCount);
            s.setStorageId(UserUtil.getUserStorageId());
            stockDao.insert(s);
        } else {
            Stock s = list.get(0);
            s.setUpdateTime(new Date());
            BigDecimal add = new BigDecimal(s.getNumber()).add(new BigDecimal(changeCount));
            if (add.compareTo(BigDecimal.ZERO) < 0) {
                throw new PlatformException(0, "库存出现负值，请确认输入数值正确");
            }
            s.setNumber(add.toString());
            stockDao.updateByPrimaryKeySelective(s);
        }
    }
}
