package com.csair.admin.core.service.impl;

import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.MonthYieldDao;
import com.csair.admin.core.dto.MonthYieldDto;
import com.csair.admin.core.dto.MonthYieldXlsFileDto;
import com.csair.admin.core.dto.StockExportDto;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.Stock;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.MonthYield;
import com.csair.admin.core.po.MonthYieldQuery;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.MonthYieldService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.vo.MonthYieldVo;
import com.csair.admin.util.ExcelUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/6 14:26
 */
@Service
public class MonthYieldServiceImpl implements MonthYieldService {
    @Autowired
    private MonthYieldDao monthYieldDao;
    @Autowired
    private StorageOutRecordService storageOutRecordService;
    @Autowired
    private StorageInputService storageInputService;
    @Autowired
    private AssemblyService assemblyService;


    @Override
    public void update(MonthYieldVo vo) {
        SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat mm = new SimpleDateFormat("yyyy-MM");
        try {
            Date parse = mm.parse(mm.format(dd.parse(vo.getMonth())));
            MonthYieldQuery e = new MonthYieldQuery();
            e.createCriteria().andAssemblyIdEqualTo(vo.getAssemblyId())
                    .andStorageIdEqualTo(UserUtil.getUserStorageId()).andMonthEqualTo(parse);
            List<MonthYield> old = monthYieldDao.selectByExample(e);
            MonthYield m = new MonthYield();
            m.setAssemblyId(vo.getAssemblyId());
            m.setStorageId(UserUtil.getUserStorageId());
            m.setMonth(parse);
            m.setBeforeMonthNotOutCount(vo.getBeforeMonthNotOutCount());
            m.setCurrentMonthNotOutCount(vo.getCurrentMonthNotOutCount());
            m.setStatus(1);
            if (old.size() == 0) {
                m.setCreateTime(new Date());
                monthYieldDao.insertSelective(m);
            } else {
                m.setId(old.get(0).getId());
                monthYieldDao.updateByPrimaryKeySelective(m);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new PlatformException(0, "时间格式不对");
        }

    }

    @Override
    public List<MonthYieldVo> pageQuery(MonthYieldVo vo) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date before;
        try {
            SimpleDateFormat mmFormat = new SimpleDateFormat("yyyy-MM");
            String month = mmFormat.format(new Date());
            before = mmFormat.parse(month);
            if (StringUtils.hasText(vo.getMonth())) {
                before = mmFormat.parse(vo.getMonth());
            } else {
                vo.setMonth(format.format(before));
            }
            vo.setDate(before);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new PlatformException(0, "时间格式不对");
        }
        Date date = DateUtils.addMonths(before, 1);
        //原始点数 原始点数合计 //物料编号 出货数量	单价 出货点数 出货点数合计
        Map<Long, MonthYieldDto> map = getLongMonthYieldDtoMap(before, date);
        MonthYieldQuery exam = new MonthYieldQuery();
        exam.createCriteria().andMonthEqualTo(before).andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<MonthYield> monthYields = monthYieldDao.selectByExample(exam);
        //减上月未出货数量	加本月未出货数量
        for (MonthYield m : monthYields) {
            map.get(m.getAssemblyId()).setMonthYield(m);
        }
        if (map.keySet().size() == 0) return new ArrayList<>();
        AssemblyQuery example = new AssemblyQuery();
        example.createCriteria().andIdIn(new ArrayList<>(map.keySet()));
        example.setOrderByClause(" company_id ,name , model_name ");
        List<Assembly> assemblyList = assemblyService.selectByExample(example);

        List<MonthYieldVo> result = new ArrayList<>();
        for (Assembly a : assemblyList) {
            MonthYieldVo v = new MonthYieldVo();
            v.setAssembly(a);
            v.setMonthYieldDto(map.get(a.getId()));
            result.add(v);
        }
        return result;
    }

    @Override
    public HSSFWorkbook exportMonthYield(String month) {
        MonthYieldVo vo = new MonthYieldVo();
        vo.setMonth(month);
        List<MonthYieldVo> list = pageQuery(vo);
//        产量表
        SimpleDateFormat sf = new SimpleDateFormat("yyyy年MM月");

        String title = sf.format(vo.getDate()) + "产量表";
        //物料编号	出货数量	单价	减上月未出货数量	加本月未出货数量	实际数量	原始点数	出货点数	原始点数合计	出货点数合计
//         备注
        List<MonthYieldXlsFileDto> data = new ArrayList<>();
        for (MonthYieldVo v : list) {
            data.add(new MonthYieldXlsFileDto(v));
        }
        return getHSSFSheet(title, data);
    }

    private HSSFWorkbook getHSSFSheet(String title, List<MonthYieldXlsFileDto> rowData) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("sheet1");
        int totalLength = 11;
        //设置列宽
        for (int i = 0; i < totalLength; i++) {
            sheet.setColumnWidth(i, 4000);
        }
        sheet.setColumnWidth(11, 10000);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, totalLength));
        int rowCount = 0;
        HSSFCellStyle baseRowStyle = ExcelUtil.getBaseRowStyle(wb);
        {
            HSSFRow row = sheet.createRow(rowCount++);
            row.setHeightInPoints((short) 35);
            ExcelUtil.setCellValue(row, 0, wb, title).setCellStyle(baseRowStyle);
        }
        {
            HSSFRow row = sheet.createRow(rowCount++);
            row.setHeightInPoints((short) 25);
            List<String> titleList = MonthYieldXlsFileDto.getTitle();
            for (int i = 0; i < titleList.size(); i++) {
                String value = titleList.get(i);
                ExcelUtil.setCellValue(row, i, wb, value).setCellStyle(baseRowStyle);
            }
        }
        {
            for (MonthYieldXlsFileDto dto : rowData) {
                int i = 0;
                HSSFRow row = sheet.createRow(rowCount++);
                row.setHeightInPoints((short) 20);
                for (Object o : dto.getRowData()) {
                    ExcelUtil.setCellValue(row, i++, wb, o).setCellStyle(baseRowStyle);
                }
            }
        }
        {
            //出货数量 上月未出货数量	本月未出货数量	 实际数量 原始点数合计	出货点数合计	金额
            Double outCount = 0D;//出货数量
            Double beforeNotCount = 0D;//上月未出货数量
            Double currentNotCount = 0D;//本月未出货数量
            Double actualCount = 0D;//实际数量
            Double inputPoint = 0D;//原始点数合计
            Double outPoint = 0D;//出货点数合计
            Double money = 0D;//金额
            for (MonthYieldXlsFileDto m : rowData) {
                outCount += m.getCount();
                beforeNotCount += m.getBeforeCount();
                currentNotCount += m.getCurrentCount();
                actualCount += m.getActualCount();
                inputPoint += m.getTotalInputPoint();
                outPoint += m.getTotalOutPoint();
                money += m.getMoney();
            }
            HSSFRow row = sheet.createRow(rowCount++);
            row.setHeightInPoints((short) 35);
            ExcelUtil.setCellValue(row, 0, wb, "汇总").setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 2, wb, outCount).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 3, wb, beforeNotCount).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 4, wb, currentNotCount).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 5, wb, actualCount).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 9, wb, inputPoint).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 10, wb, outPoint).setCellStyle(baseRowStyle);
            ExcelUtil.setCellValue(row, 11, wb, money).setCellStyle(baseRowStyle);
        }
        return wb;
    }

    private Map<Long, MonthYieldDto> getLongMonthYieldDtoMap(Date before, Date date) {
        StorageOutRecordQuery e = new StorageOutRecordQuery();
        //只找成品
        e.createCriteria().andOutStorageTimeGreaterThanOrEqualTo(before)
                .andAssemblyTypeEqualTo(3)
                .andOutTypeEqualTo(1)//只找正常出库的
                .andOutStorageTimeLessThan(date)
                .andStorageIdEqualTo(UserUtil.getUserStorageId());
        List<StorageOutRecord> outList = storageOutRecordService.selectByExample(e);

        Map<Long, MonthYieldDto> map = new HashMap<>();
        for (StorageOutRecord out : outList) {
            Long assemblyId = out.getAssemblyId();
            if (map.get(assemblyId) == null) map.put(assemblyId, new MonthYieldDto());
            MonthYieldDto dto = map.get(assemblyId);
            StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
            ex.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andChildInputStorageNumberEqualTo(out.getChildOrderId() + "");
            List<StorageInputAssembly> childOrder = storageInputService.selectByExample(ex);
            if (childOrder.size() == 0) continue;
            //原始点数 原始点数合计
            dto.getInputList().add(childOrder.get(0));
            //覆盖字段
            out.setOutPrice(childOrder.get(0).getPrice());
            //物料编号 出货数量	单价 出货点数 出货点数合计
            dto.getOutList().add(out);
        }
        return map;
    }


}
