package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.CompanyDao;
import com.csair.admin.core.dao.StorageInputAssemblyDao;
import com.csair.admin.core.dao.StorageInputDao;
import com.csair.admin.core.dto.InsertStorageInputDto;
import com.csair.admin.core.dto.StorageInputQueryObject;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.StorageInput;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.core.service.StockService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.vo.StorageInputVo;
import com.csair.admin.util.DateUtil;
import com.csair.admin.util.LoggerUtils;
import com.csair.admin.util.StringUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.shiro.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 14:02
 */
@Service("storageInputService")
public class StorageInputServiceImpl implements StorageInputService {
    @Autowired
    private StorageInputDao storageInputDao;
    @Autowired
    private StorageInputAssemblyDao storageInputAssemblyDao;
    @Autowired
    private CompanyDao companyDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private StorageOutRecordService storageOutRecordService;
    @Autowired
    private StockService stockService;
    private static Logger logger = LoggerFactory.getLogger(StorageInputServiceImpl.class);

    @Override
    public List<StorageInputAssembly> selectByExample(StorageInputAssemblyQuery example) {
        return storageInputAssemblyDao.selectByExample(example);
    }

    @Override
    public PageResult<StorageInputVo> pageQuery(StorageInputQueryObject qo) {
        StorageInputAssemblyQuery condition = new StorageInputAssemblyQuery();
        StorageInputAssemblyQuery.Criteria c = condition.createCriteria();
        c.andStateEqualTo(0);//只找能看到的
        c.andStorageIdEqualTo(UserUtil.getUserStorageId());//只查当前仓库
        condition.setPageNo(qo.getCurrentPage());
        condition.setPageSize(qo.getPageSize());
        if (qo.getId() != null && qo.getId() > 0) {
            c.andIdEqualTo(qo.getId());
        }
        if (qo.getType() != null && qo.getType() > 0) {
            c.andTypeEqualTo(qo.getType());
        }
        if (qo.getAssemblyType() != null && qo.getAssemblyType() > 0) {
            if (qo.getAssemblyType() == 3) {
                c.andAssemblyTypeEqualTo(qo.getAssemblyType());
            } else {
                c.andAssemblyTypeNotEqualTo(3);
            }
        }
        if (StringUtils.hasText(qo.getOrderId())) {
            c.andInputStorageNumberEqualTo(qo.getOrderId());
        }
        if (qo.getCompanyId() != null && qo.getCompanyId() > 0) {
            c.andCompanyIdEqualTo(qo.getCompanyId());
        }
        if (StringUtils.hasText(qo.getExpressId())) {
            c.andExpressIdEqualTo(qo.getExpressId());
        }
        if (StringUtils.hasText(qo.getInputStorageNumber())) {
            c.andInputStorageNumberEqualTo(qo.getInputStorageNumber());
        }
        if (StringUtils.hasText(qo.getCustomOrderId())) {
            c.andCustomOrderIdEqualTo(qo.getCustomOrderId());
        }
        if (StringUtils.hasText(qo.getAssemblyName())) {
            List<Long> ids = new ArrayList<>();
            List<Assembly> assemblyList = assemblyService.queryByName(qo.getAssemblyName());
            for (Assembly a : assemblyList) {
                ids.add(a.getId());
            }
            if (ids.size() == 0) {
                ids.add(-1L);//没找到就让它找不到
            }
            c.andAssemblyIdIn(ids);
        }
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (StringUtils.hasText(qo.getBeginDate())) {
                c.andInputStorageTimeGreaterThanOrEqualTo(DateUtil.getBeginTime(f.parse(qo.getBeginDate())));
            }
            if (StringUtils.hasText(qo.getEndDate())) {
                c.andInputStorageTimeLessThanOrEqualTo(DateUtil.getTodayEndTime(f.parse(qo.getEndDate())));
            }
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("时间转换异常:{}", e);
        }
        if (qo.getIsExportFile() != null && qo.getIsExportFile()) {
            condition.setOrderByClause(" create_date asc ");
        } else {
            condition.setOrderByClause(" create_date desc ");
        }
        List<StorageInputAssembly> list = storageInputAssemblyDao.selectByExample(condition);

        List<StorageInputVo> listData = new ArrayList<>();
        for (StorageInputAssembly a : list) {
            StorageInputVo vo = new StorageInputVo();
            vo.setAssemblyId(a.getAssemblyId());
            vo.setCompanyId(a.getCompanyId());
            vo.setCount(a.getCount());
            vo.setInputStorageNumber(a.getInputStorageNumber());
            vo.setDeliveryNumber(a.getInputStorageNumber());
            vo.setExpressId(a.getExpressId());
            vo.setId(a.getId());
            vo.setInputStorageTimeString(f.format(a.getInputStorageTime()));
            vo.setRemark(a.getRemark());
            vo.setUnit(a.getUnit());
            vo.setType(a.getType());
            vo.setPoint(a.getPoint());
            vo.setCustom(a.getCustom());
            vo.setStorageInputAssembly(a);
            vo.setCustomOrderId(a.getCustomOrderId());
            vo.setAssemblyTypeName(a.getAssemblyType() == 3 ? "成品" : "物料");
            Company company = companyDao.selectByPrimaryKey(a.getCompanyId());
            if (company != null) vo.setCompanyName(company.getName());
            Assembly assembly = assemblyService.selectByPrimaryKey(a.getAssemblyId());
            vo.setAssembly(assembly);
            if (assembly != null) vo.setStandard(assembly.getModelName());
            if (assembly != null) vo.setAssemblyName(assembly.getName());
            if (assembly != null) vo.setAssemblyCode(assembly.getCode());
            listData.add(vo);
        }
        Integer totalCount = storageInputAssemblyDao.countByExample(condition);
        PageResult<StorageInputVo> result = new PageResult<>(listData, totalCount, qo.getCurrentPage(), qo.getPageSize());
        return result;
    }

    private List<StorageInputAssembly> selectByStorageInputId(Long storageInputId) {
        StorageInputAssemblyQuery condition = new StorageInputAssemblyQuery();
        condition.createCriteria().andStorageInputIdEqualTo(storageInputId);
        return storageInputAssemblyDao.selectByExample(condition);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        StorageInput a = new StorageInput();
        a.setId(id);
        a.setState(1);
        LoggerUtils.logger("删除入库纪录", "id:" + id);
        StorageInputAssembly old = storageInputAssemblyDao.selectByPrimaryKey(id);
        logger.info("删除入库纪录,内容是{}", JSON.toJSONString(old));
        storageInputAssemblyDao.deleteByPrimaryKey(id);
        orderService.deleteInputStorage(old, 1);
        if(old.getAssemblyType()!=3){
            StorageOutRecordQuery qo = new StorageOutRecordQuery();
            qo.createCriteria().andOrderIdEqualTo(Long.valueOf(old.getInputStorageNumber()));
            List<StorageOutRecord> outRecordList = storageOutRecordService.selectByExample(qo);
            if(outRecordList.size()>0){
                throw new PlatformException(0,"已经有出库，不能删除");
            }
        }
        stockService.updateStock("-" + old.getCount(), assemblyService.selectByPrimaryKey(old.getAssemblyId()));
        return 0;
    }


    @Override
    public int insert(HashMap<String, String> vo) {
        logger.info("新建入库：参数：{}", JSON.toJSONString(vo));
        List<InsertStorageInputDto> list = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            list.add(new InsertStorageInputDto(vo.get("assemblyName_" + i)
                            , vo.get("assemblyModelName_" + i)
                            , vo.get("count_" + i)
                            , vo.get("point_" + i)
                            , vo.get("customOrderId_" + i)
                            , vo.get("expressId_" + i)
                            , vo.get("price_" + i)
                    )
            );
        }
        String orderId = StringUtil.getOrderId();
        List<StorageInputAssembly> insertList = new ArrayList<>();
        List<Assembly> assemblyList = new ArrayList<>();
        Set<Long> set = new HashSet<>();
        for (InsertStorageInputDto dto : list) {
            StorageInputAssembly a = null;
            if (StringUtils.hasText(dto.getName())) {
                StorageInputAssembly inputAssembly = getStorageInputAssembly(vo, dto);
                Assembly assembly = queryAssemblyId(dto, vo.get("updateType"));
                inputAssembly.setAssemblyId(assembly.getId());
                if (!set.add(inputAssembly.getAssemblyId())) throw new PlatformException(0, "同一批订单，不能有两个同样的组件。");
                inputAssembly.setAssemblyType(assembly.getType());
                inputAssembly.setInputStorageNumber(orderId);
                inputAssembly.setCustom(vo.get("custom"));
                LoggerUtils.logger("新建入库纪录", "");
                storageInputAssemblyDao.insert(inputAssembly);
                assemblyList.add(assembly);
                insertList.add(inputAssembly);
            }
        }
        if (insertList.size() == 0) {
            throw new PlatformException(0, "请输入要入库的明细");
        }
        for (int i = 0; i < insertList.size(); i++) {
            StorageInputAssembly a = insertList.get(i);
            Assembly assembly = assemblyService.selectByPrimaryKey(a.getAssemblyId());
            orderService.insertInputStorage(a, assembly, 1);
            stockService.updateStock(a.getCount(), assembly);
        }
        return 0;
    }

    private StorageInputAssembly getStorageInputAssembly(HashMap<String, String> vo, InsertStorageInputDto dto) {
        if (!StringUtil.isNumeric(dto.getCount())) {
            throw new PlatformException(0, "数量必须为数字");
        }
        Date now = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        StorageInputAssembly a = new StorageInputAssembly();
        String remark = vo.get("remark");
        a.setCount(dto.getCount());
        a.setInputStorageTime(now);//入库时间
        if (StringUtils.hasText(vo.get("inputStorageTime"))) {
            Date inputStorageTime;
            try {
                inputStorageTime = f.parse(vo.get("inputStorageTime"));
                a.setInputStorageTime(inputStorageTime);//入库时间
            } catch (ParseException e) {
                throw new PlatformException(0, "时间格式不对", e);
            }
        }
        if ("1".equals(vo.get("updateType"))) {
            a.setCustomOrderId(dto.getCustomOrderId());
        } else {
            a.setCustomOrderId(vo.get("customOrderId"));
        }
        a.setPoint(dto.getPoint());
        a.setState(0);
        a.setRemark(remark);
        a.setCreateDate(now);
        a.setType(Integer.valueOf(vo.get("type")));
        a.setStorageId(UserUtil.getUserStorageId());
        a.setCompanyId(Long.valueOf(vo.get("companyId")));
        a.setInputerId(UserUtil.getUserId());
        a.setChildInputStorageNumber(StringUtil.getChildOrderId());
        a.setExpressId(dto.getExpressId());
        a.setPrice(dto.getPrice());
        return a;
    }

    private Assembly queryAssemblyId(InsertStorageInputDto dto, String updateType) {
        String code = dto.getName();
        if (StringUtils.hasText(code)) {
            if ("1".equals(updateType)) {
                return assemblyService.selectByPrimaryKey(Long.valueOf(dto.getModelName()));
            }
            AssemblyQuery qo = new AssemblyQuery();
            qo.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andCodeEqualTo(code).andTypeNotEqualTo(3);
            List<Assembly> select = assemblyService.selectByExample(qo);
            String s = String.format("组件编码：%s，没有找到，请确认输入正确！", code);
            if (select == null || select.size() == 0) throw new PlatformException(0, s);
            return select.get(0);
        } else {
            throw new PlatformException(0, "组件名字和型号都必须输入");
        }
    }

    @Override
    public int update(HashMap<String, String> vo) {
        StorageInputAssemblyQuery e = new StorageInputAssemblyQuery();
        StorageInputAssembly old = storageInputAssemblyDao.selectByPrimaryKey(Long.valueOf((vo.get("id") + "").trim()));
        e.createCriteria().andInputStorageNumberEqualTo(old.getInputStorageNumber());
        List<StorageInputAssembly> oldList = storageInputAssemblyDao.selectByExample(e);
        logger.info("修改入库：参数：{}", JSON.toJSONString(vo));
        logger.info("修改入库：之前参数：{}", JSON.toJSONString(oldList));
        List<InsertStorageInputDto> list = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            list.add(new InsertStorageInputDto(
                    vo.get("assemblyName_" + i)
                    , vo.get("assemblyModelName_" + i)
                    , vo.get("count_" + i)
                    , vo.get("point_" + i)
                    , vo.get("customOrderId_" + i)
                    , vo.get("expressId_" + i)
                    , vo.get("price_" + i)
            ));
        }
        String orderId = StringUtil.getOrderId();
        List<StorageInputAssembly> insertList = new ArrayList<>();
        List<Assembly> assemblyList = new ArrayList<>();
        for (InsertStorageInputDto dto : list) {
            if (StringUtils.hasText(dto.getName())) {
                StorageInputAssembly inputAssembly = getStorageInputAssembly(vo, dto);
                Assembly assembly = queryAssemblyId(dto, vo.get("updateType"));
                inputAssembly.setAssemblyId(assembly.getId());
                inputAssembly.setAssemblyType(assembly.getType());
                inputAssembly.setCustom(vo.get("custom"));
//                inputAssembly.setCustomOrderId(vo.get("customOrderId"));
                inputAssembly.setInputStorageNumber(old.getInputStorageNumber());//订单号不能修改
                inputAssembly.setCreateDate(old.getCreateDate());//不会修改的参数
                insertList.add(inputAssembly);
                assemblyList.add(assembly);
            }
        }
        if (insertList.size() == 0) {
            throw new PlatformException(0, "请输入要入库的明细");
        }
        LoggerUtils.logger("修改入库纪录", JSON.toJSONString(insertList));
        storageInputAssemblyDao.deleteByExample(e);
        List<StorageInputAssembly> updateList = new ArrayList<>();
        List<StorageInputAssembly> addList = new ArrayList<>();
        List<StorageInputAssembly> deleteList = new ArrayList<>();
        for (StorageInputAssembly a : insertList) {
            boolean add = true;
            for (StorageInputAssembly b : oldList) {
                if (a.getAssemblyId().equals(b.getAssemblyId())) {
                    //修改的不修改子订单号
                    a.setChildInputStorageNumber(b.getChildInputStorageNumber());
                    updateList.add(a);
                    add = false;
                }
            }
            if (add) addList.add(a);//找不到就是添加的
        }
        for (StorageInputAssembly b : oldList) {
            boolean delete = true;
            for (StorageInputAssembly a : insertList) {
                if (a.getAssemblyId().equals(b.getAssemblyId())) {
                    delete = false;
                }
            }
            if (delete) deleteList.add(b);
        }

        Set<Long> set = new HashSet<>();
        for (StorageInputAssembly a : insertList) {
            if (!set.add(a.getAssemblyId())) throw new PlatformException(0, "同一批订单，不能有两个同样的组件。");
            storageInputAssemblyDao.insert(a);
        }


        //操作订单和库存
        for (StorageInputAssembly a : updateList) {
            Assembly assembly = getAssembly(a, assemblyList);
            orderService.updateInputStorage(a, assembly);
            for (StorageInputAssembly oldA : oldList) {
                if (a.getAssemblyId().equals(oldA.getAssemblyId())) {
                    BigDecimal subtract = new BigDecimal(a.getCount()).subtract(new BigDecimal(oldA.getCount()));
                    stockService.updateStock(subtract.toString(), assembly);
                }
            }
        }
        for (StorageInputAssembly a : addList) {
            Assembly assembly = getAssembly(a, assemblyList);
            orderService.insertInputStorage(a, assembly, 2);
            stockService.updateStock(a.getCount(), assembly);
        }
        for (StorageInputAssembly a : deleteList) {
            orderService.deleteInputStorage(a, 2);
            stockService.updateStock("-" + a.getCount(), getAssembly(a, assemblyList));
        }

        return 0;
    }

    private Assembly getAssembly(StorageInputAssembly a, List<Assembly> assemblyList) {
        for (Assembly assembly : assemblyList) {
            if (a.getAssemblyId().equals(assembly.getId())) return assembly;
        }
//        throw new PlatformException(0, "找不到相关组件。");
        return assemblyService.selectByPrimaryKey(a.getAssemblyId());
    }

    @Override
    public int updateByPrimaryKey(Company record) {
        return 0;
    }
}
