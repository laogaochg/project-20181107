package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dao.CompanyDao;
import com.csair.admin.core.dao.StorageOutRecordDao;
import com.csair.admin.core.dto.InsertStorageInputDto;
import com.csair.admin.core.dto.OutRecordPrintDto;
import com.csair.admin.core.dto.StorageInputQueryObject;
import com.csair.admin.core.dto.StorageOutQueryObject;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.OrderPo;
import com.csair.admin.core.po.OrderPoQuery;
import com.csair.admin.core.po.Stock;
import com.csair.admin.core.po.StockQuery;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.Warehouse;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.core.service.StockService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.service.WarehouseService;
import com.csair.admin.core.vo.StorageOutRecordVo;
import com.csair.admin.util.DateUtil;
import com.csair.admin.util.LoggerUtils;
import com.csair.admin.util.StringUtil;
import com.csair.admin.util.UserUtil;
import org.apache.shiro.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/29 22:25
 */
@Service
public class StorageOutRecordServiceImpl implements StorageOutRecordService {
    @Autowired
    private StorageOutRecordDao storageOutRecordDao;
    @Autowired
    private CompanyDao companyDao;
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private AssemblyDao assemblyDao;
    @Autowired
    private StorageInputService storageInputService;
    @Autowired
    private WarehouseService warehouseService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private StockService stockService;
    private static Logger logger = LoggerFactory.getLogger(StorageOutRecordServiceImpl.class);

    @Override
    public PageResult<StorageOutRecordVo> pageQuery(StorageOutQueryObject qo) {
        StorageOutRecordQuery condition = new StorageOutRecordQuery();
        StorageOutRecordQuery.Criteria c = condition.createCriteria();
        c.andStateEqualTo(0);//只找能看到的
        c.andStorageIdEqualTo(UserUtil.getUserStorageId());//只查当前仓库
        condition.setPageNo(qo.getCurrentPage());
        if (qo.getPageSize() != null && qo.getPageSize() > 0) {
            condition.setPageSize(qo.getPageSize());
        }
        if (qo.getId() != null && qo.getId() > 0) {
            c.andIdEqualTo(qo.getId());
        }
        if (qo.getOrderId() != null && qo.getOrderId() > 0) {
            c.andOrderIdEqualTo(Long.valueOf(qo.getOrderId()));
        }
        if (qo.getCompanyId() != null && qo.getCompanyId() > 0) {
            c.andCompanyIdEqualTo(qo.getCompanyId());
        }
        if (StringUtils.hasText(qo.getOutNumber())) {
            c.andOutNumberEqualTo(qo.getOutNumber());
        }
        if (qo.getAssemblyType() != null && qo.getAssemblyType() > 0) {
            if (qo.getAssemblyType() == 3) {
                c.andAssemblyTypeEqualTo(qo.getAssemblyType());
            } else if (qo.getAssemblyType() == null || qo.getAssemblyType() == 0) {
            } else {
                c.andAssemblyTypeNotEqualTo(3);
            }
        }
        if (StringUtils.hasText(qo.getCustomOrderId())) {
            c.andCustomOrderIdEqualTo(qo.getCustomOrderId());
        }
        if (StringUtils.hasText(qo.getAssemblyName())) {
            List<Long> ids = new ArrayList<>();
            List<Assembly> assemblyList = assemblyService.queryByName(qo.getAssemblyName());
            for (Assembly a : assemblyList) {
                ids.add(a.getId());
            }
            if (ids.size() == 0) {
                ids.add(-1L);//没找到就让它找不到
            }
            c.andAssemblyIdIn(ids);
        }

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (StringUtils.hasText(qo.getBeginDate())) {
                c.andOutStorageTimeGreaterThanOrEqualTo(DateUtil.getBeginTime(f.parse(qo.getBeginDate())));
            }
            if (StringUtils.hasText(qo.getEndDate())) {
                c.andOutStorageTimeLessThanOrEqualTo(DateUtil.getTodayEndTime(f.parse(qo.getEndDate())));
            }
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("时间转换异常:{}", e);
        }
        condition.setOrderByClause(" id DESC");
        List<StorageOutRecord> list = storageOutRecordDao.selectByExample(condition);
        int totalCount = storageOutRecordDao.countByExample(condition);
        List<StorageOutRecordVo> listData = new ArrayList<>();
        for (StorageOutRecord o : list) {

            //找到对应的入库单
            StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
            ex.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andChildInputStorageNumberEqualTo(o.getChildOrderId() + "");
            List<StorageInputAssembly> childOrder = storageInputService.selectByExample(ex);
            if (childOrder.size() == 0) continue;
            //覆盖字段
            o.setOutPrice(childOrder.get(0).getPrice());


            StorageOutRecordVo v = new StorageOutRecordVo(o);
            Company company = companyDao.selectByPrimaryKey(o.getCompanyId());
            if (company != null) {
                v.setCompanyName(company.getName());
            }
            Assembly assembly = assemblyDao.selectByPrimaryKey(o.getAssemblyId());
            if (assembly != null) {
                v.setAssembly(assembly);
                v.setAssemblyName(assembly.getName());
                v.setAssemblyModelName(assembly.getModelName());
            }

            listData.add(v);
        }
        PageResult<StorageOutRecordVo> result = new PageResult<>(listData, totalCount, qo.getCurrentPage(), qo.getPageSize());
        return result;
    }

    @Override
    public int update(Map<String, String> vo) {
        List<StorageOutRecord> insertList = new ArrayList<>();
        StorageOutRecord old = storageOutRecordDao.selectByPrimaryKey(Long.valueOf(vo.get("id")));
        StorageOutRecordQuery e = new StorageOutRecordQuery();
        e.createCriteria().andOutNumberEqualTo(old.getOutNumber());
        List<StorageOutRecord> oldList = storageOutRecordDao.selectByExample(e);
        logger.info("修改出库：原来的数据：{}", oldList);
        storageOutRecordDao.deleteByExample(e);
        LoggerUtils.logger("修改出库记录", "原来的数据" + JSON.toJSONString(oldList));
        for (int i = 0; i < 100; i++) {
            StorageOutRecord s = getStorageOutRecord(vo, i);
            if (s != null) {
                s.setOutNumber(old.getOutNumber());
                s.setState(0);
                s.setCreateDate(old.getCreateDate());
                s.setOutType(Integer.valueOf(vo.get("outType")));
                if (StringUtils.hasText(vo.get("outStorageTime"))) {
                    try {
                        Date outStorageTime = new SimpleDateFormat("yyyy-MM-dd").parse(vo.get("outStorageTime"));
                        s.setOutStorageTime(outStorageTime);
                    } catch (ParseException ee) {
                        throw new PlatformException(0, "时间格式不对");
                    }
                }
                insertList.add(s);
            }
        }
        for (StorageOutRecord s : insertList) {
            storageOutRecordDao.insert(s);
        }

        String remark = "修改出库信息";
        for (StorageOutRecord nowOut : insertList) {
            boolean add = true;
            boolean isProduct = nowOut.getAssemblyType() == 3;
            for (StorageOutRecord oldOut : oldList) {
                if (nowOut.getAssemblyId().equals(oldOut.getAssemblyId())) {
                    BigDecimal c = new BigDecimal(nowOut.getCount()).subtract(new BigDecimal(oldOut.getCount()));
                    BigDecimal w = new BigDecimal(nowOut.getWaste()).subtract(new BigDecimal(oldOut.getWaste()));
                    //修改的不修改子订单号
                    nowOut.setChildOrderId(oldOut.getChildOrderId());
                    if (isProduct) {
                        orderService.outStorage(c, w, remark, nowOut.getChildOrderId());
                    } else {
                        stockService.updateStock(getChangeCount(oldOut, nowOut), assemblyService.selectByPrimaryKey(nowOut.getAssemblyId()));
                        BigDecimal restCount = getRestCount(nowOut.getChildOrderId());
                        if(restCount.compareTo(BigDecimal.ZERO)<0){
                            throw new PlatformException(99,"超过可以出库的数量。");
                        }
                    }
                    add = false;
                }
            }
            if (add) {
                //找不到就是添加的
                if (isProduct) {
                    orderService.outStorage(new BigDecimal(nowOut.getCount()), new BigDecimal(nowOut.getWaste()), remark, nowOut.getChildOrderId());
                } else {
                    stockService.updateStock(getChangeCount(null, nowOut), assemblyService.selectByPrimaryKey(nowOut.getAssemblyId()));
                    BigDecimal restCount = getRestCount(nowOut.getChildOrderId());
                    if(restCount.compareTo(BigDecimal.ZERO)<0){
                        throw new PlatformException(99,"超过可以出库的数量。");
                    }
                }
            }
        }
        for (StorageOutRecord b : oldList) {
            boolean isProduct = b.getAssemblyType() == 3;
            boolean delete = true;
            for (StorageOutRecord a : insertList) {
                if (a.getAssemblyId().equals(b.getAssemblyId())) {
                    delete = false;
                }
            }
            if (delete) {
                if (isProduct) {
                    orderService.outStorage(new BigDecimal("-" + b.getCount()), new BigDecimal("-" + b.getWaste()), remark, b.getChildOrderId());
                } else {
                    stockService.updateStock(getChangeCount(b, null), assemblyService.selectByPrimaryKey(b.getAssemblyId()));
                    BigDecimal restCount = getRestCount(b.getChildOrderId());
                    if(restCount.compareTo(BigDecimal.ZERO)<0){
                        throw new PlatformException(99,"超过可以出库的数量。");
                    }
                }
            }
        }
        return 0;
    }

    @Override
    public int insert(Map<String, String> vo) {
        List<StorageOutRecord> insertList = new ArrayList<>();
        String orderId = StringUtil.getOrderId();
        for (int i = 0; i < 100; i++) {
            StorageOutRecord s = getStorageOutRecord(vo, i);
            if (s != null) {
                s.setOutNumber(orderId);
                s.setState(0);
                s.setCreateDate(new Date());
                if (StringUtils.hasText(vo.get("outStorageTime"))) {
                    try {
                        Date outStorageTime = new SimpleDateFormat("yyyy-MM-dd").parse(vo.get("outStorageTime"));
                        s.setOutStorageTime(outStorageTime);
                    } catch (ParseException e) {
                        throw new PlatformException(0, "时间格式不对");
                    }
                }
                insertList.add(s);
            }

        }
        for (StorageOutRecord s : insertList) {
            s.setOutStorageTime(s.getCreateDate());
            storageOutRecordDao.insert(s);
        }
        for (StorageOutRecord now : insertList) {
            if (now.getAssemblyType() == 3) {//成品才会计算金额
                BigDecimal changeCount = new BigDecimal(now.getCount());
                BigDecimal changeWaste = new BigDecimal(now.getWaste());
                String remark = "新建出库";
                orderService.outStorage(changeCount, changeWaste, remark, now.getChildOrderId());
            } else {
                stockService.updateStock(getChangeCount(null, now), assemblyService.selectByPrimaryKey(now.getAssemblyId()));
                BigDecimal restCount = getRestCount(now.getChildOrderId());
                if(restCount.compareTo(BigDecimal.ZERO)<0){
                    throw new PlatformException(99,"超过可以出库的数量。");
                }
            }
        }
        return 0;
    }

    private StorageOutRecord getStorageOutRecord(Map<String, String> vo, int i) {
        if (!StringUtils.hasText(vo.get("assemblyId_" + i)) && !StringUtils.hasText(vo.get("code_" + i))) {
            return null;
        }
        StorageOutRecord record = new StorageOutRecord();
        record.setChildOutNumber(StringUtil.getChildOrderId());
        Assembly assembly;
        if ("3".equals(vo.get("assemblyType"))) {
            assembly = assemblyService.selectByPrimaryKey(Long.valueOf(vo.get("assemblyId_" + i)));
        } else {
            assembly = assemblyService.selectByCode(vo.get("code_" + i));
        }
        record.setCount(StringUtils.hasText(vo.get("count_" + i)) ? vo.get("count_" + i) : null);
        record.setCustomOrderId(StringUtils.hasText(vo.get("customOrderId_" + i)) ? vo.get("customOrderId_" + i) : null);
//        record.setOutPrice(StringUtils.hasText(vo.get("price_" + i)) ? vo.get("price_" + i) : null);
        record.setRemark(StringUtils.hasText(vo.get("remark_" + i)) ? vo.get("remark_" + i) : null);
        record.setWaste(StringUtils.hasText(vo.get("waste_" + i)) ? vo.get("waste_" + i) : "0");
        if (assembly == null) throw new PlatformException(0, "编码：" + vo.get("code_" + i) + " 没有找到对应的组件或成品");
        record.setAssemblyId(assembly.getId());
        record.setAssemblyType(assembly.getType());

        record.setStorageId(UserUtil.getUserStorageId());
        record.setInputerId(UserUtil.getUserId());
        record.setOutType(Integer.valueOf(vo.get("outType")));
        String orderId = (vo.get("orderId") + "").trim();
        record.setOrderId(Long.valueOf(orderId));
        StorageInputAssemblyQuery e = new StorageInputAssemblyQuery();
        e.createCriteria().andInputStorageNumberEqualTo(orderId).andAssemblyIdEqualTo(record.getAssemblyId());
        List<StorageInputAssembly> select = storageInputService.selectByExample(e);
        if (select.size() == 0) throw new PlatformException(0, "没找到子订单号");
        StorageInputAssembly storageInputAssembly = select.get(0);
        record.setChildOrderId(Long.valueOf(storageInputAssembly.getChildInputStorageNumber()));
        //覆盖字段
        record.setOutPrice(storageInputAssembly.getPrice());
        if (record.getAssemblyType() == 3) {  //成品出库
            if (Integer.valueOf(1).equals(record.getOutType())) {//正常出库
                record.setCompanyId(storageInputAssembly.getCompanyId());
            } else {
                record.setCompanyId(StringUtils.hasText(vo.get("companyId")) ? Long.valueOf(vo.get("companyId")) : null);
            }
            // 计算成品订单金额不和组件绑定 数量*点数*价格
            BigDecimal money = calculateMoney(record);
            record.setTotalMoney(money.toString());
        } else {
            record.setCompanyId(storageInputAssembly.getCompanyId());
        }
//        Assembly assembly = assemblyDao.selectByPrimaryKey(record.getAssemblyId());
//        BigDecimal orderMoney = new BigDecimal(record.getCount()).multiply(new BigDecimal(assembly.getOutPointPrice()).setScale(3, BigDecimal.ROUND_UP));
//        record.setTotalMoney(orderMoney.toString());
        return record;
    }

    private BigDecimal calculateMoney(StorageOutRecord s) {
//        logger.info("数据:{}",JSON.toJSONString(s));
        return new BigDecimal(s.getOutPoint())
                .multiply(new BigDecimal(s.getOutPrice()))
                .multiply(new BigDecimal(s.getCount()));
    }
//    {"assemblyId":243,"assemblyType":3,"childOrderId":20190116208,"childOutNumber":"2019012210582998527",
// "companyId":40,"count":"183100","customOrderId":"1014","inputerId":63,
// "orderId":20190116207,"outPrice":"0.0035","outType":1,"storageId":1,"waste":"0"}

    @Override
    public OutRecordPrintDto printOutRecord(String outNumber) {
        StorageOutRecordQuery e = new StorageOutRecordQuery();
//        e.createCriteria().andOutNumberEqualTo(outNumber);
        List<String> list = new ArrayList<>();
        for (String s : outNumber.split(",")) {
            for (String s1 : s.trim().split("，")) {
                if (StringUtils.hasText(s1)) {
                    list.add(s1.trim());
                }
            }
        }
        if (list.size() == 0) {
            logger.error("要打印的数据为空,{}", outNumber);
            return new OutRecordPrintDto();
        }
        e.createCriteria().andOutNumberIn(list);
        List<StorageOutRecord> outList = storageOutRecordDao.selectByExample(e);
        OutRecordPrintDto result = new OutRecordPrintDto();
        boolean hadSet = false;
        //我方公司信息
        String companyName = warehouseService.selectWareByUserId().getWarehouseName();
        result.setOrderName(companyName + "出货单");
        Warehouse warehouse = warehouseService.selectWareByUserId();
        result.setProcessingPartyAddress(warehouse.getWarehouseAddress());
        result.setProcessingPartyName(warehouse.getWarehouseName());
        result.setProcessingPartyPhone(warehouse.getWarehousePhone());
        result.setOrderId(outNumber);
        result.setDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        List<OutRecordPrintDto.OutRecordPrintItemDto> items = new ArrayList<>();
        OutRecordPrintDto.OutRecordPrintItemDto a = new OutRecordPrintDto.OutRecordPrintItemDto();
        a.setCode("");
        a.setCount("");
        a.setName("");
        a.setRemark("");
        a.setResetCount("");
        a.setStandard("");
        a.setUnit("");
        result.setAssemblyType(1);
        int line = 15; //允许的最大行数
        for (int i = 0; i < line; i++) {
            if (i < outList.size()) {
                StorageOutRecord o = outList.get(i);
                //设置物料类型
                result.setAssemblyType(o.getAssemblyType());
                Assembly assembly = assemblyDao.selectByPrimaryKey(o.getAssemblyId());
                if (o.getAssemblyType() == 3) {
                    if (!hadSet && o.getCompanyId() != null) {
                        Company company = companyDao.selectByPrimaryKey(o.getCompanyId());
                        result.setCompany(company);
                        hadSet = true;
                    }
                    result.setOrderName(companyName + "送货单");
                    OrderPoQuery ex = new OrderPoQuery();
                    ex.createCriteria().andOrderIdEqualTo(o.getOrderId()).andAssemblyIdEqualTo(o.getAssemblyId());
                    List<OrderPo> orderPoList = orderService.selectByExample(ex);
                    OutRecordPrintDto.OutRecordPrintItemDto item =
                            new OutRecordPrintDto.OutRecordPrintItemDto(assembly, o, orderPoList.get(0).getResertCount());


                    StorageInputAssemblyQuery exa = new StorageInputAssemblyQuery();
                    exa.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andChildInputStorageNumberEqualTo(o.getChildOrderId() + "");
                    List<StorageInputAssembly> input = storageInputService.selectByExample(exa);
                    if (input.size() == 0) {
                        logger.error("数据问题，出库对象为:{}", JSON.toJSONString(o));
                        throw new RuntimeException("存在数据问题，请联系处理");
                    }
                    item.setStorageInputAssembly(input.get(0));

                    items.add(item);
                } else {
                    //找物料的库存量
                    /*StockQuery exam = new StockQuery();
                    exam.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andAssemblyIdEqualTo(assembly.getId());
                    List<Stock> stocks = stockService.selectByExample(exam);
                    if (stocks.size() > 0) {
                        resertCount = stocks.get(0).getNumber();
                    }*/
                    BigDecimal restCount = getRestCount(o.getChildOrderId());
                    String resertCount = restCount.setScale(0).toString();
                    if (!hadSet && o.getCompanyId() != null) {
                        Company company = companyDao.selectByPrimaryKey(o.getCompanyId());
                        result.setCompany(company);
                        hadSet = true;
                    }
                    OutRecordPrintDto.OutRecordPrintItemDto item = new OutRecordPrintDto.OutRecordPrintItemDto(assembly, o, resertCount);

                    StorageInputAssemblyQuery exa = new StorageInputAssemblyQuery();
                    exa.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andChildInputStorageNumberEqualTo(o.getChildOrderId() + "");
                    List<StorageInputAssembly> input = storageInputService.selectByExample(exa);
                    if (input.size() == 0) {
                        logger.error("数据问题，出库对象为:{}", JSON.toJSONString(o));
                        throw new RuntimeException("存在数据问题，请联系处理");
                    }
                    item.setStorageInputAssembly(input.get(0));
                    items.add(item);
                }
            } else {
                items.add(a);
            }
        }
        Set<Integer> typeSet = new HashSet<>();
        for (StorageOutRecord o : outList) {
            typeSet.add(o.getAssemblyType());
        }
        if(typeSet.contains(3)&& typeSet.size()>1) result.setOrderName("不能同时打印成品和物料！");
        if (outList.size() > 15) result.setOrderName("打印行数不能超过15行！");

        //拓展
        while (items.size() < line) items.add(a);
        result.setItems(items);
        return result;
    }

    private BigDecimal getRestCount(Long childOrderId) {
        StorageOutRecordQuery e = new StorageOutRecordQuery();
        e.createCriteria().andChildOrderIdEqualTo(childOrderId);
        List<StorageOutRecord> outList = storageOutRecordDao.selectByExample(e);
        BigDecimal out = BigDecimal.ZERO;
        BigDecimal waste = BigDecimal.ZERO;
        for (StorageOutRecord o : outList) {
            out = out.add(new BigDecimal(o.getCount()));
            if (StringUtils.hasText(o.getWaste())) {
                waste = waste.add(new BigDecimal(o.getWaste()));
            }
        }
        StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
        ex.createCriteria().andChildInputStorageNumberEqualTo(childOrderId.toString());
        List<StorageInputAssembly> inList = storageInputService.selectByExample(ex);
        BigDecimal total = BigDecimal.ZERO;
        for (StorageInputAssembly s : inList) {
            total = total.add(new BigDecimal(s.getCount()));
        }
        return total.subtract(out).subtract(waste);
    }

    private String getChangeCount(StorageOutRecord old, StorageOutRecord now) {
        if (old == null) {
            return "-" + new BigDecimal(now.getCount()).add(new BigDecimal(now.getWaste() == null ? "0" : now.getWaste())).toString();
        }
        if (now == null) {
            return new BigDecimal(old.getCount()).add(new BigDecimal(old.getWaste() == null ? "0" : old.getWaste())).toString();
        }
        BigDecimal nowCount = new BigDecimal(now.getCount()).add(new BigDecimal(now.getWaste() == null ? "0" : now.getWaste()));
        BigDecimal oldCount = new BigDecimal(old.getCount()).add(new BigDecimal(old.getWaste() == null ? "0" : old.getWaste()));
//        -now+old
        return oldCount.subtract(nowCount).toString();
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        StorageOutRecord old = storageOutRecordDao.selectByPrimaryKey(id);
        logger.info("被删除的出库记录为{}", JSON.toJSONString(old));
        LoggerUtils.logger("删除出库记录", JSON.toJSONString(old));
        if (old.getAssemblyType() == 3) {
            orderService.outStorage(new BigDecimal("-" + old.getCount()), new BigDecimal("-" + old.getWaste()), "删除出库", old.getChildOrderId());
        } else {
            stockService.updateStock(getChangeCount(old, null), assemblyService.selectByPrimaryKey(old.getAssemblyId()));
        }

        storageOutRecordDao.deleteByPrimaryKey(id);
        return 0;
    }

    @Override
    public List<StorageOutRecord> selectByExample(StorageOutRecordQuery example) {
        return storageOutRecordDao.selectByExample(example);
    }

    @Override
    public StorageOutRecord selectByPrimaryKey(Long id) {
        return storageOutRecordDao.selectByPrimaryKey(id);
    }
}
