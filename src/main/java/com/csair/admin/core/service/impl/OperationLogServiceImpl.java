package com.csair.admin.core.service.impl;

import com.csair.admin.core.dao.OperationLogDao;
import com.csair.admin.core.po.OperationLog;
import com.csair.admin.core.po.OperationLogQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.query.OperationLogQueryObject;
import com.csair.admin.core.service.OperationLogService;
import com.csair.admin.util.HttpUtils;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * laogaochg
 * 2017/7/11.
 */
@Service
public class OperationLogServiceImpl implements OperationLogService {
    @Resource
    private OperationLogDao operationLogDao;

    @Override
    public PageResult<OperationLog> pageQuery(OperationLogQueryObject qo) {

        OperationLogQuery example = new OperationLogQuery();
        example.setPageNo(qo.getCurrentPage());
        example.setLimit(qo.getPageSize());
        OperationLogQuery.Criteria criteria = example.createCriteria();
        if (qo.getId() != null) {
            criteria.andIdEqualTo(qo.getId());
        }
        if (StringUtils.isNotBlank(qo.getAction())) {
            criteria.andActionEqualTo(qo.getAction());
        }
        if (qo.getBeginTime() != null) {
            criteria.andOpTimeGreaterThanOrEqualTo(qo.getBeginTime());
        }
        if (qo.getEndTime() != null) {
            criteria.andOpTimeLessThanOrEqualTo(qo.getEndTime());
        }
        example.setOrderByClause("op_time DESC");
        int count = operationLogDao.countByExample(example);
        List<OperationLog> companyList = operationLogDao.selectByExample(example);
        if (qo.getId() != null) {
            for (OperationLog operationLog : companyList) {
                operationLog.setContent(operationLogDao.selectByPrimaryKey(operationLog.getId()).getContent());
            }
        }
        return new PageResult<OperationLog>(companyList, count, qo.getCurrentPage(), qo.getPageSize());
    }

    @Override
    public void log(Long operaterId, String action, String content, String ip) {
        OperationLog log = new OperationLog();
        log.setAction(action);
        log.setAuthor(operaterId + "");
        log.setContent(content);
        log.setOpIp(ip);
        log.setOpTime(new Date());
        log.setAccount(UserUtil.getUser().getEmail());
        operationLogDao.insert(log);
    }

}
