package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.comtains.Constants;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dao.AssemblyPropertyDao;
import com.csair.admin.core.dao.StorageInputAssemblyDao;
import com.csair.admin.core.dao.StorageOutRecordDao;
import com.csair.admin.core.dao.WarehouseDao;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyProperty;
import com.csair.admin.core.po.AssemblyPropertyQuery;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.Warehouse;
import com.csair.admin.core.po.WarehouseQuery;
import com.csair.admin.core.po.WarehouseQueryObject;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.User;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.core.service.WarehouseService;
import com.csair.admin.core.vo.StorageOutRecordVo;
import com.csair.admin.util.LoggerUtils;
import com.csair.admin.util.ParamConstants;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 实现类
 * 
 * @author: Yin ShiHua
 */
@Service
public class WarehouseServiceImpl implements WarehouseService {
	private static Logger logger = LoggerFactory.getLogger(WarehouseServiceImpl.class);
	@Autowired
	private WarehouseDao warehouseDao;
	@Override
	public Warehouse selectWareByUserId() {
		Subject currentUser = SecurityUtils.getSubject();
		  User user = (User) currentUser.getSession().getAttribute(ParamConstants.USER_SESSION);
		Warehouse ware = warehouseDao.selectWareByUserId(Long.valueOf(user.getType()));
		return ware;
	}
	
	@Override
	public int updateWare(Warehouse qo) {
		int ret = warehouseDao.updateWare(qo);
		return ret;
	}

	@Override
	public int insertWare(Warehouse qo) {
		Subject currentUser = SecurityUtils.getSubject();
		User user = (User) currentUser.getSession().getAttribute(ParamConstants.USER_SESSION);
		qo.setWarehouseManagId(Long.valueOf(user.getType()));
		qo.setWarehouseManager(user.getNickname());
		qo.setCreateDate(new Date());
		int ret = warehouseDao.insertWare(qo);
		return ret;
	}
}
