package com.csair.admin.core.service;

import com.csair.admin.core.dto.StockQueryObject;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.Stock;
import com.csair.admin.core.po.StockQuery;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.vo.StockVo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/21 0:27
 * 材料库存
 * 校正库存sql
 * update stock s set s.number =
 * (
 * SELECT
 * (sum(a.count) - (SELECT sum(b.count)
 * FROM storage_out_record b
 * WHERE a.assembly_id = b.assembly_id)) a
 * FROM storage_input_assembly a
 * where a.assembly_id = s.assembly_id group by a.assembly_id
 * ) where s.assembly_id = 252
 */
public interface StockService {

    List<Stock> selectByExample(StockQuery example);

    PageResult<StockVo> pageQuery(StockQueryObject qo);

    void updateStock(String changeCount, Assembly assembly);

    XSSFWorkbook exportFile(StockQueryObject qo);
}
