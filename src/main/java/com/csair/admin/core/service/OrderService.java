package com.csair.admin.core.service;

import com.csair.admin.core.dao.OrderPoDao;
import com.csair.admin.core.dto.OrderVo;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.OrderPo;
import com.csair.admin.core.po.OrderPoQuery;
import com.csair.admin.core.po.OrderQueryObject;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.vo.StorageOutRecordVo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/10 22:21
 */
public interface OrderService {

    PageResult<OrderVo> pageQuery(OrderQueryObject qo);


    void insertInputStorage(StorageInputAssembly inputStorage, Assembly assembly, int type);

    void updateInputStorage(StorageInputAssembly inputStorage, Assembly assembly);

    void deleteInputStorage(StorageInputAssembly delete, Integer type);

    /**
     * 成品添加出库
     */
    void insertOutStorage(StorageOutRecord s);
    /**
     * 已出库数量 修改时间 剩余数量
     * 添加 添加数量
     * 删除 回加数量
     * 修改 变化数量
     *
     * @param changeCount  变化的数量
     * @param remark       备注
     * @param childOrderId 子订单id
     * @param changeWaste  变化的损耗
     */
    void outStorage(BigDecimal changeCount, BigDecimal changeWaste, String remark, Long childOrderId);

    /**
     * 设置了组件的价格
     */
    void setAssemblyPrice(Assembly assembly );
    List<OrderPo> selectByExample(OrderPoQuery example);


    /**
     * 生成对账单
     */
    HSSFWorkbook exportAccountStatement(OrderQueryObject qo);
}
