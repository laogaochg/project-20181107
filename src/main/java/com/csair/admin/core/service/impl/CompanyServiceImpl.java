package com.csair.admin.core.service.impl;

import com.csair.admin.core.dao.CompanyDao;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.CompanyQuery;
import com.csair.admin.core.po.CompanyQueryObject;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.util.LoggerUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:34
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyDao companyDao;

    @Override
    public PageResult<Company> pageQuery(CompanyQueryObject qo) {
        CompanyQuery example = new CompanyQuery();
        example.setPageNo(qo.getCurrentPage());
        example.setPageSize(qo.getPageSize());
        CompanyQuery.Criteria criteria = example.createCriteria();
        if (qo.getId() != null) {
            criteria.andIdEqualTo(qo.getId());
        }
        if(StringUtils.isNotBlank(qo.getName())){
            criteria.andNameLike("%"+qo.getName()+"%");
        }
        if(StringUtils.isNotBlank(qo.getContacts())){
            criteria.andContactsEqualTo(qo.getContacts());
        }
        example.setOrderByClause("create_date DESC");
        int count = companyDao.countByExample(example);
        List<Company> companyList = companyDao.selectByExample(example);
        return new PageResult(companyList, count, qo.getCurrentPage(), qo.getPageSize());
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return companyDao.deleteByPrimaryKey(id);
    }

    @Override
    public Company selectByPrimaryKey(Long id) {
        return companyDao.selectByPrimaryKey(id);
    }

    @Override
    public int insert(Company record) {
        record.setCreateDate(new Date());
        LoggerUtils.logger("建立公司", "公司名:" + record.getName());
        return companyDao.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(Company record) {
        return companyDao.updateByPrimaryKey(record);
    }
}
