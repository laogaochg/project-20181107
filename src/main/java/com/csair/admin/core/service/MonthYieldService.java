package com.csair.admin.core.service;

import com.csair.admin.core.dto.StockQueryObject;
import com.csair.admin.core.vo.MonthYieldVo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2019/1/6 14:25
 */
public interface MonthYieldService {

    List<MonthYieldVo> pageQuery(MonthYieldVo vo);

    void update(MonthYieldVo map);

    HSSFWorkbook exportMonthYield(String month);
}
