package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.OrderChangeRecordDao;
import com.csair.admin.core.dao.OrderPoDao;
import com.csair.admin.core.dto.OrderVo;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.Company;
import com.csair.admin.core.po.OrderChangeRecord;
import com.csair.admin.core.po.OrderPo;
import com.csair.admin.core.po.OrderPoQuery;
import com.csair.admin.core.po.OrderQueryObject;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.CompanyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.core.service.StorageInputService;
import com.csair.admin.core.service.StorageOutRecordService;
import com.csair.admin.core.service.WarehouseService;
import com.csair.admin.util.DateUtil;
import com.csair.admin.util.ExcelUtil;
import com.csair.admin.util.UserUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/12/10 22:27
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderPoDao orderPoDao;
    @Autowired
    private OrderChangeRecordDao orderChangeRecordDao;
    @Autowired
    private AssemblyService assemblyService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private WarehouseService warehouseService;
    @Autowired
    private StorageInputService storageInputService;
    @Autowired
    private StorageOutRecordService storageOutRecordService;
    private static Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    public PageResult<OrderVo> pageQuery(OrderQueryObject qo) {
        OrderPoQuery e = new OrderPoQuery();
        e.setPageNo(qo.getCurrentPage());
        e.setPageSize(qo.getPageSize());
        OrderPoQuery.Criteria c = e.createCriteria();
        if (qo.getOrderId() != null && qo.getOrderId() > 0) {
            c.andOrderIdEqualTo(qo.getOrderId());
        }
        if (qo.getCompanyId() != null && qo.getCompanyId() > 0) {
            c.andCompanyIdEqualTo(qo.getCompanyId());
        }
        if (qo.getAssemblyId() != null) {
            c.andAssemblyIdEqualTo(qo.getAssemblyId());
        }
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (StringUtils.hasText(qo.getBeginDate())) {
                c.andCreateTimeGreaterThanOrEqualTo(DateUtil.getBeginTime(f.parse(qo.getBeginDate())));
            }
            if (StringUtils.hasText(qo.getEndDate())) {
                c.andCreateTimeLessThanOrEqualTo(DateUtil.getTodayEndTime(f.parse(qo.getEndDate())));
            }
        } catch (ParseException ee) {
            ee.printStackTrace();
            logger.error("时间转换异常:{}", ee);
        }

        if (StringUtils.hasText(qo.getCustomOrderId())) {
            StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
            ex.createCriteria().andCustomOrderIdEqualTo(qo.getCustomOrderId());
            List<Long> ids = new ArrayList<>();
            List<StorageInputAssembly> list = storageInputService.selectByExample(ex);
            for (StorageInputAssembly s : list) {
                ids.add(Long.valueOf(s.getChildInputStorageNumber()));
            }
            if (ids.size() == 0) {
                ids.add(-1L);//没找到就让它找不到
            }
            c.andIdIn(ids);
        }
        if (StringUtils.hasText(qo.getAssemblyName())) {
            List<Long> ids = new ArrayList<>();
            List<Assembly> assemblyList = assemblyService.queryByName(qo.getAssemblyName());
            for (Assembly a : assemblyList) {
                ids.add(a.getId());
            }
            if (ids.size() == 0) {
                ids.add(-1L);//没找到就让它找不到
            }
            c.andAssemblyIdIn(ids);
        }

        e.setOrderByClause(" create_time DESC ");
        List<OrderPo> list = orderPoDao.selectByExample(e);
        int i = orderPoDao.countByExample(e);
        List<OrderVo> vos = new ArrayList<>();
        for (OrderPo po : list) {
            OrderVo vo = new OrderVo(po);
            StorageOutRecordQuery outE = new StorageOutRecordQuery();
            outE.createCriteria()
                    .andOrderIdEqualTo(po.getOrderId())
                    .andChildOrderIdEqualTo(po.getId())
            ;
            List<StorageOutRecord> outList = storageOutRecordService.selectByExample(outE);
            BigDecimal totalOut = new BigDecimal("0");
            for (StorageOutRecord out : outList) {
                totalOut = totalOut.add(new BigDecimal(out.getTotalMoney()));
            }
            vo.setMoney(totalOut.toString());
            vo.setOutList(outList);
            Company company = companyService.selectByPrimaryKey(po.getCompanyId());
            if (company != null) vo.setCompanyName(company.getName());
            Assembly assembly = assemblyService.selectByPrimaryKey(po.getAssemblyId());
            if (assembly != null) {
                vo.setAssemblyCode(assembly.getCode());
                vo.setAssemblyName(assembly.getName());
                vo.setAssembly(assembly);
            }
            vos.add(vo);

        }
        return new PageResult<>(vos, i, qo.getCurrentPage(), qo.getPageSize());
    }

    @Override
    public HSSFWorkbook exportAccountStatement(OrderQueryObject qo) {
        Date endDate;
        Date beginDate;
        try {
            endDate = new SimpleDateFormat("yyyy-MM-dd").parse(qo.getEndDate());
            beginDate = new SimpleDateFormat("yyyy-MM-dd").parse(qo.getBeginDate());
        } catch (ParseException e) {
            e.printStackTrace();
            throw new PlatformException(0, "时间参数不对");
        }
        String companyName = warehouseService.selectWareByUserId().getWarehouseName();
        String title_0 = String.format(companyName + "%s对账单", new SimpleDateFormat("yyyy年MM月").format(endDate));
        Company company = companyService.selectByPrimaryKey(qo.getCompanyId());
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
        String title_1 = String.format("对账单位：%s            对帐所属时间：%s至%s"
                , company.getName(), format.format(beginDate), format.format(endDate));
        StorageOutRecordQuery e = new StorageOutRecordQuery();
        StorageOutRecordQuery.Criteria criteria = e.createCriteria();
        criteria.andCompanyIdEqualTo(qo.getCompanyId())
                .andAssemblyTypeEqualTo(3)//只对成品
                .andOutStorageTimeLessThanOrEqualTo(DateUtil.getTodayEndTime(endDate))
                .andOutStorageTimeGreaterThanOrEqualTo(DateUtil.getBeginTime(beginDate));
        if (StringUtils.hasText(qo.getOutType())) criteria.andOutTypeEqualTo(Integer.valueOf(qo.getOutType()));
        List<StorageOutRecord> outList = storageOutRecordService.selectByExample(e);
        List<StorageInputAssembly> inputList = new ArrayList<>();
        for (StorageOutRecord s : outList) {
            StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
            ex.createCriteria().andStorageIdEqualTo(UserUtil.getUserStorageId()).andChildInputStorageNumberEqualTo(s.getChildOrderId() + "");
            List<StorageInputAssembly> childOrder = storageInputService.selectByExample(ex);
            if (childOrder.size() == 0) {
                inputList.add(null);
                continue;
            }
            inputList.add(childOrder.get(0));
            //覆盖字段
            s.setOutPrice(childOrder.get(0).getPrice());
        }

        HSSFWorkbook wb = getHSSFSheet(title_0, title_1, outList, inputList);
        return wb;
    }

    private HSSFWorkbook getHSSFSheet(String title_0, String title_1, List<StorageOutRecord> outList, List<StorageInputAssembly> inputList) {
        HSSFWorkbook wb = new HSSFWorkbook();
        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet("sheet1");
        //设置列宽
        int column = 0;
        sheet.setColumnWidth(column++, 4000);
        sheet.setColumnWidth(column++, 5000);
        sheet.setColumnWidth(column++, 5000);
        sheet.setColumnWidth(column++, 4000);
        sheet.setColumnWidth(column++, 7000);
        sheet.setColumnWidth(column++, 7000);
        sheet.setColumnWidth(column++, 7000);
        sheet.setColumnWidth(column++, 2000);
        sheet.setColumnWidth(column++, 5000);
        sheet.setColumnWidth(column++, 5000);
        sheet.setColumnWidth(column++, 5000);
        sheet.setColumnWidth(column++, 5000);
        int totalLength = 12;
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, totalLength));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, totalLength)); //
        int rowCount = 0;
        {
            HSSFRow row = sheet.createRow(rowCount++);
            HSSFCell cell = row.createCell(0);
            row.setHeightInPoints((short) 30);
            cell.setCellValue(title_0);
            cell.setCellStyle(ExcelUtil.getFirstRowStyle(wb));
        }
        {
            HSSFRow row = sheet.createRow(rowCount++);
            HSSFCell cell = row.createCell(0);
            row.setHeightInPoints((short) 25);
            cell.setCellValue(title_1);
            cell.setCellStyle(ExcelUtil.getFirstRowStyle(wb));
        }
        HSSFCellStyle baseRowStyle = ExcelUtil.getBaseRowStyle(wb);
        {
            HSSFRow row = sheet.createRow(rowCount++);
            String[] split = ("日期,入库单,送货单" +
//                    ",编码" +
                    ",入库类型" +
                    ",客户单号" +
                    ",名称,规格型号,单位,数量,点数,单价,金额,备注").split(",");
            for (int i = 0; i < split.length; i++) {
                HSSFCell cell = row.createCell(i);
                row.setHeightInPoints((short) 20);
                cell.setCellValue(split[i]);
                cell.setCellStyle(baseRowStyle);
            }
        }
        double totalMoney = 0D;
        {
            for (int i = 0; i < outList.size(); i++) {
                HSSFRow row = sheet.createRow(rowCount++);
                row.setHeightInPoints((short) 20);
                StorageOutRecord o = outList.get(i);
                Assembly assembly = assemblyService.selectByPrimaryKey(o.getAssemblyId());
                int count = 0;
                String format = new SimpleDateFormat("yyyy-MM-dd").format(o.getOutStorageTime());
                //1:正常入库,2:外协入库
                String inputType = inputList.get(i) != null && inputList.get(i).getType() == 1 ? "正常入库" : "外协入库";

                setCellValue(row, count++, baseRowStyle, format);//日期
                setCellValue(row, count++, baseRowStyle, o.getOrderId() + "");//入库单
                setCellValue(row, count++, baseRowStyle, o.getOutNumber());//送货单
                setCellValue(row, count++, baseRowStyle, inputType);//入库类型
                setCellValue(row, count++, baseRowStyle, o.getCustomOrderId() == null ? "" : o.getCustomOrderId());//客户单号
                setCellValue(row, count++, baseRowStyle, assembly.getName());//名称
                setCellValue(row, count++, baseRowStyle, assembly.getModelName());//规格型号
                setCellValue(row, count++, baseRowStyle, assembly.getUnit());//单位
                setCellValue(row, count++, baseRowStyle, Double.valueOf(o.getCount()));//数量
                setCellValue(row, count++, baseRowStyle, Double.valueOf(o.getOutPoint()));//数量
                setCellValue(row, count++, baseRowStyle, new BigDecimal(o.getOutPrice() == null ? "0" : o.getOutPrice()).setScale(6, BigDecimal.ROUND_UP).doubleValue());//单价
                double money = new BigDecimal(o.getTotalMoney() == null ? "0" : o.getTotalMoney()).setScale(3, BigDecimal.ROUND_UP).doubleValue();
                totalMoney += money;
                setCellValue(row, count++, baseRowStyle, money);//金额
                setCellValue(row, count++, baseRowStyle, o.getRemark() == null ? "" : o.getRemark());//备注

            }
        }
        {
            HSSFRow row = sheet.createRow(rowCount++);
            setCellValue(row, 10, baseRowStyle, "金额汇总：");
            setCellValue(row, 11, baseRowStyle, totalMoney);
        }
        return wb;
    }

    private HSSFCell setCellValue(HSSFRow row, int count, HSSFCellStyle baseRowStyle, Object o) {
        HSSFCell cellEntry = row.createCell(count);
        if (o instanceof String) {
            cellEntry.setCellValue((String) o);
        } else {
            try {
                cellEntry.setCellValue(Double.valueOf(o + ""));
            } catch (Exception e) {
                cellEntry.setCellValue(o + "");
            }
        }
        cellEntry.setCellStyle(baseRowStyle);
        return cellEntry;
    }


    /**
     * 已出库数量 修改时间 剩余数量
     * 添加 添加数量
     * 删除 回加数量
     * 修改 变化数量
     * 不再计算金额
     *
     * @param changeCount  变化的数量
     * @param remark       备注
     * @param childOrderId 子订单id
     */
    @Override
    public void outStorage(BigDecimal changeCount, BigDecimal changeWaste, String remark, Long childOrderId) {
        OrderPo old = orderPoDao.selectByPrimaryKey(childOrderId);
        OrderPo now = orderPoDao.selectByPrimaryKey(childOrderId);
        if (old == null) {
            String afterCount = BigDecimal.ZERO.add(changeCount).setScale(3, BigDecimal.ROUND_UP).toString();
            String afterWaste = BigDecimal.ZERO.add(changeWaste).setScale(3, BigDecimal.ROUND_UP).toString();
            now.setOutCount(afterCount);
            now.setWasteCount(afterWaste);
        } else {
            String afterCount = new BigDecimal(old.getOutCount()).add(changeCount).setScale(3, BigDecimal.ROUND_UP).toString();
            String afterWaste = new BigDecimal(old.getWasteCount()).add(changeWaste).setScale(3, BigDecimal.ROUND_UP).toString();
            now.setOutCount(afterCount);
            now.setWasteCount(afterWaste);
        }
        now.setRemark(remark);
        now.setUpdateTime(new Date());
        BigDecimal resetCount = new BigDecimal(now.getInputCount())
                .subtract(new BigDecimal(now.getOutCount()))
                .subtract(new BigDecimal(now.getWasteCount()));
        if (resetCount.compareTo(BigDecimal.ZERO) < 0) {
            throw new PlatformException(0, "出库数量已超过库存数量");
        }
        now.setResertCount(resetCount.toString());
        orderPoDao.updateByPrimaryKeySelective(now);
        insertOrderChangeRecord(now, old, remark);
    }

    /**
     * 入库产生订单
     */
    @Override
    public void insertInputStorage(StorageInputAssembly inputStorage, Assembly assembly, int type) {
        if (assembly.getType() != 3) return;
        OrderPo a = getOrderPoByInputStorage(inputStorage);
        calculateMoney(a, inputStorage, assembly);//计算订单价格
        if (type == 1) {
            a.setRemark("添加入库");
        } else {
            a.setRemark("修改入库");
        }
        orderPoDao.insert(a);
        insertOrderChangeRecord(a, null, type == 1 ? "添加入库" : "修改入库");
    }

    /**
     * 修改入库记录
     */
    @Override
    public void updateInputStorage(StorageInputAssembly inputStorage, Assembly assembly) {
        if (assembly.getType() != 3) return;
        Long orderId = Long.valueOf(inputStorage.getChildInputStorageNumber());
        OrderPo old = orderPoDao.selectByPrimaryKey(orderId);
        OrderPo a = getOrderPoByInputStorage(inputStorage);
        calculateMoney(a, inputStorage, assembly);//计算订单价格
        BigDecimal out = new BigDecimal(old.getOutCount());
        if (old.getOutCount() != null
                && BigDecimal.ZERO.compareTo(out) != 0) {//已经有出库
            throw new PlatformException(0, "已经有出库，不能修改或删除");
        }
        String remark = "修改入库";
        a.setRemark(remark);
        orderPoDao.updateByPrimaryKeySelective(a);
        insertOrderChangeRecord(a, old, "修改入库");
    }

    private void calculateMoney(OrderPo a, StorageInputAssembly inputStorage, Assembly assembly) {
        if (assembly != null && StringUtils.hasText(assembly.getOutPointPrice())) {
            String totalMoney = new BigDecimal(inputStorage.getCount())
                    .multiply(new BigDecimal(assembly.getOutPointPrice()))
                    .setScale(3, BigDecimal.ROUND_UP).toString();
            a.setTotalMoney(totalMoney);//计算总金额
        } else if (assembly != null) {
            a.setType(1);
            a.setRemark("没有出库单价，没有计算金额");
        }
    }

    //得到订单对象(不计算金额)
    private OrderPo getOrderPoByInputStorage(StorageInputAssembly inputStorage) {
        OrderPo a = new OrderPo();
        Date date = new Date();
        a.setCompanyId(inputStorage.getCompanyId());
        a.setId(Long.valueOf(inputStorage.getChildInputStorageNumber()));
        a.setType(0);
        a.setAssemblyId(inputStorage.getAssemblyId());
        a.setInputCount(inputStorage.getCount());
        a.setResertCount(inputStorage.getCount());// 剩余数量
        a.setOutCount("0");
        a.setPaidCount("0");
        a.setTotalMoney("0");//金额
        a.setStorageId(UserUtil.getUserStorageId());
        a.setUpdateTime(date);
        a.setWasteCount("0");
        a.setCreateTime(inputStorage.getInputStorageTime());
        a.setOrderId(Long.valueOf(inputStorage.getInputStorageNumber()));
        return a;
    }

    @Override
    public void deleteInputStorage(StorageInputAssembly delete, Integer type) {
        if (delete.getAssemblyType() != 3) return;
        Long orderId = Long.valueOf(delete.getChildInputStorageNumber());
        OrderPo a = getOrderPoByInputStorage(delete);
        OrderPo old = orderPoDao.selectByPrimaryKey(orderId);
        if (old == null) return;
        BigDecimal out = new BigDecimal(old.getOutCount());
        if (old.getOutCount() != null
                && BigDecimal.ZERO.compareTo(out) != 0) {//已经有出库
            throw new PlatformException(0, "已经有出库不能删除");
        }//数量归0
        a.setInputCount("0");
        a.setResertCount("0");// 剩余数量
        String remark;
        if (type == 1) {
            remark = "删除入库";
        } else {
            remark = "修改入库";
        }
        orderPoDao.updateByPrimaryKeySelective(a);
        insertOrderChangeRecord(a, old, remark);
    }

    private void insertOrderChangeRecord(OrderPo now, OrderPo old, String remark) {
        OrderChangeRecord a = new OrderChangeRecord();
        a.setCreateTime(new Date());
        a.setUserId(UserUtil.getUserId());
        a.setOrderId(now.getId());
        a.setStorageId(UserUtil.getUserStorageId());
        if (old == null) {
            a.setInputCountBefore("0");
            a.setOutCountBefore("0");
            a.setPaidCountBefore("0");
            a.setResertCountBefore("0");
            a.setTotalMoneyBefore("0");
        } else {
            a.setInputCountBefore(old.getInputCount());
            a.setOutCountBefore(old.getOutCount());
            a.setPaidCountBefore(old.getPaidCount());
            a.setResertCountBefore(old.getResertCount());
            a.setTotalMoneyBefore(old.getTotalMoney());
        }


        a.setInputCountAfter(now.getInputCount());
        a.setOutCountAfter(now.getOutCount());
        a.setPaidCountAfter(now.getPaidCount());
        a.setResertCountAfter(now.getResertCount());
        a.setTotalMoneyAfter(now.getTotalMoney());

        a.setRemark(remark);
        a.setType(0);
        orderChangeRecordDao.insert(a);
    }

    @Override
    public void setAssemblyPrice(Assembly assembly) {
        /*logger.info("设置了价格:{}", JSON.toJSONString(assembly));
        if (assembly.getOutPointPrice() != null && assembly.getId() != null) {
            OrderPoQuery e = new OrderPoQuery();
            OrderPoQuery.Criteria c = e.createCriteria();
            c.andAssemblyIdEqualTo(assembly.getId());
            List<OrderPo> oldOrderList = orderPoDao.selectByExample(e);
            for (OrderPo old : oldOrderList) {
                old.setUpdateTime(new Date());
                old.setTotalMoney();
                old.setShouldGetCount();
            }

        }*/
    }

    @Override
    public void insertOutStorage(StorageOutRecord s) {
    }


    @Override
    public List<OrderPo> selectByExample(OrderPoQuery example) {
        return orderPoDao.selectByExample(example);
    }


}
