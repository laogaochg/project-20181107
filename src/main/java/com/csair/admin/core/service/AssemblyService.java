package com.csair.admin.core.service;

import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyProperty;
import com.csair.admin.core.po.AssemblyPropertyQuery;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.core.PageResult;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:33
 */
public interface AssemblyService {
    PageResult pageQuery(AssemblyQueryObject qo);

    int deleteByPrimaryKey(Long id);

    int insert(HashMap<String,String> record);

    int updateByPrimaryKey(HashMap<String,String> record);

    List<Assembly> selectByExample(AssemblyQuery example);

    List<Assembly> queryAllName(Integer type);
    List<Assembly> queryByName(String name);

    Assembly selectByPrimaryKey(Long id);

    /**
     *
     * @param code 组件编码
     * @return
     */
    Assembly selectByCode(String code);

}
