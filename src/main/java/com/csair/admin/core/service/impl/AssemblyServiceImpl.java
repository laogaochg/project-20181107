package com.csair.admin.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.csair.admin.comtains.Constants;
import com.csair.admin.config.PlatformException;
import com.csair.admin.core.dao.AssemblyDao;
import com.csair.admin.core.dao.AssemblyPropertyDao;
import com.csair.admin.core.dao.StorageInputAssemblyDao;
import com.csair.admin.core.dao.StorageOutRecordDao;
import com.csair.admin.core.po.Assembly;
import com.csair.admin.core.po.AssemblyProperty;
import com.csair.admin.core.po.AssemblyPropertyQuery;
import com.csair.admin.core.po.AssemblyQuery;
import com.csair.admin.core.po.AssemblyQueryObject;
import com.csair.admin.core.po.AssemblyVo;
import com.csair.admin.core.po.StorageInputAssembly;
import com.csair.admin.core.po.StorageInputAssemblyQuery;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.service.AssemblyService;
import com.csair.admin.core.service.OrderService;
import com.csair.admin.core.vo.StorageOutRecordVo;
import com.csair.admin.util.LoggerUtils;
import com.csair.admin.util.StringUtil;
import com.csair.admin.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/9 22:34
 */
@Service
public class AssemblyServiceImpl implements AssemblyService {
    @Autowired
    private AssemblyDao assemblyDao;
    @Autowired
    private StorageInputAssemblyDao storageInputAssemblyDao;
    @Autowired
    private StorageOutRecordDao storageOutRecordDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private AssemblyPropertyDao assemblyPropertyDao;

    private static Logger logger = LoggerFactory.getLogger(AssemblyServiceImpl.class);

    @Override
    public PageResult pageQuery(AssemblyQueryObject qo) {
        AssemblyQuery example = new AssemblyQuery();
        example.setPageNo(qo.getCurrentPage());
        example.setPageSize(qo.getPageSize());
        AssemblyQuery.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(1);//有效
        criteria.andStorageIdEqualTo(UserUtil.getUserStorageId());
        if (qo.getId() != null) {
            criteria.andIdEqualTo(qo.getId());
        }
        if (qo.getType() != null && qo.getType() >= 0) {
            criteria.andTypeEqualTo(qo.getType());
        }
        if (StringUtils.isNotBlank(qo.getName())) {
            criteria.andNameLike("%" + qo.getName() + "%");
        }
        example.setOrderByClause("create_time DESC");
        int count = assemblyDao.countByExample(example);
        List<Assembly> assemblyList = assemblyDao.selectByExample(example);
        List<AssemblyVo> result = new ArrayList<>();
        for (Assembly a : assemblyList) {
            AssemblyVo vo = new AssemblyVo(a);
            AssemblyPropertyQuery sqo = new AssemblyPropertyQuery();
            sqo.createCriteria().andBlockIdEqualTo(a.getId());
            List<AssemblyProperty> list = assemblyPropertyDao.selectByExample(sqo);
            List<AssemblyVo> propertiesList = new ArrayList<>();
            for (AssemblyProperty p : list) {
                Assembly proper = assemblyDao.selectByPrimaryKey(p.getPropertyId());
                AssemblyVo assemblyVo = new AssemblyVo(proper);
                assemblyVo.setCounts(p.getCount() + "");
                if (proper != null) propertiesList.add(assemblyVo);
            }
            vo.setProperties(propertiesList);
            result.add(vo);
        }
        logger.debug(JSON.toJSONString(result));
        return new PageResult(result, count, qo.getCurrentPage(), qo.getPageSize());
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        LoggerUtils.logger("删除组件", "组件名ID:" + id);
        Assembly a = new Assembly();
        a.setId(id);
        a.setStatus(2);
        return assemblyDao.updateByPrimaryKeySelective(a);
    }

    @Override
    public int insert(HashMap<String, String> map) {
//        "name": "12",
//                "type": "3",
//                "code": "",
//                "modelName": "",
//                "unit": "PCS",
//                "companyId": "0",
//                "remark": "",
        Assembly a = new Assembly();
        a.setName(map.get("name"));
        a.setType(Integer.valueOf(map.get("type")));
        a.setCode(map.get("code"));
        a.setModelName(map.get("modelName"));
        a.setUnit(StringUtils.isNotBlank(map.get("unit")) ? map.get("unit") : "PCS");
        a.setRemark(map.get("remark"));
        a.setCreateTime(new Date());
        a.setStorageId(UserUtil.getUserStorageId());
        a.setStatus(1);
        if (StringUtils.isNumeric(map.get("companyId"))) {
            a.setCompanyId(Long.valueOf(map.get("companyId")));
        }
        checkAssembly(a);
        int result = assemblyDao.insert(a);
        LoggerUtils.logger("建立组件", "组件名:" + a.getName());
        Long assemblyId = a.getId();
        logger.info("建立组件", JSON.toJSONString(a));
        updateProperties(map, assemblyId);
        return result;
    }

    private void checkAssembly(Assembly a) {
        AssemblyQuery e = new AssemblyQuery();
        if (a.getType() == 3) {
            e.createCriteria().andTypeEqualTo(3).andNameEqualTo(a.getName()).andModelNameEqualTo(a.getModelName()).andCompanyIdEqualTo(a.getCompanyId());
            List<Assembly> list = assemblyDao.selectByExample(e);
            if (list.size() != 0) {
                throw new PlatformException(0, "名字" + a.getName() + "规格" + a.getModelName() + "的主件已经存在，请确认输入正确。");
            }
        } else {
            e.createCriteria().andTypeNotEqualTo(3).andNameEqualTo(a.getName()).andModelNameEqualTo(a.getModelName());
            List<Assembly> list = assemblyDao.selectByExample(e);
            if (list.size() != 0) {
                throw new PlatformException(0, "名字" + a.getName() + "规格" + a.getModelName() + "的SMT料已经存在，请确认输入正确。");
            }
            e.clear();
            e.createCriteria().andCodeEqualTo(a.getCode());
            list = assemblyDao.selectByExample(e);
            if (list.size() != 0) throw new PlatformException(0, "编码" + a.getCode() + "的SMT料已经存在，请确认输入正确。");
        }
    }

    private void updateProperties(HashMap<String, String> map, Long assemblyId) {
        for (int i = 0; i < 16; i++) {
            String name = map.get("assemblyName_" + i);
            String modelName = map.get("assemblyModelName_" + i);
            String count = map.get("count_" + i);
            if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(modelName) && StringUtils.isNotBlank(count)) {
                AssemblyQuery e = new AssemblyQuery();
                e.createCriteria().andTypeNotEqualTo(3).andNameEqualTo(name).andModelNameEqualTo(modelName);
                List<Assembly> list = assemblyDao.selectByExample(e);
                if (list.size() == 0)
                    throw new PlatformException(0, "名字：" + name + "，规格：" + modelName + "没有找到对应的物料，请确认输入正确。");
                if (StringUtils.isNumeric(count)) {
                    AssemblyProperty pro = new AssemblyProperty();
                    pro.setBlockId(assemblyId);
                    pro.setPropertyId(list.get(0).getId());
                    pro.setCount(Long.valueOf(count));
                    assemblyPropertyDao.insert(pro);
                }
            }
        }
    }

    @Override
    public int updateByPrimaryKey(HashMap<String, String> map) {
        Long id = Long.valueOf(map.get("id"));
        Assembly old = assemblyDao.selectByPrimaryKey(id);
        Assembly a = new Assembly();
        a.setId(id);
        a.setName(map.get("name"));
        a.setType(Integer.valueOf(map.get("type")));
        a.setCode(map.get("code"));
        a.setModelName(map.get("modelName"));
        a.setUnit(StringUtils.isNotBlank(map.get("unit")) ? map.get("unit") : "PCS");
        a.setRemark(map.get("remark"));
        a.setCreateTime(a.getCreateTime());
        if (StringUtils.isNumeric(map.get("companyId"))) {
            a.setCompanyId(Long.valueOf(map.get("companyId")));
        }
        logger.info("修改组件，之前的值{}，新的值", JSON.toJSONString(old), JSON.toJSONString(a));
        //删除关连关系
        AssemblyPropertyQuery example = new AssemblyPropertyQuery();
        example.createCriteria().andBlockIdEqualTo(id);
        LoggerUtils.logger("修改组件", JSON.toJSONString(a));
        assemblyPropertyDao.deleteByExample(example);
        updateProperties(map, id);
        //同步更新冗余字段
        updateFieldOtherTable(a);
        checkAssembly(a);
        return assemblyDao.updateByPrimaryKeySelective(a);
    }

    private void updateFieldOtherTable(Assembly assembly) {
        Assembly old = assemblyDao.selectByPrimaryKey(assembly.getId());
        Integer newType = assembly.getType();
        Integer oldType = old.getType();
        if (oldType != null && !oldType.equals(newType)) {
            StorageInputAssemblyQuery ex = new StorageInputAssemblyQuery();
            ex.createCriteria().andAssemblyIdEqualTo(assembly.getId());
            StorageInputAssembly input = new StorageInputAssembly();
            input.setAssemblyType(newType);
            List<StorageInputAssembly> list = storageInputAssemblyDao.selectByExample(ex);
            if (list.size() > 0) throw new PlatformException(0, "已经有入库的组件不能修改其类型。");
//            storageInputAssemblyDao.updateByExampleSelective(input, ex);

//            StorageOutRecordQuery exa = new StorageOutRecordQuery();
//            exa.createCriteria().andAssemblyIdEqualTo(assembly.getId());
//            StorageOutRecord out = new StorageOutRecordVo();
//            out.setAssemblyType(newType);
//            storageOutRecordDao.updateByExampleSelective(out, exa);
        }
    }


    @Override
    public List<Assembly> selectByExample(AssemblyQuery example) {
        return assemblyDao.selectByExample(example);
    }


    @Override
    public List<Assembly> queryByName(String name) {
        AssemblyQuery e = new AssemblyQuery();
        e.createCriteria().andNameEqualTo(name);
        return assemblyDao.selectByExample(e);
    }

    @Override
    public List<Assembly> queryAllName(Integer type) {
        AssemblyQuery e = new AssemblyQuery();
        AssemblyQuery.Criteria c = e.createCriteria();
        c.andStatusEqualTo(1);
        List<Assembly> assemblies = assemblyDao.selectByExample(e);
        Set<String> set = new HashSet<>();
        List<Assembly> resultList = new ArrayList<>();
        //根据名字分类
        for (Assembly assembly : assemblies) {
            if (set.add(assembly.getName())) {
                resultList.add(assembly);
            }
        }
        return resultList;
    }

    @Override
    public Assembly selectByPrimaryKey(Long id) {
        return assemblyDao.selectByPrimaryKey(id);
    }

    @Override
    public Assembly selectByCode(String code) {
        AssemblyQuery e = new AssemblyQuery();
        e.createCriteria().andCodeEqualTo(code).andStorageIdEqualTo(UserUtil.getUserStorageId()).andTypeNotEqualTo(3);
        List<Assembly> list = assemblyDao.selectByExample(e);
        if (list.size() > 1) {
            throw new PlatformException(0, "编码：" + code + "，发现多个物料。");
        }
        return list.size() == 0 ? null : list.get(0);
    }
}
