package com.csair.admin.core.service;

import com.csair.admin.core.dto.OutRecordPrintDto;
import com.csair.admin.core.dto.StorageInputQueryObject;
import com.csair.admin.core.dto.StorageOutQueryObject;
import com.csair.admin.core.po.StorageOutRecord;
import com.csair.admin.core.po.StorageOutRecordQuery;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.vo.StorageInputVo;
import com.csair.admin.core.vo.StorageOutRecordVo;

import java.util.List;
import java.util.Map;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/29 22:24
 */
public interface StorageOutRecordService {

    PageResult<StorageOutRecordVo> pageQuery(StorageOutQueryObject qo);

    List<StorageOutRecord> selectByExample(StorageOutRecordQuery example);

    StorageOutRecord selectByPrimaryKey(Long id);


    int insert(Map<String, String> vo);
    int update(Map<String, String> vo);

    int deleteByPrimaryKey(Long id);
    OutRecordPrintDto printOutRecord(String outNumber);
}
