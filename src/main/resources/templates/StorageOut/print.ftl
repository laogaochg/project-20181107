<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>${(warehouse.warehouseName)!""}送货单</title>
    <style type="text/css">
        .title {
            /*margin-left: 30%;*/
            text-align: center;
            font: 微软雅黑;
        }

        .outOrderId {
            margin-left: 80%;
            font-size: 20px;
        }

        .titleContent {
            float: left;
            width: 40%;
            margin-left: 10%;
        }

        .rightMachiningEnd {
            float: left;
            margin-left: 10%;
            width: 25%;
        }

        .time {
            float: left;
            margin-left: -4%;
        }

        .contentTable {
            clear: left;
        }

        .contentTable {
            width: 100%;
        }

        td {
            text-align: center;
            border: 1px solid #000;
            height: 20px;
        }

        .consignee {
            float: left;
            width: 40%;
            margin-left: 10%;
        }

        .deliverer {
            float: left;
            width: 40%;
            margin-left: 10%;
        }
        .printButtonClass{
            margin-left: 15%;
            height: 40px;
            margin-top: 30px;
            width: 150px;
        }

    </style>
</head>

<body>
<div class="page-content">
    <div class="title"><h1>${(entity.orderName)!""}</h1></div>
<#--<div class="outOrderId"><h5>NO:${(entity.orderId)!""}</h5></div>-->
    <div style="float: left;width: 100%">
    <#--result.setCompanyAddress(company.getAddress());
    result.setCompanyName(company.getName());
    result.setCompanyPhone(company.getPhone()); -->
        <div class="titleContent">客户:${(entity.company.name)!""}</div>
        <div class="titleContent">加工方：${(entity.processingPartyName)!""}</div>
        <div class="titleContent">地址：${(entity.company.address)!""}</div>
        <div class="titleContent">地址：${(entity.processingPartyAddress)!""}</div>
        <div class="titleContent">联系电话：${(entity.company.phone)!""}</div>
        <div class="rightMachiningEnd">联系电话：${(entity.processingPartyPhone)!""}</div>
        <br><div class="time">时间：${(entity.date)!""}</div>
    </div>
    <div style="clear: both">
        <table class="contentTable" style="width: 99%;float: left;">
            <tbody>
            <tr>
                <td style="width:10%;">出库单号</td>
                <td style="width:15%;">名称</td>
                <td style="width:15%;">规格</td>
                <td style="width:5%;">单位</td>
                <td style="width:10%;">数量</td>
                <td style="width:10%;">点数</td>
                <td style="width:10%;">剩余</td>
                <td style="width:15%;">客户单号</td>
                <td style="width:10%;">备注</td>
            </tr>
            <#list 0..9 as i>
            <tr>
                <td>${(entity.items[i].storageOutRecord.outNumber)!"&nbsp;"}</td>
                <td>${(entity.items[i].name)!"&nbsp;"}</td>
                <td>${(entity.items[i].standard)!"&nbsp;"}</td>
                <td>${(entity.items[i].unit)!"&nbsp;"}</td>
                <td>${(entity.items[i].count)!"&nbsp;"}</td>
                <td>${(entity.items[i].storageInputAssembly.expressId)!"&nbsp;"}</td>
                <td>${(entity.items[i].resetCount)!"&nbsp;"}</td>
                <td>${(entity.items[i].storageOutRecord.customOrderId)!"&nbsp;"}</td>
                <td>${(entity.items[i].remark)!"&nbsp;"}</td>
            </tr>
            </#list>

            </tbody>
            <tfoot>
            <tr style="height: 40px">
                <td>合计：</td>
                <td></td>
                <td></td>
                <td></td>
                <td>${totalCount!"&nbsp;"}</td>
                <td></td>
                <td>${totalResidue!"&nbsp;"}</td>
                <td></td>
                <td ></td>
            </tr>
            </tfoot>
        </table>
    </div>
      <div style="width:0;float: left;margin-left: 5px;">
		白联财务 红联客户 黄联仓库
    </div>
    <div>
        <div>
            <div class="consignee">收货人及经手人：</div>
            <div class="deliverer">送货人及经手人：</div>
        </div>
    </div>
</div>
<div >
    <button type="button" class="btn btn-success printButtonClass" onclick="printContent(this,1);">打印出库单</button>
    <button type="button" class="btn btn-success printButtonClass" onclick="printContent(this,2);">打印退货单</button>
</div>
</body>
</html>
<script>
    jQuery(function ($) {
    })
	//
    function printContent(o,type) {
        $(".printButtonClass").hide();
        var title = $(".title  h1");
        var str = title.text();
        if (type == 1) {
            str = str.replace("送货单", "出库单")
        } else if (type == 2) {
            str = str.replace("送货单", "退货单")
        }
        console.log(str,type,type == 3);
        title.html(str)
        bdhtml = window.document.body.innerHTML;//获取当前页的html代码
        window.print();
        window.document.body.innerHTML = bdhtml;

    }

</script>