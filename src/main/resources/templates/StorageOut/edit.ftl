<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <style type="text/css">
        .inputAssembly {
            clear: left;
            text-align: center;
            border: 1px solid #c1c184;
            margin-left: 5%;
            width: 80%
        }

        .inputAssembly select {
            width: 180px;
            height: 30px;
            float: right;
        }

        #editInputStorage input {
            height: 30px;
            width: 100%;
        }

        #editInputStorage select {
            height: 35px;
            width: 80%;
        }

        .inputAssembly .endTh {
            width: 90px;
        }

        .inputAssembly tr button {
            height: 28px;
            line-height: 10px;
            font-size: 15px;
        }

        .inputAssembly th {
            text-align: center;
        }
        .inputAfter{
            display: inline-block;
            margin-left: 25px;
        }

        /*.inputAssembly tbody th {
            border: 1px solid #d00f0f;
        }*/
    </style>
    <title>编辑出库</title>
</head>

<body>
<div>
    <h1 style="text-align: center;margin-left: -12%;">编辑出库</h1>
</div>
<div style="align-content: center">
    <form id="editInputStorage">
        <div class="add_menber" style="margin-left: 3%;">
            <ul class="page-content">
                <li><label class="label_name">入库单号：</label><span class="add_name">

                <input  id="orderId" value="${(entity.orderId)!""}" name="orderId" type="text"
                       class="text_add"/></span>
                </li>
                <#--<li><label class="label_name">客户单号：</label><span class="add_name">

                <input value="${(entity.customOrderId)!""}" name="customOrderId" type="text"
                       class="text_add"/></span>
                    <div class="prompt r_f"></div>
                </li>-->
<#if (entity.id)??>
                <li><label class="label_name">出库时间：</label><span class="add_name">

                <input name="id" value="${(entity.id)!""}" type="hidden"/>
                <input value="${((entity.outStorageTime)?string("yyyy-MM-dd"))!""}" name="outStorageTime" id="start"
                       type="text"
                       class="text_add"/></span>
                    <div class="prompt r_f"></div>
                </li>
</#if>
               <#-- <li><label class="label_name">客户公司：</label><span class="add_name">
                    <select name="companyId">
                            <option value="0">请选择</option>
                    <#list companyList as data>
                        <option  <#if (data.id=(entity.companyId)!0)>selected="selected"</#if>
                                 value="${data.id}">${data.name}</option>
                    </#list>
                        </select>
                </span>
                    *正常出库可以不选择-->
                <li><label class="label_name">出货类型：</label><span class="add_name">
                    <select style="    width: 150%;"  name="outType">
                            <option ${((entity.type)?? &&(entity.type==1))?string('selected="selected"','')}
                                    value="1">正常出库</option>
                            <option ${((entity.type)?? &&(entity.type==2))?string('selected="selected"','')}
                                    value="2">外协出库</option>
                        </select>
                </span>
                    <div class="prompt r_f"></div>
                </li>

            </ul>
        </div>
        <div class="inputAssembly">
            <div style="margin-bottom: 10px">
                <h3> 出货明细：</h3>
            </div>
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th>编码</th>
                    <th>数量</th>
                    <#--<th>单价</th>-->
                    <#--<th>点数</th>-->
                    <th>损耗</th>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody class="tbody">


                <#list 0..14 as i>
                <tr>
                    <#--<th>

                        <select name="assemblyName_${i}" onchange="selectAssemblyName(this,'assemblyId_${i}')">
                            <option value="">请选择</option>
                            <#list assemblyList as data>
                                <option value="${data.name}">${data.name}</option>
                            </#list>
                        </select>
                    </th>
                    <th>
                        <select name="assemblyId_${i}">
                            <option value="">请选择</option>
                        </select>
                    </th>-->
                    <th><input value="" name="code_${i}"/></th>
                    <th><input value="" name="count_${i}"/></th>
                    <#--<th><input value="" name="price_${i}"/></th>-->
                    <#--<th><input value="" name="point_${i}"/></th>-->
                    <th><input value="" name="waste_${i}"/></th>
                    <th><input value="" name="remark_${i}"/></th>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
        <div style="clear: left;">
            <button style="margin-left: 33%;" class="btn btn-primary" type="button"
                    onclick="window.history.back()">返回
            </button>
            <button style="margin-left: 14%;" class="btn btn-primary" type="button"
                    onclick="submitForm()">提交
            </button>
        </div>
    </form>
</div>

</body>
</html>
<script>
    var selectAssemblyCount = 2;
    var trString = '';

    function addSelectAssembly() {
        selectAssemblyCount++;
        var s = trString.replace(new RegExp("selectAssemblyCount", "gm"), selectAssemblyCount);
        $("tbody").append(s);
    }

    function deleteAssembly(count) {
        var s = "selectAssembly_" + count + "_tr";
        $("#" + s).remove();
    }

    function selectAssemblyName(e, modelNameSelect, doValue, value) {
        $.ajax({
            url: '/assembly/queryAssembly',
            method: "post",
            data: "name=" + e.value,
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            success: function (obj) {
                var s = "[name=" + modelNameSelect + "]";
                console.log(obj, s);
                var model = $(s)[0];
                console.log(model, model.options);
                model.options.length = obj.length + 1;
                //循环将数组中的数据写入<option>标记中
                for (var j = 0; j < obj.length; j++) {
                    model.options[j + 1].text = obj[j].modelName;
                    model.options[j + 1].value = obj[j].id;
                }
                if (doValue) {
                    $(s).val(value);
                }
            }
        });
    }

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }
    /**
     * 防止重复提交
     */
    var preventingDuplicateSubmissions = true;
    function submitForm() {
        if (!preventingDuplicateSubmissions) {
            return;
        }
        preventingDuplicateSubmissions = false;
        var data = $("#editInputStorage").serializeObject();
        console.debug(data);
        $.ajax({
            url: '/StorageOut/update',
            method: "post",
            data: JSON.stringify(data),
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            contentType: "application/json; charset=utf-8",
            success: function (obj) {
                preventingDuplicateSubmissions = true;
                if (obj && obj.success) {
                    layer.alert('编辑成功！', {
                        title: '提示框',
                        icon: 1,
                        yes: function () {
                            window.location.href = "/StorageOut/list";
                        }
                    });
                } else {
                    layer.alert('编辑失败！\n\r\n' + obj.msg, {
                        title: '提示框',
                        icon: 1
                    });
                }
            }
        });
    }

    var selectAssemblySJON = '${(selectAssemblySJON)!""}';
    $(function () {
        $("#orderId").blur(function (a, b) {
            var orderId = $("#orderId").val();
            $.ajax({
                url: '/StorageInput/queryStorageInput?orderId='+orderId,
                method: "post",
                contentType: "application/json; charset=utf-8",
                success: function (obj) {
                    if(obj && obj.totalCount && obj.totalCount >0){
                        for (var i = 0; i < obj.listData.length; i++) {
                            var entity = obj.listData[i];
                            $("[name=count_" + i + "]").val(entity.count);
                            $("[name=waste_" + i + "]").val("0");
//                            $("[name=remark_" + i + "]").val(entity.remark);
                            $("[name=code_" + i + "]").val(entity.assembly.code);
                        }
                    }
                }
            });
        });

        if (selectAssemblySJON !== '') {
            var selectAssembly = JSON.parse(selectAssemblySJON);
            console.log(selectAssembly);
            for (var i = 0; i < selectAssembly.length; i++) {
                var entity = selectAssembly[i];
                var a = $("[name=assemblyName_" + i + "]");
                a.val(entity.assemblyName);
                $("[name=count_" + i + "]").val(entity.count);
                $("[name=waste_" + i + "]").val(entity.waste);
                $("[name=remark_" + i + "]").val(entity.remark);
                $("[name=code_" + i + "]").val(entity.assembly.code);
            }
        }
    });
<#if (entity.id)??>
    laydate({
        elem: '#start',
        event: 'focus'
    });
</#if>

</script>