<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>出库列表</title>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" action="/StorageOut/list" method="post">
                <div class="search_style">
                    <input name="currentPage" value="${pageResult.currentPage}" type="hidden">
                    <ul class="search_content clearfix">
                        <li><label class="l_f">货源公司&emsp;</label><select name="companyId" style="width:145px">
                            <option value="0">请选择</option>
                        <#list companyList as data>
                            <option value="${data.id}">${data.name}</option>
                        </#list>
                        </select></li>
                        <li><label class="l_f">出库单号</label><input name="outNumber" type="text" class="text_add"
                                                                   value="${(qo.outNumber)!""}"
                                                                   style=" width:150px"/></li>
                        <li><label class="l_f"  >类型&emsp;</label><select name="assemblyType" style="width:145px">
                            <option value="0">请选择</option>
                            <option ${((qo.assemblyType)?? &&(qo.assemblyType==3))?string('selected="selected"','')} value="3">成品</option>
                            <option ${((qo.assemblyType)?? &&(qo.assemblyType==2))?string('selected="selected"','')}  value="2">物料</option>
                        </select></li>
                        <li><label class="l_f">名称</label><input name="assemblyName" type="text" class="text_add"
                                                                value="${(qo.assemblyName)!""}"
                                                                style=" width:150px"/></li>
                        <li><label class="l_f">客户单号</label><input name="customOrderId" type="text" class="text_add"
                                                                  value="${(qo.customOrderId)!""}"
                                                                  style=" width:150px"/></li>
						</br>
                        </br>
                        <li><label class="l_f">开始时间</label><input id="inputStartDate" name="beginDate" type="text" class="inputDate text_add"
                                                                   value="${(qo.beginDate)!""}"
                                                                   style=" width:150px"/></li>
                        <li><label class="l_f">结束时间</label><input id="inputEndDate" name="endDate" type="text" class="inputDate text_add"
                                                                   value="${(qo.endDate)!""}"
                                                                   style=" width:150px"/></li>
                        <li><label class="l_f">每页显示条数：</label>
                            <select name="pageSize">
                                <option ${(qo.pageSize == 10)?string('selected="selected"','2')?html}>10</option>
                                <option ${(qo.pageSize == 20)?string('selected="selected"','2')?html}>20</option>
                                <option ${(qo.pageSize == 50)?string('selected="selected"','2')?html}>50</option>
                                <option ${(qo.pageSize == 100)?string('selected="selected"','2')?html}>100</option>
                            </select>
                        </li>

                        <li style="">
                            <button type="submit" class="btn_search"><i class="icon-search"></i>查询</button>
                        </li>
                        <li style="width:150px;">
		                    <span class="l_f">
		                    <a href="/StorageOut/toEdit"  class="btn btn-success"><i
		                            class="icon-plus"></i>新增物料出库</a>
		                   	</span>
                  		 </li>
                        <li style="width:150px;">
		                    <span class="l_f">
		                    <a href="/StorageOut/toEditProduct"  class="btn btn-success"><i
		                            class="icon-plus"></i>新增成品出库</a>
		                   	</span>
                  		 </li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <th>入库单号</th>
                        <th>出库单号</th>
                        <th>货源公司</th>
                        <th>出库时间</th>
                        <th>出库类型</th>
                        <th>物料类型</th>
                        <th>编码</th>
                        <th>名称</th>
                        <th>规格型号</th>
                        <th>数量</th>
                        <th>出库点数</th>
                        <th>单价</th>
                        <th>金额</th>
                        <th>损耗</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as data>
                    <tr>
                        <td> ${(data.orderId)!""}</td>
                        <td>${(data.outNumber)!""}</td>
                        <td>${(data.companyName)!""}</td>
                        <td>${((data.outStorageTime)?string("yyyy-MM-dd"))!""}</td>
                        <td>${(data.outType==2)?string("外协出库","正常出库")}</td>
                        <td>${(data.assembly.type==3)?string("成品","物料")}</td>
                        <td>${(data.assembly.code)!""}</td>
                        <td>${(data.assemblyName)!""}</td>
                        <td>${(data.assemblyModelName)!""}</td>
                        <td>${(data.count)!""}</td>
                        <td>${(data.outPoint)!""}</td>
                        <td>${(data.storageOutRecord.outPrice)!""}</td>
                        <td>${(data.storageOutRecord.totalMoney)!""}</td>
                        <td>${(data.waste)!""}</td>
                        <td class="td-manage">
                            <a title="打印送货单" href="javascript:;" onclick="window.open('/StorageOut/print?outNumber=${(data.outNumber)!""}');"
                               class="btn btn-xs btn-info">打印送货单</a>
                            <a title="编辑" href="/StorageOut/${(data.assembly.type==3)?string("toEditProduct","toEdit")}?id=${(data.id)!""}"
                               class="btn btn-xs btn-info"><i class="icon-edit bigger-120"></i></a>
                            <a title="删除" href="javascript:;" onclick="member_del(this,${(data.id)!""})"
                               class="btn btn-xs btn-warning"><i class="icon-trash  bigger-120"></i></a>
                        </td>
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                <div style="margin-top: -25px;">
                    <div style="display: inline-block ; margin-left: 32%;">
                        <ul id="pagination" class="pagination"></ul>
                    </div>
                    <div style="margin-top: -50px; margin-left: 12%;">
                        <span style="">共${(pageResult.totalCount)!""}&nbsp;条数据，当前&nbsp;${(pageResult.totalPage)!""}&nbsp;页</span>
                    </div>
                </div>
                </tfoot>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    jQuery(function ($) {
        $('#pagination').twbsPagination({
            first: "首页",
            prev: "上一页",
            next: "下一页",
            last: "未页",
            startPage:${qo.currentPage},
            totalPages: ${pageResult.totalPage},
            visiblePages: ${qo.pageSize},
            onPageClick: function (event, page) {
                $("[name=currentPage]").val(page);
                $("#searchForm").submit();
            }
        });


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })
    /*用户-添加*/
    $('#member_add').on('click', function () {
        layer.open({
            type: 1,
            title: '添加公司',
            maxmin: true,
            shadeClose: true, //点击遮罩关闭层
            area: ['800px', ''],
            content: $('#add_menber_style'),
            btn: ['提交', '取消'],
            yes: function (index, layero) {
                var num = 0;
                var str = "";
                var data = {};
                $(".add_menber input[type$='text']").each(function (n) {
                    data[$(this).attr("name")] = $(this).val();
                    $(".add_menber input[type$='text']").each(function (n) {
                        data[$(this).attr("name")] = $(this).val();
                        if ($(this).val() == "") {
                            var name = $(this).attr("name");
                            var alertName = null;
                            if (name = "name") {
                                alertName = "企业名称";
                            }
                            if (name = "phone") {
                                alertName = "联系电话";
                            }
                            if (name = "contacts") {
                                alertName = "联系人";
                            }
                            if (!alertName) {
                                layer.alert(str += "" + $(this).attr("name") + "不能为空！\r\n", {
                                    title: '提示框',
                                    icon: 0,
                                });
                                num++;
                                return false;
                            }
                        }
                    });
                });
                console.log(data);
                if (num > 0) {
                    return false;
                }
                else {
                    $.ajax({
                        url: '/company/update',
                        method: "post",
                        data: data,
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        dataType: 'json',
                        success: function (obj) {
                            layer.close(index);//关闭
                            if (obj && obj.success) {
                                layer.alert('添加成功！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            } else {
                                layer.alert('添加失败！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            }
                        }
                    });


                }
            }
        });
    });

    /*用户-查看*/
    function member_show(title, url, id, w, h) {
        layer_show(title, url + '#?=' + id, w, h);
    }

    /*用户-停用*/
    function member_stop(obj, id) {
        layer.confirm('确认要停用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs " onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
            $(obj).remove();
            layer.msg('已停用!', {icon: 5, time: 1000});
        });
    }

    /*用户-启用*/
    function member_start(obj, id) {
        layer.confirm('确认要启用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs btn-success" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
            $(obj).remove();
            layer.msg('已启用!', {icon: 6, time: 1000});
        });
    }

    /*用户-编辑*/
    function member_edit(id) {
        var data = {};
        $.ajax({
            url: '/company/getByCondition?id=' + id,
            method: "get",
            dataType: 'json',
            success: function (obj) {
                data = obj[0];

                layer.open({
                    type: 1,
                    title: '修改企业信息',
                    maxmin: true,
                    shadeClose: false, //点击遮罩关闭层
                    area: ['800px', ''],
                    content: $('#add_menber_style'),
                    btn: ['提交', '取消'],
                    success: function (layero, lockIndex) {
                        var body = layer.getChildFrame('body', lockIndex);
                        pubUtil.load(layero, data);//填充表单
                    },
                    yes: function (index, layero) {
                        var num = 0;
                        var str = "";
                        data = {};
                        data.id = id;
                        $(".add_menber input[type$='text']").each(function (n) {
                            data[$(this).attr("name")] = $(this).val();
                            $(".add_menber input[type$='text']").each(function (n) {
                                data[$(this).attr("name")] = $(this).val();
                                if ($(this).val() == "") {
                                    var name = $(this).attr("name");
                                    var alertName = null;
                                    if (name = "name") {
                                        alertName = "企业名称";
                                    }
                                    if (name = "phone") {
                                        alertName = "联系电话";
                                    }
                                    if (name = "contacts") {
                                        alertName = "联系人";
                                    }
                                    if (!alertName) {
                                        layer.alert(str += "" + $(this).attr("name") + "不能为空！\r\n", {
                                            title: '提示框',
                                            icon: 0,
                                        });
                                        num++;
                                        return false;
                                    }
                                }
                            });
                        });
                        if (num > 0) {
                            return false;
                        }
                        else {
                            $.ajax({
                                url: '/company/update',
                                method: "post",
                                data: data,
                                type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                                dataType: 'json',
                                success: function (obj) {
                                    layer.close(index);//关闭
                                    if (obj && obj.success) {
                                        layer.alert('修改成功！', {
                                            title: '提示框',
                                            icon: 1,
                                        });
                                    } else {
                                        layer.alert('修改失败！', {
                                            title: '提示框',
                                            icon: 1,
                                        });
                                    }
                                }
                            });
                            layer.close(index);
                        }
                    }
                });
            }
        });

    }

    /*用户-删除*/
    function member_del(obj, id) {
        layer.confirm('确认要删除吗？', function (index) {
            $.ajax({
                url: '/StorageOut/delete?ids=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('已删除!', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('删除失败', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

    laydate({
        elem: '#inputStartDate',
        event: 'focus'
    });
    laydate({
        elem: '#inputEndDate',
        event: 'focus'
    });

</script>