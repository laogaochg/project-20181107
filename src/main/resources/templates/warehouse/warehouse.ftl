<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <style type="text/css">
        .inputAssembly {
            clear: left;
            text-align: center;
            border: 1px solid #c1c184;
            margin-left: 5%;
            width: 80%
        }

        .inputAssembly select {
            width: 180px;
            height: 30px;
            float: right;
        }

        #editInputStorage input {
            height: 30px;
            width: 100%;
        }

        #editInputStorage select {
            margin-left: 10px;
            height: 35px;
            width: 267px;
        }

        .inputAssembly .endTh {
            width: 90px;
        }

        .inputAssembly tr button {
            height: 28px;
            line-height: 10px;
            font-size: 15px;
        }

        .inputAssembly th {
            text-align: center;
        }

        /*.inputAssembly tbody th {
            border: 1px solid #d00f0f;
        }*/
    </style>
    <title>仓库信息</title>
</head>

<body>
<div>
    <h1 style="text-align: center;margin-left: -12%;">仓库信息</h1>
</div>
<#-- 来料加工，自动生成订单号，入库数量、入库分类、入库货物、单价、对应快递单号、入库时间、加工状态未完成-->
<#-- 日期公司名称，物料编号，数量，单位，组件数量，点数，组件单价，合计单价-->

<div style="align-content: center">
 <fieldset style="width: 80%;height:150;border:4px #EEEEEE groove;margin: 5%;" align=center>
  
   <br/>
    <form id="editInputStorage">
        <div class="add_menber" style="margin-left: 3%;">
            <ul class="page-content">
                <li><label class="label_name">公司名称：</label><span class="add_name">
                <input value="${(ware.warehouseName)!""}" name="warehouseName" type="text"
                       class="text_add" style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name">公司地址：</label><span class="add_name">
                <input value="${ware.warehouseAddress!""}" name="warehouseAddress" type="text" class="text_add"
                       style="width: 250px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <input name="id" value="${(entity.id)!""}" type="hidden"/>
                <li><label class="label_name">仓库电话：</label><span class="add_name">
                <input value="${(ware.warehousePhone)!""}" name="warehousePhone" type="text" class="text_add"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name">仓库传真：</label><span class="add_name">
                <input value="${(ware.warehouseFax)!""}" name="warehouseFax" type="text" class="text_add"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name">仓库邮箱：</label><span class="add_name">
                <input value="${(ware.warehouseEmail)!""}" name="warehouseEmail" type="text" class="text_add"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name"></label><span class="add_name">
                <input value="${(ware.id)!""}" name="id" type="hidden" class="text_add" id="id"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name"></label><span class="add_name">
                <input value="${(ware.warehouseManagId)!""}" name="warehouseManagId" type="hidden" class="text_add"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
             <!--    <li><label class="label_name">仓库管理：</label><span class="add_name">
                <input value="" name="warehouseManager" type="text" class="text_add"
                       style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
 				-->
            </ul>
        </div>
        <br/>
        <div style="clear: left;" >
            <button style="margin-left: 0%;" class="btn btn-primary" type="button"
                    onclick="window.history.back()">返回
            </button>
            <button style="margin-left: 14%;" id="submitDiv" class="btn btn-primary" type="button"
                    onclick="submitForm()">提交
            </button>
            <button style="margin-left: 14%;" id="viewDiv"  class="btn btn-primary" type="button"
                    onclick="editForm()">修改
            </button>
        </div>
    </form>
    <br/>
   <br/>
    </fieldset>
</div>

</body>
</html>
<script>

    $(function() {// 初始化内容
    	if($("#id").val()==null||$("#id").val()==""){
    		
    		$("#submitDiv").show();
        	$("#viewDiv").hide();
    	}else{
    		$("#submitDiv").hide();
        	$("#viewDiv").show();
        	$('input[type="text"]').attr('disabled','disabled');
    	}
    	
    });  
	function editForm(){
		$("#submitDiv").show();
    	$("#viewDiv").hide();
    	$('input[type="text"]').removeAttr('disabled');
	}
	
    function submitForm() {
        var data = $("#editInputStorage").serializeObject();
        $.ajax({
            url: '/ware/update',
            method: "post",
            data: JSON.stringify(data),
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            contentType: "application/json; charset=utf-8",
            success: function (obj) {
                if (obj && obj.success) {
                    layer.alert('编辑成功！', {
                        title: '提示框',
                        icon: 1,
                        yes: function () {
                            window.location.href = "/ware/list";
                        }
                    });
                } else {
                    layer.alert('编辑失败！\n\r\n' + obj.msg, {
                        title: '提示框',
                        icon: 1
                    });
                }
            }
        });
    }
    
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
</script>