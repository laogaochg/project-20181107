<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>出库列表</title>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" action="/order/list" method="post">
                <div class="search_style">
                    <input name="currentPage" value="${pageResult.currentPage}" type="hidden">
                    <ul class="search_content clearfix">
                        <li><label class="l_f">公司&emsp;</label><select name="companyId" style="width:145px">
                            <option value="0">请选择</option>
                        <#list companyList as data>
                            <option value="${data.id}">${data.name}</option>
                        </#list>
                        </select></li>
                        <li><label class="l_f">入库单号</label><input name="orderId" type="text" class="text_add"
                                                                  value="${(qo.outNumber)!""}"
                                                                  style=" width:150px"/></li>
                        <li><label class="l_f">名称</label><input name="assemblyName" type="text" class="text_add"
                                                                value="${(qo.assemblyName)!""}"
                                                                style=" width:150px"/></li>
                        <li><label class="l_f">客户单号</label><input name="customOrderId" type="text" class="text_add"
                                                                  value="${(qo.customOrderId)!""}"
                                                                  style=" width:150px"/></li>
                        </br>
                        </br>
                        <li><label class="l_f">开始时间</label><input id="inputStartDate" name="beginDate" type="text"
                                                                  class="inputDate text_add"
                                                                  value="${(qo.beginDate)!""}"
                                                                  style=" width:150px"/></li>
                        <li><label class="l_f">结束时间</label><input id="inputEndDate" name="endDate" type="text"
                                                                  class="inputDate text_add"
                                                                  value="${(qo.endDate)!""}"
                                                                  style=" width:150px"/></li>
                        <li><label class="l_f">每页显示条数：</label>
                            <select name="pageSize">
                                <option ${(qo.pageSize == 10)?string('selected="selected"','2')?html}>10</option>
                                <option ${(qo.pageSize == 20)?string('selected="selected"','2')?html}>20</option>
                                <option ${(qo.pageSize == 50)?string('selected="selected"','2')?html}>50</option>
                                <option ${(qo.pageSize == 100)?string('selected="selected"','2')?html}>100</option>
                            </select>
                        </li>

                        <li style="">
                            <button type="submit" class="btn_search"><i class="icon-search"></i>查询</button>
                        </li>
                        <li><label class="l_f">对账单类型：</label>
                            <select name="outType">
                                <option value="1">正常出库</option>
                                <option value="2">外协出库</option>
                            </select>
                        </li>
                        <li style="width: 150px">
                            <button type="button" onclick="toAccountStatement(this)"
                                    class="btn btn-success accountStatement">生成对账单
                            </button>
                        </li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <th>入库单号</th>
                        <th>名称</th>
                        <th>规格</th>
                        <th>公司名称</th>
                        <th>创建时间</th>
                        <th>单位</th>
                        <th>入库数量</th>
                        <th>已出库数量</th>
                        <th>已出库金额</th>
                        <th>剩余数量</th>
                    <#--<th>应结金额</th>-->
                    <#--<th>损耗</th>-->
                    <#--<th>订单修改备注</th>-->
                    <#--<th>操作</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as data>
                    <tr>
                        <td> ${(data.orderId)!""}</td>
                        <td> ${(data.assemblyName)!""}</td>
                        <td> ${(data.assembly.modelName)!""}</td>
                        <td> ${(data.companyName)!""}</td>
                        <td>${((data.createTime)?string("yyyy-MM-dd"))!""}</td>
                        <td>PCS</td>
                        <td>${(data.inputCount)!"0"}</td>
                        <td>${(data.outCount)!"0"}</td>
                        <td>
                            <#if shopId = 2>
                            ${(data.money)!""}
                            <#elseif shopId = 0>
                            ${(data.money)!""}
                            <#else>
                                -
                            </#if>
                        </td>
                        <td>${(data.resertCount)!"0"}</td>
                    <#--<td>${(data.shouldGetCount)!"0"}</td>-->
                    <#--<td>${(data.wasteCount)!"0"}</td>-->
                    <#--<td>${(data.remark)!""}</td>-->
                    <#--<td class="td-manage">-->
                    <#-- <a title="编辑" href="/StorageOut/toEdit?id=${(data.id)!""}"
                           class="btn btn-xs btn-info"><i class="icon-edit bigger-120"></i></a>
                        <a title="删除" href="javascript:;" onclick="member_del(this,${(data.id)!""})"
                           class="btn btn-xs btn-warning"><i class="icon-trash  bigger-120"></i></a>-->
                    <#--</td>-->
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                <div style="margin-top: -25px;">
                    <div style="display: inline-block ; margin-left: 32%;">
                        <ul id="pagination" class="pagination"></ul>
                    </div>
                    <div style="margin-top: -50px; margin-left: 12%;">
                        <span style="">共${(pageResult.totalCount)!""}&nbsp;条数据，当前&nbsp;${(pageResult.totalPage)!""}&nbsp;页</span>
                    </div>
                </div>
                </tfoot>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    jQuery(function ($) {
        $('#pagination').twbsPagination({
            first: "首页",
            prev: "上一页",
            next: "下一页",
            last: "未页",
            startPage:${qo.currentPage},
            totalPages: ${pageResult.totalPage},
            visiblePages: ${qo.pageSize},
            onPageClick: function (event, page) {
                $("[name=currentPage]").val(page);
                $("#searchForm").submit();
            }
        });


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })

    function toAccountStatement(a) {
        var data = $("#searchForm").serializeObject();
        var message = null;
        if (!data.beginDate) {
            message = "请选择对账开始时间";
        }
        if (!data.endDate) {
            message = "请选择对账结束时间";
        }
        if (!data.outType) {
            message = "请选择对账单类型";
        }
        if (data.companyId < 1) {
            message = "请选择对账公司";
        }
        console.debug(data,message);
        var dataString = "?beginDate=" + data.beginDate
                + "&companyId=" + data.companyId
                + "&endDate=" + data.endDate
                + "&outType=" + data.outType;
        if (!message) {
            layer.alert('确定下载数据？', {
                title: '提示框',
                icon: 1,
                btn: ['确实', '取消'],
                yes: function (index, a) {
                    console.log(index, a)
                    layer.close(index);//关闭
                    window.location.href = "/order/exportAccountStatement" + dataString;
                }
            });
        } else {
            layer.alert(message, {
                title: '提示框',
                icon: 1,
                btn: ['确定']
            });
        }
    }

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }


    laydate({
        elem: '#inputStartDate',
        event: 'focus'
    });
    laydate({
        elem: '#inputEndDate',
        event: 'focus'
    });

</script>