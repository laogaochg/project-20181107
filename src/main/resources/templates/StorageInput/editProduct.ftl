<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <style type="text/css">
        .inputAssembly {
            clear: left;
            text-align: center;
            border: 1px solid #c1c184;
            margin-left: 5%;
            width: 80%
        }

        .inputAssembly select {
            width: 180px;
            height: 30px;
            float: right;
        }

        #editInputStorage input {
            height: 30px;
            width: 100%;
        }

        #editInputStorage select {
            margin-left: 10px;
            height: 35px;
            width: 267px;
        }

        .inputAssembly .endTh {
            width: 90px;
        }

        .inputAssembly tr button {
            height: 28px;
            line-height: 10px;
            font-size: 15px;
        }

        .inputAssembly th {
            text-align: center;
        }

        /*.inputAssembly tbody th {
            border: 1px solid #d00f0f;
        }*/
    </style>
    <title>入库列表</title>
</head>

<body>
<div>
    <h1 style="text-align: center;margin-left: -12%;">编辑入库</h1>
</div>
<#--来料加工，自动生成订单号，入库数量、入库分类、入库货物、单价、对应快递单号、入库时间、加工状态未完成-->
<#--日期，公司名称，物料编号，数量，单位，组件数量，点数，组件单价，合计单价-->
<div style="align-content: center">
    <form id="editInputStorage">
        <div class="add_menber" style="margin-left: 3%;">
            <ul class="page-content">
            <#if (entity.id)??>
                <li><label class="label_name">入库时间：</label><span class="add_name">
                <input value="${(entity.inputStorageTimeString)!""}" name="inputStorageTime" id="start" type="text"
                       class="text_add" style="width: 150px;"/></span>
                    <div class="prompt r_f"></div>
                </li>
            </#if>
                <input name="id" value="${(entity.id)!""}" type="hidden"/>
            <#--<li><label class="label_name">快递单号：</label><span class="add_name">
            <input value="${(entity.expressId)!""}" name="expressId" type="text" class="text_add"
                   style="width: 150px;"/></span>
                <div class="prompt r_f"></div>
            </li>-->
            <#--<li><label class="label_name">客户：</label><span class="add_name">
            <input value="${(entity.custom)!""}" name="custom" type="text" class="text_add"
                   style="width: 150px;"/></span>
                <div class="prompt r_f"></div>
            </li>-->
            <#--<li><label class="label_name">客户单号：</label><span class="add_name">
            <input value="${(entity.customOrderId)!""}" name="customOrderId" type="text" class="text_add"
                   style="width: 150px;"/></span>
                <div class="prompt r_f"></div>
            </li>-->
                <li><label class="label_name">客户公司：</label><span class="add_name">
                    <select name="companyId" onchange="selectCompanyId(this)" style="width: 150px;">
                            <option value="0">请选择</option>
                    <#list companyList as data>
                        <option  <#if (data.id=(entity.companyId)!0)>selected="selected"</#if>
                                 value="${data.id}">${data.name}</option>
                    </#list>
                        </select>
                </span>
                <li><label class="label_name">入库类型：</label><span class="add_name">
                    <select name="type" style="width: 150px;">
                            <option ${((entity.type)?? &&(entity.type==1))?string('selected="selected"','')}
                                    value="1">正常入库</option>
                            <option ${((entity.type)?? &&(entity.type==2))?string('selected="selected"','')}
                                    value="2">外协入库</option>
                        </select>
                </span>
                    <div class="prompt r_f"></div>
                </li>
                <li><label class="label_name">备注：</label>
                    <span class="add_name">
                <input value="${(entity.remark)!""}" name="remark" type="text" class="text_add" style="width: 150px;"/>
                    </span>
                    <div class="prompt r_f"></div>
                </li>

            </ul>
        </div>
        <div class="inputAssembly">
            <div style="margin-bottom: 10px">
                <h3> 明细：</h3>
            </div>
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th>名称</th>
                    <th>规格型号</th>
                    <th>单位</th>
                    <th>客户单号</th>
                    <th>数量</th>
                    <th>出库点数</th>
                    <th>原始点数</th>
                    <th>单价</th>
                </tr>
                </thead>
                <tbody class="tbody">

                <#list 1..15 as i>
                <tr>
                    <th>

                        <select name="assemblyName_${i}" onchange="selectAssemblyName(this,'assemblyModelName_${i}')">
                            <option value="">请选择</option>
                            <#list assemblyList as data>
                                <option value="${data.name}">${data.name}</option>
                            </#list>
                        </select>
                    </th>
                    <th>
                        <select name="assemblyModelName_${i}">
                            <option value="0">请选择</option>
                        </select>
                    </th>
                    <th><input value="PCS" readonly="readonly" name="unit_${i}"/></th>
                    <th><input value="" name="customOrderId_${i}"/></th>
                    <th><input value="" name="count_${i}"/></th>
                    <th><input value="" name="expressId_${i}"/></th>
                    <th><input value="" name="point_${i}"/></th>
                    <th><input value="" name="price_${i}"/></th>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
        <div style="clear: left;">
            <button style="margin-left: 33%;" class="btn btn-primary" type="button"
                    onclick="window.history.back()">返回
            </button>
            <button style="margin-left: 14%;" class="btn btn-primary" type="button"
                    onclick="submitForm()">提交
            </button>
        </div>
    </form>
</div>

</body>
</html>
<script>
    var selectAssemblyCount = 2;
    var trString = '<tr id="selectAssembly_selectAssemblyCount_tr" >\n' +
            '                    <th>\n' +
            '                        <select name="assemblyIds">\n' +
            '                            <option value="0">请选择</option>\n' +
            '                        <#list assemblyList as data>\n'+
            '                            <option value="${data.id}">${data.name}</option>\n' +
            '                        </#list>\n' +
            '                        </select>\n' +
            '                    </th>\n' +
            '                    <th><input name="standardList"/></th>\n' +
            '                    <th><input name="unitList"/></th>\n' +
            '                    <th><input name="unitPointCountList"/></th>\n' +
            '                    <th><input value="" name="assemblyPriceList"/></th>\n' +
            '                    <th><input value="" name="countList"/></th>\n' +
            '                    <th class="endTh" style="border-width: 0px">\n' +
            '                        <button class="btn btn-info" type="button" onclick="deleteAssembly(selectAssemblyCount)">删除</button>\n' +
            '                    </th>\n' +
            '                </tr>';

    function addSelectAssembly() {
        selectAssemblyCount++;
        var s = trString.replace(new RegExp("selectAssemblyCount", "gm"), selectAssemblyCount);
        $("tbody").append(s);
    }

    function deleteAssembly(count) {
        var s = "selectAssembly_" + count + "_tr";
        $("#" + s).remove();
    }

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    function selectCompanyId(ele, doValue) {
        var companyId = $("[name=companyId]").val();
        $.ajax({
            url: '/assembly/queryAssembly',
            method: "post",
            data: "companyId=" + companyId,
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            success: function (obj) {
                var selectData = [];
                for (var i = 0; i < obj.length; i++) {
                    var add = true;
                    for (var j = 0; j < selectData.length; j++) {
                        if (selectData[j] && (selectData[j].name === obj[i].name)) {
                            add = false;
                        }
                    }
                    if (add) {
                        selectData.push(obj[i]);
                    }
                }
                obj = selectData;
                //assemblyName_1
                for (var i = 1; i <= 15; i++) {
                    var s = "[name=assemblyName_" + i + "]";
                    var model = $(s)[0];
                    model.options.length = obj.length + 1;
                    //循环将数组中的数据写入<option>标记中
                    for (var j = 0; j < obj.length; j++) {
                        model.options[j + 1].text = obj[j].name;
                        model.options[j + 1].value = obj[j].name;
                    }
                    if (doValue && doValue[i - 1]) {
                        $(s).val(doValue[i - 1].assemblyName);
                        var e = {};
                        e.value = doValue[i - 1].assemblyName;
                        selectAssemblyName(e, 'assemblyModelName_' + i, true, doValue[i - 1].assemblyId)
                    }
                }
            }
        });
    }

    function selectAssemblyName(e, modelNameSelect, doValue, value) {
        var companyId = $("[name=companyId]").val();
        $.ajax({
            url: '/assembly/queryAssembly',
            method: "post",
            data: "name=" + e.value + "&companyId=" + companyId,
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            success: function (obj) {
                var s = "[name=" + modelNameSelect + "]";
                console.log(obj, s);
                var model = $(s)[0];
                console.log(model, model.options);
                model.options.length = obj.length + 1;
                //循环将数组中的数据写入<option>标记中
                for (var j = 0; j < obj.length; j++) {
                    model.options[j + 1].text = obj[j].modelName;
                    model.options[j + 1].value = obj[j].id;
                }
                if (doValue) {
                    $(s).val(value);
                }
            }
        });
    }

    /**
     * 防止重复提交
     */
    var preventingDuplicateSubmissions = true;
    function submitForm() {
        if (!preventingDuplicateSubmissions) {
            return;
        }
        preventingDuplicateSubmissions = false;
        var data = $("#editInputStorage").serializeObject();
        data.updateType = 1;
        console.debug(data);
        var str = "";
        for (var i = 1; i <= 15; i++) {
            console.debug(data["assemblyModelName_" + i]
                    , data["customOrderId_" + i]
                    , data["count_" + i]
                    , data["expressId_" + i]
                    , data["point_" + i]
                    , data["price_" + i]
            );
            if (data["assemblyModelName_" + i] > 0) {
                if (!data["customOrderId_" + i]) {
                    str = "客户单号不能为空。";
                    break;
                }
                if (!data["count_" + i]) {
                    str = "数量不能为空。";
                    break;
                }
                if (!data["expressId_" + i]) {
                    str = "出库存点不能为空。";
                    break;
                }
                if (!data["point_" + i]) {
                    str = "原始点数不能为空。";
                    break;
                }
                if (!data["price_" + i]) {
                    str = "单价不能为空。";
                    break;
                }
            }
        }
        if (str === "") {
        } else {
            preventingDuplicateSubmissions = true;
            layer.alert(str, {
                title: '提示框',
                icon: 1,
            });
        }
        if (str === "") {
        } else {
            preventingDuplicateSubmissions = true;
            return;
        }

        $.ajax({
            url: '/StorageInput/update',
            method: "post",
            data: JSON.stringify(data),
            type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
            contentType: "application/json; charset=utf-8",
            success: function (obj) {
                preventingDuplicateSubmissions = true;
                if (obj && obj.success) {
                    layer.alert('编辑成功！', {
                        title: '提示框',
                        icon: 1,
                        yes: function () {
                            window.location.href = "/StorageInput/list";
                        }
                    });
                } else {
                    layer.alert('编辑失败！\n\r\n' + obj.msg, {
                        title: '提示框',
                        icon: 1
                    });
                }
            }
        });
    }

    var selectAssemblySJON = '${(selectAssemblySJON)!""}';
    $(function () {
        if (selectAssemblySJON !== '') {
            var selectAssembly = JSON.parse(selectAssemblySJON);
            console.log(selectAssembly);
            var companyId = $("[name=companyId]").val();
            selectCompanyId(null, selectAssembly);
            for (var i = 0; i < selectAssembly.length; i++) {
                var entity = selectAssembly[i];
                var count = i + 1;
//                $("[name=assemblyName_" + count + "]").val(entity.assemblyCode);
                $("[name=count_" + count + "]").val(entity.count);
                $("[name=point_" + count + "]").val(entity.point);
                $("[name=customOrderId_" + count + "]").val(entity.storageInputAssembly.customOrderId);
                $("[name=expressId_" + count + "]").val(entity.expressId);
                $("[name=price_" + count + "]").val(entity.storageInputAssembly.price);
                //下拉框的赋值
//                selectAssemblyName(a[0], 'assemblyModelName_' + count, true, entity.standard);
            }
        }
    });


    <#if (entity.id)??>
    laydate({
        elem: '#start',
        event: 'focus'
    });
    </#if>

</script>