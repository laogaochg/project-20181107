<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>入库列表</title>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" action="/monthYield/list">
                <div class="search_style">
                    <ul class="search_content clearfix">
                        <li style="margin-left: 10%;"><label class="l_f">当前产量归属月份</label><input name="month" id="month"
                                                                                                type="text"
                                                                                                class="text_add"
                                                                                                value="${(qo.month)!""}"
                                                                                                style=" width:150px"/>
                        </li>

                        <li style="width:100px;">
                            <button type="submit" class="btn_search"><i class="icon-search"></i>查询</button>
                        </li>
                        <li style="width:150px;">
                            <button style="width:150px;" type="button" onclick="getDataFile()"  class="btn btn-xs btn-success">导出产量数据</button>
                        </li>
                        <li style="width:290px;">
                        </li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <!-- <th width="25"><label><input type="checkbox" class="ace"><span class="lbl"></span></label></th>-->
                    <#--物料编号	出货数量	单价	减上月未出货数量	加本月未出货数量
                    实际数量	原始点数	出货点数	原始点数合计	出货点数合计-->
                    <#--<th>主板ID</th>-->
                        <#--<th>公司名称</th>-->
                        <th>主板名称</th>
                        <th>主板规格</th>
                        <#--<th>主板编码</th>-->
                        <th>出货数量</th>
                        <th>单价</th>
                        <th>上月未出货数量</th>
                        <th>本月未出货数量</th>
                        <th>实际数量</th>
                        <th>单位</th>
                        <th>原始点数</th>
                        <th>出货点数</th>
                        <th>原始点数合计</th>
                        <th>出货点数合计</th>
                        <th>金额</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult as data>
                    <tr>
                        <!--<td><label><input type="checkbox" class="ace"><span class="lbl"></span></label></td>-->
                    <#--<td> ${(data.assembly.id)!""}</td>-->
                        <#--<td>${(data.assembly.companyId)!""}</td>-->
                        <td>${(data.assembly.name)!""}</td>
                        <td>${(data.assembly.modelName)!""}</td>
                        <#--<td>${(data.assembly.code)!""}</td>-->
                        <td>${(data.monthYieldDto.totalCount)!""}</td>

                        <td>
                            <#if shopId = 2>
                        ${(data.monthYieldDto.price)!""}
                            <#elseif shopId = 0>
                        ${(data.monthYieldDto.price)!""}
                            <#else>
                                -
                            </#if>
                        </td>
                        <td>${(data.monthYieldDto.monthYield.beforeMonthNotOutCount)!"未设置"}</td>
                        <td>${(data.monthYieldDto.monthYield.currentMonthNotOutCount)!"未设置"}</td>
                        <td>${(data.monthYieldDto.totalActualCount)!""}</td>
                        <td>PCS</td>
                        <td>${(data.monthYieldDto.avgInputPoint)!""}</td>
                        <td>${(data.monthYieldDto.avgOutPoint)!""}</td>
                        <td>${(data.monthYieldDto.totalInputPoint)!""}</td>
                        <td>${(data.monthYieldDto.totalOutPoint)!""}</td>
                        <td>
                            <#if shopId = 2>
                        ${(data.monthYieldDto.totalMoney)!""}
                            <#elseif shopId = 0>
                        ${(data.monthYieldDto.totalMoney)!""}
                            <#else>
                                -
                            </#if>
                        </td>
                        <td class="td-manage">
                            <a title="编辑" onclick="set_price(
                                    '${(data.assembly.id)!""}'
                                    ,'${(qo.month)!""}'
                                    ,'${(data.monthYieldDto.monthYield.beforeMonthNotOutCount)!"0"}'
                                    ,'${(data.monthYieldDto.monthYield.currentMonthNotOutCount)!"0"}'
                                    )"
                               href="javascript:;"
                               class="btn btn-xs btn-success">设置未出货数量</a>
                        </td>
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                </tfoot>
            </div>
        </div>
    </div>
</div>
<div class="add_menber add_price" id="add_price" style="display:none">
    <ul class="">
        <li style="width: 60%;margin-left: 15%;">
            上月未出货数量：
            <input value="" name="beforeMonthNotOutCount" type="text" class="text_add"/>
        </li>
        <li style="width: 60%;margin-left: 15%;">
            本月未出货数量：
            <input name="currentMonthNotOutCount" style="margin-left: 11px;" type="text" class="text_add"/>
        </li>

    </ul>
</div>
</body>
</html>
<script>
    jQuery(function ($) {


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })

    function getDataFile(a) {
        var month = $("#month").val();
        var message = null;
        if (!month) {
            message = "请选择时间";
        }
        console.debug(message);
        var dataString = "?month=" + month;
        if (!message) {
            layer.alert('确定下载数据？', {
                title: '提示框',
                icon: 1,
                btn: ['确实', '取消'],
                yes: function (index, a) {
                    console.log(index, a)
                    layer.close(index);//关闭
                    window.location.href = "/monthYield/exportFile" + dataString;
                }
            });
        } else {
            layer.alert(message, {
                title: '提示框',
                icon: 1,
                btn: ['确定']
            });
        }
    }

    function set_price(assemblyId, month, beforeMonthNotOutCount, currentMonthNotOutCount) {
        var data = {};
        data.assemblyId = assemblyId;
        data.month = month;
        data.beforeMonthNotOutCount = beforeMonthNotOutCount;
        data.currentMonthNotOutCount = currentMonthNotOutCount;
        layer.open({
            type: 1,
            title: '修改',
            maxmin: true,
            shadeClose: false, //点击遮罩关闭层
            area: ['600px', '300px'],
            content: $('#add_price'),
            btn: ['提交', '取消'],
            success: function (layero, lockIndex) {
                var body = layer.getChildFrame('body', lockIndex);
                pubUtil.load(layero, data);//填充表单
            },
            yes: function (index, layero) {
                var num = 0;
                $(".add_price input[type$='text']").each(function (n) {
                    data[$(this).attr("name")] = $(this).val();
                });
                var str = null;
                if (data.beforeMonthNotOutCount == null || data.beforeMonthNotOutCount == '') {
                    str = "上月未出货数量";
                }
                if (data.currentMonthNotOutCount == null || data.currentMonthNotOutCount == '') {
                    str = "本月未出货数量";
                }
                console.log(data, str);
                if (str != null) {
                    layer.alert(str + "不能为空！\r\n", {
                        title: '提示框',
                        icon: 0,
                    });
                    return false;
                } else {
                    $.ajax({
                        url: '/monthYield/update',
                        method: "post",
                        data: JSON.stringify(data),
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        contentType: "application/json; charset=utf-8",
                        success: function (obj) {
                            layer.close(index);//关闭
                            if (obj && obj.success) {
                                layer.alert('修改成功！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            } else {
                                layer.alert('修改失败！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            }
                        }
                    });
                    layer.close(index);
                }
            }
        });
    }


    laydate({
        elem: '#month',
        event: 'focus'
    });
</script>