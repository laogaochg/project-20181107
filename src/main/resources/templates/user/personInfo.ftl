<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>入库列表</title>
    <style type="text/css">
        #edit_personal_info {
            padding: 10px;
            margin: 60px 30%;
        }
    </style>
</head>

<body>
<!--用户图层-->
<div class="add_menber clearfix" id="add_menber_style">

    <ul class=" page-content">
        <li><label class="label_name">用户账号：</label><span class="add_name">
                ${user.email}</span>
            <div class="prompt r_f"></div>
        </li>
        <li><label class="label_name">名字：</label><span class="add_name"> ${(user.name)!""}</span>
            <div class="prompt r_f"></div>
        </li>
        <li id="passwordLi"><label class="label_name">密码：</label>
            <div class="prompt r_f"></div>
            <a href="javascript:void(0)" class="btn btn-warning" id="change_Password">修改密码</a>
        </li>
        <li><label class="label_name">联系电话：</label><span class="add_name">${(user.mobile)!""}</span>
            <div class="prompt r_f"></div>
        </li>

        <li><label class="label_name">邮箱：</label><span class="add_name">${(user.nickname)!""}</span>
            <div class="prompt r_f"></div>
        </li>

        <#if user.email == "admin">
        <li><label class="label_name">归属仓库：</label>
            <span class="add_name" style="margin-left: 20px;">
                ${typeStr}
            </span>
            <div class="prompt r_f"></div>
        </li>
    </ul>
    </#if>

    <!--编辑用户图层-->
    <div class="add_menber" id="edit_menber_style" style="display:none">

        <ul class=" page-content">
            <li><label class="label_name">用户账号：</label><span class="add_name">
            <input id="addEmail" name="email" type="text" value="${(user.email)!""}" class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">名字：</label><span class="add_name"><input name="name" type="text"
                                                                                   value="${(user.name)!""}"
                                                                                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>

            <li><label class="label_name">联系电话：</label><span class="add_name"><input name="mobile" type="text"
                                                                                     value="${(user.mobile)!""}"
                                                                                     class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>

            <li><label class="label_name">邮箱：</label><span class="add_name"><input name="nickname" type="text"
                                                                                   value="${(user.nickname)!""}"
                                                                                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>


        </ul>
    </div>
</div>

<input value="${user.id}" id="user_id" style="display: none;"/>
<input value="${user.pswd}" id="user_pswd" style="display: none;"/>

<#--编辑个人信息按钮-->
    <div id="edit_personal_info">
        <a title="编辑" id="edit_info_btn" href="javascript:;" class="btn btn-xs btn-info okmemo-tmp-unselect"><i
                    class="icon-edit bigger-120"></i>编辑信息</a>
    </div>
<#--修改密码-->
<div class="change_Pass_style" id="change_Pass">
    <ul class="xg_style">
        <li><label class="label_name">原&nbsp;&nbsp;密&nbsp;码</label><input name="原密码" type="password" class=""
                                                                          id="password"></li>
        <li><label class="label_name">新&nbsp;&nbsp;密&nbsp;码</label><input name="新密码" type="password" class=""
                                                                          id="Nes_pas"></li>
        <li><label class="label_name">确认密码</label><input name="再次确认密码" type="password" class="" id="c_mew_pas"></li>
    </ul>
</div>
</body>
</html>
<script>
    jQuery(function ($) {


    })

    //修改密码
    $('#change_Password').on('click', function () {
        layer.open({
            type: 1,
            title: '修改密码',
            area: ['300px', '300px'],
            shadeClose: true,
            content: $('#change_Pass'),
            btn: ['确认修改'],
            yes: function (index, layero) {
                if ($("#password").val() == "") {
                    layer.alert('原密码不能为空!', {
                        title: '提示框',
                        icon: 0,

                    });
                    return false;
                }
                if ($("#Nes_pas").val() == "") {
                    layer.alert('新密码不能为空!', {
                        title: '提示框',
                        icon: 0,

                    });
                    return false;
                }

                if ($("#c_mew_pas").val() == "") {
                    layer.alert('确认新密码不能为空!', {
                        title: '提示框',
                        icon: 0,

                    });
                    return false;
                }
                if (!$("#c_mew_pas").val || $("#c_mew_pas").val() != $("#Nes_pas").val()) {
                    layer.alert('密码不一致!', {
                        title: '提示框',
                        icon: 0,

                    });
                    return false;
                } else {
                    var data = {
                        oldPassword: $("#password").val(),
                        newPassword: $("#Nes_pas").val()
                    }
                    $.ajax({
                        url: '/user/changePassword',
                        method: "post",
                        data: data,
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        dataType: 'json',
                        success: function (obj) {
                            layer.close(index);//关闭
                            if (obj && obj.success && !obj.msg.includes("原密码不正确")) {
                                layer.alert('修改成功！', {
                                    title: '提示框',
                                    icon: 1,
                                });
                            } else {
                                layer.alert(obj.msg, {
                                    title: '提示框',
                                    icon: 1,
                                });
                            }
                        }
                    });
                }
            }
        });
    });


    //编辑个人信息
    $('#edit_info_btn').on('click', function () {
        var id = $("#user_id").val();
        console.log(id);
        $.ajax({
            url: '/user/getByCondition?id=' + id,
            method: "get",
            dataType: 'json',
            success: function (obj) {
                data = obj;

                layer.open({
                    type: 1,
                    title: '修改用户信息',
                    maxmin: true,
                    shadeClose: false, //点击遮罩关闭层
                    area: ['800px', ''],
                    content: $('#edit_menber_style'),
                    btn: ['提交', '取消'],
                    success: function (layero, lockIndex) {
                        var body = layer.getChildFrame('body', lockIndex);
                        pubUtil.load(layero, data);//填充表单
                        $(".selectType").val(data.type);
                    },
                    yes: function (index, layero) {
                        data.id = id;
                        $(".add_menber input[type='text']").each(function (n, k) {
                            data[k.name] = k.value;
                        });
                        var alertName = null;
                        data.type = $("[name=type]").val();
                        if (data.email == null || data.email == '') {
                            alertName = "用户账号";
                        }
                        if (alertName != null) {
                            layer.alert(alertName + "不能为空\r\n", {
                                title: '提示框',
                                icon: 0,
                            });
                        } else {
                            $.ajax({
                                url: '/user/editUser',
                                method: "post",
                                data: data,
                                type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                                dataType: 'json',
                                success: function (obj) {
                                    layer.close(index);//关闭
                                    if (obj && obj.success) {
                                        layer.alert('修改成功！', {
                                            title: '提示框',
                                            icon: 1
                                        });
                                    } else {
                                        layer.alert('修改失败！', {
                                            title: '提示框',
                                            icon: 1
                                        });
                                    }
                                }
                            });
                            layer.close(index);
                        }
                    }
                });
            }
        });
    });

</script>
