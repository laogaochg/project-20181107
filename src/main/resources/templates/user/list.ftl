<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>入库列表</title>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" action="/user/list">
                <div class="search_style">
                    <input name="currentPage" value="${pageResult.currentPage}" type="hidden">
                    <ul class="search_content clearfix">
                        <li><label class="l_f">用户账号</label><input name="email" type="text" class="text_add"
                                                                  value="${(qo.email)!""}"
                                                                  style=" width:200px"/></li>
                        <li><label class="l_f">每页显示条数：</label>
                            <select name="pageSize">
                                <option ${(qo.pageSize == 10)?string('selected="selected"','2')?html}>10</option>
                                <option ${(qo.pageSize == 20)?string('selected="selected"','2')?html}>20</option>
                                <option ${(qo.pageSize == 50)?string('selected="selected"','2')?html}>50</option>
                                <option ${(qo.pageSize == 100)?string('selected="selected"','2')?html}>100</option>
                            </select>
                        </li>

                        <li style="width:150px;">
                            <button type="submit" class="btn_search"><i class="icon-search"></i>查询</button>
                        </li>
                        <li style="width:90px;">
                    <span class="l_f">
                    <a href="javascript:ovid()" id="member_add" class="btn btn-warning"><i
                            class="icon-plus"></i>添加用户</a>
                   </span></li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <th width="25"><label><input type="checkbox" class="ace"><span class="lbl"></span></label></th>
                        <th>ID</th>
                        <th>用户账号</th>
                        <th>邮箱</th>
                        <th>状态</th>
                        <th>归属仓库编号</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as data>
                    <tr>
                        <td><label><input type="checkbox" class="ace"><span class="lbl"></span></label></td>
                        <td> ${(data.id)!""}</td>
                        <td>${(data.email)!""}</td>
                        <td>${(data.nickname)!""}</td>
                        <td>${(data.statusDisPlay)!""}</td>
                        <td>${(data.type)!""}</td>
                        <td class="td-manage">
                            <a title="" onclick="resetUserPassword(${(data.id)!""})" href="javascript:;"
                               class="btn btn-xs btn-default">重置密码</a>
                            <a title="" onclick="updateUserLogin(${data.status},${(data.id)!""})" href="javascript:;"
                               class="btn btn-xs btn-default">${(data.status==1)?string('禁止该用户登陆','允许该用户登陆')?html}</a>
                            <a title="编辑" onclick="member_edit(${(data.id)!""})" href="javascript:;"
                               class="btn btn-xs btn-info"><i class="icon-edit bigger-120"></i></a>
                            <a title="删除" href="javascript:;" onclick="member_del(this,${(data.id)!""})"
                               class="btn btn-xs btn-warning"><i class="icon-trash  bigger-120"></i></a>
                        </td>
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                <div style="margin-top: -25px;">
                    <div style="display: inline-block ; margin-left: 32%;">
                        <ul id="pagination" class="pagination"></ul>
                    </div>
                    <div style="margin-top: -50px; margin-left: 12%;">
                        <span style="">共${(pageResult.totalCount)!""}条数据，${(pageResult.totalPage)!""}页</span>
                    </div>
                </div>
                </tfoot>
            </div>
        </div>
    </div>
</div>
<!--添加用户图层-->
<div class="add_menber" id="add_menber_style" style="display:none">

    <ul class=" page-content">
        <li><label class="label_name">用户账号：</label><span class="add_name">
            <input id="addEmail" name="email" type="text" class="text_add"/></span>
            <div class="prompt r_f"></div>
        </li>
        <li><label class="label_name">名字：</label><span class="add_name"><input name="name" type="text"
                                                                               class="text_add"/></span>
            <div class="prompt r_f"></div>
        </li>
        <li id="passwordLi"><label class="label_name">密码：</label><span class="add_name"><input name="pswd" type="text"
                                                                                               class="text_add"/></span>
            <div class="prompt r_f"></div>
        </li>
        <li><label class="label_name">联系电话：</label><span class="add_name"><input name="mobile" type="text"
                                                                                 class="text_add"/></span>
            <div class="prompt r_f"></div>
        </li>

        <li><label class="label_name">邮箱：</label><span class="add_name"><input name="nickname" type="text"
                                                                               class="text_add"/></span>
            <div class="prompt r_f"></div>
        </li>

        <li><label class="label_name">归属仓库：</label>
            <span class="add_name" style="margin-left: 20px;">
            <select class="selectType" name="type">
                <option value="-1">请选择</option>
                <option value="1">1仓库</option>
                <option value="2">2仓库</option>
                <option value="3">3仓库</option>
            </select>
            </span>
            <div class="prompt r_f"></div>
        </li>

        <li><label class="label_name">用户类型</label>
            <span class="add_name" style="margin-left: 20px;">
            <select class="selectUserType" name="shopId" id="shop_id">
                <option value="-1">请选择</option>
                <option value="1">仓管</option>
                <option value="2">财务</option>
            </select>
            </span>
            <div class="prompt r_f"></div>
        </li>

    </ul>
</div>
</body>
</html>
<script>
    jQuery(function ($) {
        $('#pagination').twbsPagination({
            first: "首页",
            prev: "上一页",
            next: "下一页",
            last: "未页",
            startPage:${qo.currentPage},
            totalPages: ${pageResult.totalPage},
            visiblePages: ${qo.pageSize},
            onPageClick: function (event, page) {
                $("[name=currentPage]").val(page);
                $("#searchForm").submit();
            }
        });


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })
    /*用户-添加*/
    $('#member_add').on('click', function () {
        $("#passwordLi").css("display", "");
        $("#addEmail").attr('readonly', false);
        layer.open({
            type: 1,
            title: '添加用户',
            maxmin: true,
            shadeClose: true, //点击遮罩关闭层
            area: ['800px', ''],
            content: $('#add_menber_style'),
            btn: ['提交', '取消'],
            yes: function (index, layero) {
                var num = 0;
                var data = {};
                $(".add_menber input[type='text']").each(function (n, k) {
                    data[k.name] = k.value;
                });
                var alertName = null;
                data.type = $("[name=type]").val();
                data.shopId = $("#shop_id").val();
                if (data.type == -1) {
                    alertName = "请选择仓库";
                }
                if (data.shopId == -1) {
                    alertName = "请选择用户类型";
                }
                if (data.pswd == null || data.pswd == '') {
                    alertName = "用户密码";
                }
                if (data.email == null || data.email == '') {
                    alertName = "用户账号";
                }
                if (alertName != null) {
                    layer.alert(alertName + "不能为空\r\n", {
                        title: '提示框',
                        icon: 0,
                    });
                }

                console.log('---', data, num);
                if (num > 0) {
                    return false;
                } else {
                    $.ajax({
                        url: '/user/editUser',
                        method: "post",
                        data: data,
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        dataType: 'json',
                        success: function (obj) {
                            layer.close(index);//关闭
                            if (obj && obj.success) {
                                layer.alert('添加成功！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            } else {
                                layer.alert('添加失败！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            }
                        }
                    });


                }
            }
        });
    });

    /*用户-查看*/
    function member_show(title, url, id, w, h) {
        layer_show(title, url + '#?=' + id, w, h);
    }

    function resetUserPassword(id) {
        var alterMsg = "要将这个用户的密码重置为123456？"
        var url = "/user/resetUserPassword";
        layer.confirm(alterMsg, function (index) {
            $.ajax({
                url: url + '?id=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('修改成功！', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('修改失败！'+obj.msg, function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

    function updateUserLogin(type, id) {
        var alterMsg = type == 1 ? "确认要禁止这个用户登陆吗？" : "确认要允许这个用户登陆吗？"
        var url = type == 1 ? "/user/forbidUserLogin" : "/user/cancelForbidUserLogin";
        layer.confirm(alterMsg, function (index) {
            $.ajax({
                url: url + '?id=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('修改成功！', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('修改失败！', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

    /*用户-编辑*/
    function member_edit(id) {
        var data = {};
        $("#passwordLi").css("display", "none");
        $("#addEmail").attr('readonly', true);
        $.ajax({
            url: '/user/getByCondition?id=' + id,
            method: "get",
            dataType: 'json',
            success: function (obj) {
                data = obj;

                layer.open({
                    type: 1,
                    title: '修改用户信息',
                    maxmin: true,
                    shadeClose: false, //点击遮罩关闭层
                    area: ['800px', ''],
                    content: $('#add_menber_style'),
                    btn: ['提交', '取消'],
                    success: function (layero, lockIndex) {
                        var body = layer.getChildFrame('body', lockIndex);
                        pubUtil.load(layero, data);//填充表单
                        $(".selectType").val(data.type);
                        $(".selectUserType").val(data.shopId);
                    },
                    yes: function (index, layero) {
                        data = {};
                        data.id = id;
                        data.shopId = $("#shop_id").val();
                        $(".add_menber input[type='text']").each(function (n, k) {
                            data[k.name] = k.value;
                        });
                        var alertName = null;
                        data.type = $("[name=type]").val();
                        if (data.type == -1) {
                            alertName = "请选择仓库";
                        }
                        if (data.email == null || data.email == '') {
                            alertName = "用户账号";
                        }
                        if (alertName != null) {
                            layer.alert(alertName + "不能为空\r\n", {
                                title: '提示框',
                                icon: 0,
                            });
                        } else {
                            $.ajax({
                                url: '/user/editUser',
                                method: "post",
                                data: data,
                                type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                                dataType: 'json',
                                success: function (obj) {
                                    layer.close(index);//关闭
                                    if (obj && obj.success) {
                                        layer.alert('修改成功！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    } else {
                                        layer.alert('修改失败！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    }
                                }
                            });
                            layer.close(index);
                        }
                    }
                });
            }
        });

    }

    /*用户-删除*/
    function member_del(obj, id) {
        layer.confirm('确认要删除吗？', function (index) {
            $.ajax({
                url: '/user/delete?id=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('已删除!', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('删除失败', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

</script>