<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>入库列表</title>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" action="/log/list">
                <div class="search_style">
                    <input name="currentPage" value="${pageResult.currentPage}" type="hidden">
                    <ul class="search_content clearfix">
                        <li><label class="l_f">操作类型</label><input name="action" type="text" class="text_add"
                                                                  value="${(qo.action)!""}"
                                                                  style=" width:150px"/></li>
                        <li><label class="l_f">操作开始时间</label><input name="beginTime" type="text" class="text_add"
                                                                    value="${((qo.beginTime)?string("yyyy-MM-dd"))!""}"
                                                                    id="beginTime"
                                                                    style=" width:150px"/></li>
                        <li><label class="l_f">操作结束时间</label><input name="endTime" type="text" class="text_add"
                                                                    value="${((qo.endTime)?string("yyyy-MM-dd"))!""}"
                                                                    id="endTime"
                                                                    style=" width:150px"/></li>
                        <li><label class="l_f">每页显示条数：</label>
                            <select name="pageSize">
                                <option ${(qo.pageSize == 10)?string('selected="selected"','2')?html}>10</option>
                                <option ${(qo.pageSize == 20)?string('selected="selected"','2')?html}>20</option>
                                <option ${(qo.pageSize == 50)?string('selected="selected"','2')?html}>50</option>
                                <option ${(qo.pageSize == 100)?string('selected="selected"','2')?html}>100</option>
                            </select>
                        </li>

                        <li style="width:150px;">
                            <button type="submit" class="btn_search" style="width: 70px;" ><i class="icon-search"></i>查询</button>
                        </li>
                        <li style="width:90px;">
                    <span class="l_f">
                   </span></li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <th width="25"><label><input type="checkbox" class="ace"><span class="lbl"></span></label></th>
                        <th>ID</th>
                        <th>操作帐户</th>
                        <th>操作类型</th>
                        <th>操作时间</th>
                        <th>操作IP</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as data>
                    <tr>
                        <td><label><input type="checkbox" class="ace"><span class="lbl"></span></label></td>
                        <td> ${(data.id)!""}</td>
                        <td>${(data.account)!""}</td>
                        <td>${(data.action)!""}</td>
                        <td>${((data.opTime)?string("yyyy-MM-dd HH:mm"))!""}</td>
                        <td>${(data.opIp)!""}</td>
                        <td class="td-manage">
                            <a title="查看" onclick="member_edit(${(data.id)!""})" href="javascript:;"
                               class="btn btn-xs btn-info">详情</a>
                        </td>
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                <div style="margin-top: -25px;">
                    <div style="display: inline-block ; margin-left: 32%;">
                        <ul id="pagination" class="pagination"></ul>
                    </div>
                    <div style="margin-top: -50px; margin-left: 12%;">
                        <span style="">找到${pageResult.totalCount}条数据，共${pageResult.totalPage}页</span>
                    </div>
                </div>
                </tfoot>
            </div>
        </div>
    </div>
</div>
<!--添加用户图层-->
<div class="add_menber" id="add_menber_style" style="display:none">
    <div style="margin-left: 5%">
        操作类型:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input value="" name="action" type="text" style="margin-left: 5%; width:400px;margin-top: 15px" class="text_add"/>
    </div>
    <div style="margin-left: 5%">
        操作人员id：<input name="author" type="text" style="margin-left: 5%; width:400px;margin-top: 15px" class="text_add"/>
    </div>
    <div style="margin-left: 5%">
        操作人账户：<input name="account" type="text" style="margin-left: 5%; width:400px;margin-top: 15px" class="text_add"/>
    </div>
    <div style="margin-left: 5%">
        操作ip：  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="opIp" type="text" style="margin-left: 5%; width:400px;margin-top: 15px"/>
    </div>
    <div style="margin-left: 5%">
        操作内容：&nbsp;&nbsp;&nbsp;<input name="content" type="text" style="margin-left: 5%; width:400px;margin-top: 15px"/>
    </div>

</div>
</body>
</html>
<script>
    jQuery(function ($) {
        $('#pagination').twbsPagination({
            first: "首页",
            prev: "上一页",
            next: "下一页",
            last: "未页",
            startPage:${qo.currentPage},
            totalPages: ${pageResult.totalPage},
            visiblePages: ${qo.pageSize},
            onPageClick: function (event, page) {
                $("[name=currentPage]").val(page);
                $("#searchForm").submit();
            }
        });


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })
    /*用户-添加*/
    $('#member_add').on('click', function () {
        layer.open({
            type: 1,
            title: '添加公司',
            maxmin: true,
            shadeClose: true, //点击遮罩关闭层
            area: ['800px', ''],
            content: $('#add_menber_style'),
            btn: ['提交', '取消'],
            yes: function (index, layero) {
                var num = 0;
                var str = "";
                var data = {};
                $(".add_menber input[type$='text']").each(function (n) {
                    data[$(this).attr("name")] = $(this).val();
                    $(".add_menber input[type$='text']").each(function (n) {
                        data[$(this).attr("name")] = $(this).val();
                        if ($(this).val() == "") {
                            var name = $(this).attr("name");
                            var alertName = null;
                            if (name = "name") {
                                alertName = "企业名称";
                            }
                            if (name = "phone") {
                                alertName = "联系电话";
                            }
                            if (name = "contacts") {
                                alertName = "联系人";
                            }
                            if (!alertName) {
                                layer.alert(str += "" + $(this).attr("name") + "不能为空！\r\n", {
                                    title: '提示框',
                                    icon: 0,
                                });
                                num++;
                                return false;
                            }
                        }
                    });
                });
                console.log(data);
                if (num > 0) {
                    return false;
                }
                else {
                    $.ajax({
                        url: '/company/update',
                        method: "post",
                        data: data,
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        dataType: 'json',
                        success: function (obj) {
                            layer.close(index);//关闭
                            if (obj && obj.success) {
                                layer.alert('添加成功！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            } else {
                                layer.alert('添加失败！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            }
                        }
                    });


                }
            }
        });
    });

    /*用户-查看*/
    function member_show(title, url, id, w, h) {
        layer_show(title, url + '#?=' + id, w, h);
    }

    /*用户-停用*/
    function member_stop(obj, id) {
        layer.confirm('确认要停用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs " onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
            $(obj).remove();
            layer.msg('已停用!', {icon: 5, time: 1000});
        });
    }

    /*用户-启用*/
    function member_start(obj, id) {
        layer.confirm('确认要启用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs btn-success" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
            $(obj).remove();
            layer.msg('已启用!', {icon: 6, time: 1000});
        });
    }

    /*用户-编辑*/
    function member_edit(id) {
        var data = {};
        $.ajax({
                    url: '/log/getByCondition?id=' + id,
                    method: "get",
                    dataType: 'json',
                    success: function (obj) {
                        data = obj[0];

                        layer.open({
                            type: 1,
                            title: '详情',
                            maxmin: true,
                            shadeClose: false, //点击遮罩关闭层
                            area: ['50%', '50%'],
                            content: $('#add_menber_style'),
                            btn: ['确定'],
                            success: function (layero, lockIndex) {
                                var body = layer.getChildFrame('body', lockIndex);
                                pubUtil.load(layero, data);//填充表单
                            },
                            yes: function (index, layero) {
                                layer.close(index);
                            }
                        })
                    }
                }
        );

    }

    /*用户-删除*/
    function member_del(obj, id) {
        layer.confirm('确认要删除吗？', function (index) {
            $.ajax({
                url: '/company/delete?ids=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('已删除!', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('删除失败', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

    laydate({
        elem: '#beginTime',
        event: 'focus'
    });
    laydate({
        elem: '#endTime',
        event: 'focus'
    });

</script>