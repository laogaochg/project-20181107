<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <link href="/assets/css/codemirror.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/js/public.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>
    <!-- page specific plugin scripts -->
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/js/H-ui.js"></script>
    <script type="text/javascript" src="/js/H-ui.admin.js"></script>
    <script src="/assets/layer/layer.js" type="text/javascript"></script>
    <script src="/assets/laydate/laydate.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.twbsPagination.min.js"></script>
    <title>入库列表</title>
    <style type="text/css">
        .selectOptionList div {
            margin-left: 19px;
            margin-bottom: 20px;
            float: left;
            width: 50%;
            width: 47%;
        }
    </style>
</head>

<body>
<div class="page-content clearfix">
    <div id="Member_Ratings">
        <div class="d_Confirm_Order_style">
            <form id="searchForm" method="post" action="/assembly/list">
                <div class="search_style">
                    <input name="currentPage" value="${pageResult.currentPage}" type="hidden">
                    <ul class="search_content clearfix">
                        <li><label class="l_f">名称</label><input name="name" type="text" class="text_add"
                                                                value="${(qo.name)!""}"
                                                                style=" width:100px"/></li>
                        <li><label class="l_f">类型&emsp;</label><select name="type" class="text_add"
                                                                       style=" width:100px">
                            <option value="-1">全部</option>
                            <#--<option value="0" ${(qo.type == 0)?string('selected="selected"','')?html}>组件</option>-->
                            <option value="3" ${(qo.type == 3)?string('selected="selected"','')?html}>主板</option>
                            <option value="1" ${(qo.type == 1)?string('selected="selected"','')?html}>SMT料</option>
                        </select>
                        </li>
                        <li><label class="l_f">每页显示条数：</label>
                            <select name="pageSize">
                                <option ${(qo.pageSize == 10)?string('selected="selected"','2')?html}>10</option>
                                <option ${(qo.pageSize == 20)?string('selected="selected"','2')?html}>20</option>
                                <option ${(qo.pageSize == 50)?string('selected="selected"','2')?html}>50</option>
                                <option ${(qo.pageSize == 100)?string('selected="selected"','2')?html}>100</option>
                            </select>
                        </li>

                        <li style="width:150px;">
                            <button type="submit" class="btn_search"><i class="icon-search"></i>查询</button>
                        </li>
                        <li style="width:90px;">
                    <span class="l_f">
                    <a href="javascript:ovid()" id="member_add" class="btn btn-warning"><i
                            class="icon-plus"></i>添加组件</a>
                   </span></li>
                    </ul>
                </div>
            </form>
            <div class="table_menu_list">
                <table class="table table-striped table-bordered table-hover" id="sample-table">
                    <thead>
                    <tr>
                        <th width="25"><label><input type="checkbox" class="ace"><span class="lbl"></span></label></th>
                        <th>ID</th>
                        <th>类型</th>
                        <th>编码</th>
                        <th>名称</th>
                        <th>规格</th>
                        <th>单位</th>
                    <#--<th>出库点</th>-->
                    <#--<th>出库单价</th>-->
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as data>
                    <tr>
                        <td><label><input type="checkbox" class="ace"><span class="lbl"></span></label></td>
                        <td> ${(data.id)!""}</td>
                        <td>
                            <#if data.type==0>
                                组件
                            </#if>
                            <#if data.type==1>
                                SMT料
                            </#if>
                            <#if data.type==3>
                                主板
                            </#if>
                        </td>
                        <td>${(data.code)!""}</td>
                        <td>${(data.name)!""}</td>
                        <td>${(data.modelName)!""}</td>
                        <td>PCS</td>
                    <#--<td>${(data.outPoint)!""}</td>-->
                    <#--<td>${(data.outPointPrice)!""}</td>-->
                        <td class="td-manage">
                        <#--<a title="编辑" onclick="set_price(${(data.id)!""})" href="javascript:;"
                           class="btn btn-xs btn-success">设置出库价格</a>-->
                            <a title="编辑" onclick="member_edit(${(data.id)!""})" href="javascript:;"
                               class="btn btn-xs btn-info"><i class="icon-edit bigger-120"></i></a>
                            <a title="删除" href="javascript:;" onclick="member_del(this,${(data.id)!""})"
                               class="btn btn-xs btn-warning"><i class="icon-trash  bigger-120"></i></a>
                        </td>
                    </tr>
                    </#list>
                </table>
                </tbody>
                <tfoot>
                <div style="margin-top: -25px;margin-left: 60%;">
                    <div style="display: inline-block ; margin-left: 40%;">
                        <ul id="pagination" class="pagination"></ul>
                    </div>
                    <div style="margin-top: -45px; margin-left: 8%;">
                        <span style="">共&nbsp;${(pageResult.totalCount)!""}&nbsp;条数据，当前&nbsp;${(pageResult.totalPage)!""}&nbsp;页</span>
                    </div>
                </div>
                </tfoot>
            </div>
        </div>
    </div>
</div>
<!--添加图层-->
<div class="add_menber" id="add_menber_style" style="display:none">
    <form action="#" id="editAssembly">

        <ul class=" page-content">
            <li><label class="label_name">名称：</label><span class="add_name"><input value="" name="name" type="text"
                                                                                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">类型：</label>
                <span class="add_name" style="margin-left: 20px;">
            <select class="selectType" name="type" id="selectedassembliy" style="width: 270%;">
                <option value="3">主板</option>
                <#--<option value="0">组件</option>-->
                <option value="1">SMT</option>
            </select>
            </span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">编码：</label><span class="add_name">
            <input name="code" type="text"
                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">规格类型：</label><span class="add_name"><input name="modelName" type="text"
                                                                                     class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">单位：</label><span class="add_name"><input name="unit" value="PCS" type="text"
                                                                                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>
            <li><label class="label_name">公司&emsp;</label><span class="add_name"><select name="companyId"
                                                                                         style="width:145px">
            <option value="0">请选择</option>
            <#list companyList as data>
                <option   <#if (data.id=(qo.companyId)!0)>selected="selected"</#if>
                          value="${data.id}">${data.name}</option>
            </#list>
        </select>非成品不选</span></li>
            <li><label class="label_name">备注：</label><span class="add_name"><input name="remark" type="text"
                                                                                   class="text_add"/></span>
                <div class="prompt r_f"></div>
            </li>

            <div class="selectOptionList" style="clear: left" id="smtdiv">
                <div style="margin-left: 100px">
                    <table>
                        <tr>
                            <th>名称</th>
                            <th>规格</th>
                            <th>数量</th>
                        </tr>
                    <#list 0..15 as i>
                        <tr>
                            <th><input name="assemblyName_${i}"/></th>
                            <th><input name="assemblyModelName_${i}"/></th>
                            <th><input name="count_${i}"/></th>
                        </tr>
                    </#list>
                    </table>
                </div>
            </div>

        </ul>
    </form>
</div>

<#--添加价格-->
<div class="add_menber add_price" id="add_price" style="display:none">
    <input name="id" type="hidden"/>
    <ul class="">
        <li style="width: 60%;margin-left: 15%;">
            出库点：
            <input value="" name="outPoint" type="text" class="text_add"/>
        </li>
        <li style="width: 60%;margin-left: 15%;">
            出库单价：
            <input name="outPointPrice" style="margin-left: 22px;" type="text" class="text_add"/>
        </li>

    </ul>
</div>
</body>
</html>
<script>
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
    jQuery(function ($) {
        $('#pagination').twbsPagination({
            first: "首页",
            prev: "上一页",
            next: "下一页",
            last: "未页",
            startPage:${qo.currentPage},
            totalPages: ${pageResult.totalPage},
            visiblePages: ${qo.pageSize},
            onPageClick: function (event, page) {
                $("[name=currentPage]").val(page);
                $("#searchForm").submit();
            }
        });


        $('table th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });


        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
            return 'left';
        }
    })
    /*用户-添加  pubUtil.load(layero, data);//填充表单 */
    $('#member_add').on('click', function () {
        var data = {};
        layer.open({
            type: 1,
            title: '添加组件',
            maxmin: true,
            shadeClose: true, //点击遮罩关闭层
            area: ['800px', ''],
            content: $('#add_menber_style'),
            btn: ['提交', '取消'],
            success: function (layero, index) {
                var data = {};
                data.unit = "PCS";
                pubUtil.load(layero, data);//填充表单
            },
            yes: function (index, layero) {
                var num = 0;
                var str = "";
                var data = $("#editAssembly").serializeObject();
                console.log(data);
                if (data.name == null || data.name == '') {
                    layer.alert("名称不能为空！\r\n", {
                        title: '提示框',
                        icon: 0,
                    });
                    return false;
                } else {
                    $.ajax({
                        url: '/assembly/update',
                        method: "post",
                        data: JSON.stringify(data),
                        type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                        contentType: "application/json; charset=utf-8",
                        success: function (obj) {
                            if (obj && obj.success) {
                                layer.close(index);//关闭
                                layer.alert('添加成功！', {
                                    title: '提示框',
                                    icon: 1,
                                    yes: function () {
                                        $("#searchForm").submit();
                                    }
                                });
                            } else {
                                var str = "";
                                if(obj.msg){
                                    str = obj.msg;
                                }
                                layer.alert('添加失败！'+str, {
                                    title: '提示框',
                                    icon: 1,
                                });
                            }
                        }
                    });


                }
            }
        });
    });

    /*用户-查看*/
    function member_show(title, url, id, w, h) {
        layer_show(title, url + '#?=' + id, w, h);
    }

    /*用户-停用*/
    function member_stop(obj, id) {
        layer.confirm('确认要停用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs " onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
            $(obj).remove();
            layer.msg('已停用!', {icon: 5, time: 1000});
        });
    }

    /*用户-启用*/
    function member_start(obj, id) {
        layer.confirm('确认要启用吗？', function (index) {
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" class="btn btn-xs btn-success" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="icon-ok bigger-120"></i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
            $(obj).remove();
            layer.msg('已启用!', {icon: 6, time: 1000});
        });
    }

    function set_price(id) {
        var data = {};
        $.ajax({
            url: '/assembly/getByCondition?id=' + id,
            method: "get",
            dataType: 'json',
            success: function (obj) {
                data = obj[0];
                console.debug(data);
                layer.open({
                    type: 1,
                    title: '修改组件价格信息',
                    maxmin: true,
                    shadeClose: false, //点击遮罩关闭层
                    area: ['600px', '300px'],
                    content: $('#add_price'),
                    btn: ['提交', '取消'],
                    success: function (layero, lockIndex) {
                        var body = layer.getChildFrame('body', lockIndex);
                        data.unit = "PCS";
                        pubUtil.load(layero, data);//填充表单
                    },
                    yes: function (index, layero) {
                        var num = 0;
                        data = {};
                        $(".add_price input[type$='text']").each(function (n) {
                            data[$(this).attr("name")] = $(this).val();
                        });
                        data.id = id;
                        var str = null;
                        if (data.outPoint == null || data.outPoint == '') {
                            str = "出库点";
                        }
                        if (data.outPointPrice == null || data.outPointPrice == '') {
                            str = "出库单价";
                        }
                        data.updateAction = 1;
                        console.log(data, str);
                        if (str != null) {
                            layer.alert(str + "不能为空！\r\n", {
                                title: '提示框',
                                icon: 0,
                            });
                            return false;
                        } else {
                            $.ajax({
                                url: '/assembly/update',
                                method: "post",
                                data: JSON.stringify(data),
                                type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                                contentType: "application/json; charset=utf-8",
                                success: function (obj) {
                                    layer.close(index);//关闭
                                    if (obj && obj.success) {
                                        layer.alert('修改成功！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    } else {
                                        layer.alert('修改失败！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    }
                                }
                            });
                            layer.close(index);
                        }
                    }
                });
            }
        });
    }

    /*用户-编辑*/
    function member_edit(id) {
        var data = {};
        $.ajax({
            url: '/assembly/getByCondition?id=' + id,
            method: "get",
            dataType: 'json',
            success: function (obj) {
                data = obj[0];
                layer.open({
                    type: 1,
                    title: '修改组件信息',
                    maxmin: true,
                    shadeClose: false, //点击遮罩关闭层
                    area: ['800px', ''],
                    content: $('#add_menber_style'),
                    btn: ['提交', '取消'],
                    success: function (layero, lockIndex) {
                        var body = layer.getChildFrame('body', lockIndex);
                        pubUtil.load(layero, data);//填充表单
                        if (data.type != 3) {
                            $("#smtdiv").hide();
                        } else {
                            $("#smtdiv").show();
                        }
                        $("#selectedassembliy").val(data.type);
                        if (data.companyId) {
                            $("[name=companyId]").val(data.companyId);
                        }
                        for (var i = 0; i < 16; i++) {
                            if (data.properties[i]) {
                                $("[name=" + "assemblyName_" + i + "]").val(data.properties[i].name);
                                $("[name=" + "assemblyModelName_" + i + "]").val(data.properties[i].modelName);
                                $("[name=" + "count_" + i + "]").val(data.properties[i].counts);

                            }

                        }
                    },
                    yes: function (index, layero) {
                        var num = 0;
                        var str = "";
                        var data = $("#editAssembly").serializeObject();
                        data.updateAction = 0;
                        data.id = id;
                        console.log(data);
                        if (data.name == null || data.name == '') {
                            layer.alert("名称不能为空！\r\n", {
                                title: '提示框',
                                icon: 0,
                            });
                            return false;
                        }
                        if (num > 0) {
                            return false;
                        }
                        else {
                            $.ajax({
                                url: '/assembly/update',
                                method: "post",
                                data: JSON.stringify(data),
                                type: 'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
                                contentType: "application/json; charset=utf-8",
                                success: function (obj) {
                                    layer.close(index);//关闭
                                    if (obj && obj.success) {
                                        layer.alert('修改成功！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    } else {
                                        layer.alert('修改失败！', {
                                            title: '提示框',
                                            icon: 1,
                                            yes: function () {
                                                $("#searchForm").submit();
                                            }
                                        });
                                    }
                                }
                            });
                            layer.close(index);
                        }
                    }
                });
            }
        });

    }

    /*用户-删除*/
    function member_del(obj, id) {
        layer.confirm('确认要删除吗？如要有对应建立入库不要删除，避免错误', function (index) {
            $.ajax({
                url: '/assembly/delete?ids=' + id,
                dataType: 'json',
                success: function (obj) {
                    layer.close(index);//关闭
                    if (obj && obj.success) {
                        layer.confirm('已删除!', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })

                    } else {
                        layer.confirm('删除失败', function (index) {
                            $("#searchForm").submit();
                        }, function (index) {
                            $("#searchForm").submit();
                        })
                    }
                }
            });

        });
    }

    $("#selectedassembliy").click(function () {
        var assembliy = $('#selectedassembliy').val();
        if (assembliy == '3') {
            $("#smtdiv").show();
        } else {
            $("#smtdiv").hide();
        }
    });
</script>