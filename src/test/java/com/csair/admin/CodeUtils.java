package com.csair.admin;

import com.csair.admin.core.dto.OrderVo;
import com.csair.admin.core.dto.OutRecordPrintDto;
import com.csair.admin.core.po.OrderChangeRecord;
import org.junit.Test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/11/25 13:39
 * 生成代码工具类
 */
public class CodeUtils {
    @Test
    public void tests() throws Exception {
        BeanInfo beanInfo = Introspector.getBeanInfo(OutRecordPrintDto.OutRecordPrintItemDto.class, Object.class);
        for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {
            String name = pd.getName();
            String nName = name.substring(0, 1).toUpperCase() + name.substring(1);
//            String s = "this.outPrice = a.getOutPrice();".replace("outPrice", name).replace("OutPrice", nName);
//            String s = "record.setOutPrice(StringUtils.hasText(vo.get(\"outPrice\"))?vo.get(\"outPrice\"):null);".replace("outPrice", name).replace("OutPrice", nName);
//            String s = "result.setOutPrice(this.getOutPrice());".replace("outPrice", name).replace("OutPrice", nName);
//            String s = "a.setOutPrice(b.getOutPrice());".replace("outPrice", name).replace("OutPrice", nName);
            String s = "a.setOutPrice(\"\");".replace("outPrice", name).replace("OutPrice", nName);
            System.out.println(s);
        }
    }
}
